//
//  TakeoffLayout.swift
//  VPHCalc
//
//  Created by Admin on 06.08.14.
//  Copyright (c) 2014 Volga-Dnepr Airline. All rights reserved.
//

import Foundation
import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


class TakeoffLayout: NSObject {
    
    var noms2D = [Int : VDANomogram2D]()
    
    //database parameters
    var allAerodromeCode:[String] = []
    var allAerodromeNames:[String] = []
    var icaoCodeIndex: Int = 5566
    var icaoCodeName: String = "UUEE"
    var allRunways: [String] = []
    var myRunway: String = ""
    
    let nfcdictionary: [Double:Double] = [0.4:8.0,0.41:8.4,0.42:8.8,0.43:9.2,0.44:9.6,0.45:10.0,0.46:10.5,0.47:10.8,0.48:11.3,0.49:11.8,0.50:12.0,0.51:12.4,0.52:12.6,0.53:13.1,0.54:13.2,0.55:13.8,0.56:14.0,0.57:14.4,0.58:14.6,0.59:14.8,0.6:15.0]
    var ActualTOW :Double = 0
    var AbsoluteTOW :Double = 200
    var CalculatedTOW :Double = 0
    var AltitudeForCalculation :Double = 0
    var RWYHeading :Double = 359
    var TORA :Double = 3000
    var TODA :Double = 3000
    var ASDA :Double = 3000
    var SID :Double = 3
    var RUD :Double = 120
    var Elevation :Double = 0
    var deltaISA :Double = 0
    var QNH :Double = 1013
    var AOT :Double = 15
    var Slope :Double = 0
    var WindDirection :Double = 359
    var WindSpeed :Double = 0
    var NFC :Double = 0.6
    var Time :Double = 6
    var isUserWeight :Bool = false
    var isPressureAltitude :Bool = true
    var vvIsOne :Bool = false
    var Lrun3req :Double?
    var Lrun4 :Double?
    var Dtakeoff3req :Double?
    var DStop :Double?
    var Lrun4req :Double?
    var Dtkf :Double?    // excluded
    var Dtkfreq :Double? // excluded
    var PressureAltitude:Double?
    var VVmin :Double?
    var VVabsolute :Double = 1
    var VV :Double?
    var VVrequired :Double?
    
    //RUD variables
    var RUD718: Double?
    var RUD723: Double?
    var RUD724: Double?
    var RUD725: Double?
    var minimumRUD: Double?
    
    func load (FromInterface dict:[String:Parameters]) {
        if let x = dict["TORA"]?.target {
            self.TORA = x
        }
        if let x = dict["TODA"]?.target {
            self.TODA = x
        }
        if let x = dict["ASDA"]?.target {
            self.ASDA = x
        }
        if let x = dict["Actual TOW"]?.target {
            self.ActualTOW = x
        }
        if let x = dict["Runway Heading"]?.target {
            self.RWYHeading = x
        }
        if let x = dict["Wind Direction"]?.target {
            self.WindDirection = x
        }
        if let x = dict["Wind Speed"]?.target {
            self.WindSpeed = x
        }
        if let x = dict["QNH"]?.target {
            self.QNH = x
        }
        if let x = dict["Runway slope"]?.target {
            self.Slope = x
        }
        if let x = dict["Temperature"]?.target {
            self.AOT = x
        }
        if let x = dict["Braking action"]?.target {
            self.NFC = x
        }
        if let x = dict["Elevation"]?.target {
            self.Elevation = x
        }
        if let x = dict["SID gradient"]?.target {
            self.SID = x
        }
        if let x = dict["Warm Time"]?.target {
            self.Time = x
        }
        
    }

    
    var Headwind :Double  {
        let x = WindSpeed * cos(((WindDirection - RWYHeading) * 3.14159265359) / 180) //Расчет попутной/встречной составляющей +=head, -=tail
        return x
    }
    var Crosswind :Double {
            let x = -WindSpeed * sin(((WindDirection - RWYHeading) * 3.14159265359) / 180) //Расчет боковой составляющей
            return x
    }
    var MaxCrosswind :Double {
        return nfcdictionary[NFC]!
    }
    
    
    func checkTail(Heading h:Double, Winddir dir: Double, Windspd spd: Double) -> Bool {
        return (-5...30 ~= (spd * cos(((dir - h) * 3.14159265359) / 180)))
    }
    
    
    var Grad3120 :Double?
    var Grad4120 :Double?
    var Grad3400 :Double?
    var Grad4400 :Double?
    var TOWmax :Double?
    var TOWTORA :Double?
    var TOWTODA :Double?
    var TOWASDA :Double?
    var TOWSID :Double?
    var TOWgrad :Double?
    var QFE :Double?
    var V1 :Double?
    var Vr :Double?
    var Votr :Double?
    var V2 :Double?
    var V2n :Double?
    var V3015 :Double?
    var V152 :Double?
    var V20:Double?
    var V4 :Double?
    var Vcircle :Double?
    var ISAT :Double = 0
    var ISAPhpa :Double = 0
    var ISAPmmhg :Double = 0
    var FL4 :Double?
    var FL3 :Double?
    
    
    
    
    
    //----------------------------------------------------------------------------------------------
    
    func getNom2D(_ pNomId:Int) -> VDANomogram2D?{
      
        if let n = noms2D[pNomId]{
            NSLog("\(pNomId) found")
            return n
        }
        else{
            
            let n = VDANomogram2D(filename: "n"+pNomId.description+".txt")
                
            if  n.nomogram.isEmpty{
                return nil
            }
            else{
                noms2D.updateValue(n, forKey: pNomId)
                return n
            }
            
            
        }
        
    }
    //----------------------------------------------------------------------------------------------
    
       //==============================================================================================
    //  Р А С Ч Е Т    В З Л Е Т А
    //  Общие переменные
    var v724leftY :Double = 0
    var v723leftY :Double = 0
    
    //7.18 Максимальная масса для обеспечения градиента 3% (warm up time 2 minutes)
    func f718toweight(_ pAOT :Double, pELV :Double, pHWND :Double, pRUD :Double) -> (Double?) {
       
        //1 column
        var v718column1 :Double = 0
        var v718column2 :Double = 0
        var dm718c2 :Double
        
        if let n718011 = getNom2D(718011){
            NSLog("7.18: 718011 loaded")
            if let x = n718011.getFByValues(pAOT, x2: pELV)
            {
                v718column1 = x
				
                if v718column1 > 400 || v718column1 < 250 
				{
                    NSLog("7.18: column 1 G out of range [250-400]" )
				}
                
                if self.deltaISA > 30
                {
                   LogItem.warningTitle += ["7.18 Delta T ISA Deviation, double check"]
                   LogItem.warningDetail += [" "]
                }

                NSLog("7.18: column 1 G=\(v718column1)")
                LogItem.title += ["7.18/1"]
                LogItem.detail += ["\(v718column1)"]
			}
            else
            {
                NSLog("7.18: column 1 error")
            }
        }
        //2 column
        switch pHWND {
        case let x where x >= 0:
            dm718c2 = pHWND * (0.003159 * v718column1 - 0.6703644)
        case let x where x < 0:
            dm718c2 = -pHWND * (-0.002653 * v718column1 + 1.6451307)
        default: dm718c2 = 0
        }
        NSLog("7.18: delta 2 column = \(dm718c2)")
        v718column2 = v718column1 + dm718c2
        NSLog("7.18: returning =\(v718column2)")
        
		LogItem.title += ["7.18/2"]
        LogItem.detail += ["\(v718column2)"]
        
        //3 column
        //nothing happens
		LogItem.title += ["7.18/3"]
        LogItem.detail += ["\(v718column2)"]
        
        if (v718column2 > 400) { v718column2 = 400  }
        
		LogItem.title += ["7.18 G max by min gradient "]
        LogItem.detail += ["\(v718column2)"]
        return v718column2
    }
    
    //7.19 Максимальная масса для обеспечения градиента 3% (warm up time 4 minutes)
    func f719toweight(_ pAOT :Double, pELV :Double, pHWND :Double, pRUD :Double) -> (Double?) {
        
        //1 column
        var v719column1 :Double = 0
        var v719column2 :Double = 0
        var dm719c2 :Double
        
        if let n719011 = getNom2D(719011){
            NSLog("7.19: 719011 loaded")
            if let x = n719011.getFByValues(pAOT, x2: pELV)
            {
                v719column1 = x
                if v719column1 > 400 || v719column1 < 250 
				{
                    NSLog("7.19: column 1 G out of range [250-400]" )
				}
                
				//let margin = 2.54 * pAOT + 215.8
                if self.deltaISA > 30
                {
                   LogItem.warningTitle += ["7.19 Delta T ISA Deviation, double check"]
                   LogItem.warningDetail += [" "]      
				}
                
                NSLog("7.19: column 1 G=\(v719column1)")
                LogItem.title += ["7.19/1"]
                LogItem.detail += ["\(v719column1)"]
            }
                
            else
            {
                NSLog("7.19: column 1 error")
            }
        }
        //2 column
        switch pHWND {
        case let x where x>0:
            let k = 0.003159 * v719column1 - 0.6703644
            dm719c2 = pHWND * k
        case let x where x<0:
            let k = -0.002653 * v719column1 + 1.6451307
            dm719c2 = -pHWND * k
        default: dm719c2 = 0
        }
        
		NSLog("7.19 delta 2 column = \(dm719c2)")
        v719column2 = v719column1 + dm719c2
        
		LogItem.title += ["7.19/2"]
        LogItem.detail += ["\(v719column2)"]
		
        if (v719column2 > 400) 
		{ 
			v719column2 = 400 
		}
        
		LogItem.title += ["7.19 G max by min gradient "]
        LogItem.detail += ["\(v719column2)"]
		
        return v719column2
    }
    
    //7.20 Максимальная масса для обеспечения градиента 3% (warm up time 6 minutes)
    func f720toweight(_ pAOT :Double, pELV :Double, pHWND :Double, pRUD :Double) -> (Double?) {
       
        //1 column
        var v720column1 :Double = 0
        var v720column2 :Double = 0
        var dm720c2 :Double
        
        if let n720011 = getNom2D(720011){
            NSLog("7.20: 720011 loaded")
            if let x = n720011.getFByValues(pAOT, x2: pELV)
            {
                v720column1 = x
                if v720column1 > 400 || v720column1 < 250 
				{
                    NSLog("7.20: column 1 G out of range [250-400]" )
				}
                
				//let margin = 2.76 * pAOT + 212.8
                
				if self.deltaISA > 30
                {
                   LogItem.warningTitle += ["7.20 Delta T ISA Deviation, double check"]
                   LogItem.warningDetail += [" "] 
                }

                NSLog("7.20: column 1 G=\(v720column1)")
                LogItem.title += ["7.20/1"]
                LogItem.detail += ["\(v720column1)"]
            }
                
            else
            {
                NSLog("7.20: column 1 error")
            }
        }
        //2 column
        switch pHWND {
        case let x where x>0:
            let k = 0.003159 * v720column1 - 0.6703644
            dm720c2 = pHWND * k
        case let x where x<0:
            let k = -0.002653 * v720column1 + 1.6451307
            dm720c2 = -pHWND * k
        default: dm720c2 = 0
        }
        NSLog("7.20 delta 2 column = \(dm720c2)")

        v720column2 = v720column1 + dm720c2
        
		LogItem.title += ["7.20/2"]
        LogItem.detail += ["\(v720column2)"]
  
		if (v720column2 > 400) 
		{ 
			v720column2 = 400 
		}
        
		LogItem.title += ["7.20 Gmax by min gradient"]
        LogItem.detail += ["\(v720column2)"]
        
		return v720column2
    }
    
    func f723toweight (_ pDST :Double, pAOT :Double, pELV :Double, pHWND :Double, pSLP :Double, pRUD: Double) -> Double {
        
        //Определяем левый Y по первому столбцу
        
        if let n723011 = getNom2D(723011){
            NSLog("7.23: 723011 loaded")
            if let x = n723011.getFByValues(pAOT, x2: pELV)
            {
                v723leftY = x
                if v723leftY > 3500 || v723leftY < 1000 
				{
                    NSLog("7.23: column 1 left Y out of range [1000-3500]" )
				}
                //let margin = -2209.8491355 * log (pAOT) + 10072.1533977
                
				if self.deltaISA > 30
                {
                   LogItem.warningTitle += ["7.23 Delta T ISA Deviation, double check"]
                   LogItem.warningDetail += [" "] 
                }

                
                NSLog("7.23: column 1 left Y=\(v723leftY)")
                LogItem.title += ["7.23/1"]
                LogItem.detail += ["\(self.v723leftY)"]
            }
                
            else
            {
                NSLog("7.23: column 1 error")
            }
        }
        
        //Определяем Lразбега (график 6, файл не нужен) //Сделать switch для разных дистанций
        LogItem.title += ["7.23/6 right, Lrun required"]
        LogItem.detail += ["\(pDST)"]
        
		let vLRun = (pDST - 350) / 1.15
        
		LogItem.title += ["7.23/6 left, Lrun"]
        LogItem.detail += ["\(vLRun)"]
        
		NSLog("7.23: pDST= \(pDST), column 6 vLrun= \(vLRun) ")
        
        //Учитываем уклон (график 5)
        var v7235rightleft :Double = 0
        if let n723052 = getNom2D(723052){
            NSLog("7.23: 723052 loaded")
            if let x = n723052.getFByValues(pSLP, x2: vLRun)
            {
                v7235rightleft = x
                if v7235rightleft > 4000 || v7235rightleft < 0 
				{
                    NSLog("7.23: column 5 after SLOPE right to left out of range [0-4000]" )
				}
                NSLog("7.23: column 5 Dist after Slope right to left =\(v7235rightleft)")
                LogItem.title += ["7.23/5"]
				LogItem.detail += ["\(v7235rightleft)"]

            }
                
            else
            {
                NSLog("7.23: column 5 error")
            }
        }
        
        //Учитываем RUD (график 4)
        var v7234rightleft :Double = 0
        if let n723042 = getNom2D(723042){
            NSLog("7.23: 723042 loaded")
            if let x = n723042.getFByValues(pRUD, x2: v7235rightleft)
            {
                v7234rightleft = x
                if v7234rightleft > 4000 || v7234rightleft < 0 
				{
                    NSLog("7.23: column 4 after RUD right to left out of range [0-4000]" )
				}
                NSLog("7.23: column 4 Dist after RUD right to left =\(v7234rightleft)")
                LogItem.title += ["7.23/4"]
				LogItem.detail += ["\(v7234rightleft)"]
            }
                
            else
            {
                NSLog("7.23: column 4 error")
            }
        }
        
        //Учитываем ветер (график 3)
        //Есть погрешности, переделать на формулы перебором
        
        /* var dm7233 :Double = 0
        switch pHWND {
        case let x where x>0: //head
        var k = 0.0114744 * v7234rightleft + 3.510257
        dm7233 = pHWND * k
        case let x where x<0: //tail
        var k = 0.0347375 * v7234rightleft + 15.1732341
        dm7233 = -pHWND * k
        default: dm7233 = 0
        }
        NSLog("7.23 delta 3 column = \(dm7233)")
        var v723rightY :Double = v7234rightleft + dm7233 */
        
        var v723rightY :Double = 0
        if let n723032 = getNom2D(723032){
            NSLog("7.23: 723032 loaded")
            if let x = n723032.getFByValues(pHWND, x2: v7234rightleft)
            {
                v723rightY = x
                if v723rightY > 4000 || v723rightY < 0 
				{
                    NSLog("7.23: column 3 after HWND right to left out of range [0-4000]" )
				}
                NSLog("7.23: column 3 Dist after HWND right to left =\(v723rightY)")
                LogItem.title += ["7.23/3"]
				LogItem.detail += ["\(v723rightY)"]
            }
                
            else
            {
                NSLog("7.23: column 3 error")
            }
        }
        
        //Определяем массу (основной расчет)
        var v723G :Double = 0
        if let n723023 = getNom2D(723023){
            NSLog("7.23: 723023 loaded")
            if let x = n723023.getFByValues(v723leftY, x2: v723rightY)
            {
                v723G = x
                if v723G > 400 || v723G < 200 
				{
                    NSLog("7.23: column 2 Gmax out of range [200-400]" )
				}
                NSLog("7.23: column 2 Gmax =\(v723G)")
                LogItem.title += ["7.23/2"]
				LogItem.detail += ["\(v723G)"]
            }
                
            else
            {
                NSLog("7.23: column 2 error")
            }
        }
        
        if (v723G > 400) { v723G = 400 }
        LogItem.title += ["7.23 G max by TORA"]
		LogItem.detail += ["\(v723G)"]
        return v723G
    }
    
    func f724toweight (_ pDST :Double, pVV :Double, pAOT :Double, pELV :Double, pHWND :Double, pSLP :Double, pRUD: Double) -> Double {
        
        //Определяем левый Y по первому столбцу
      
        if let n724011 = getNom2D(724011){
            NSLog("7.24: 724011 loaded")
            if let x = n724011.getFByValues(pAOT, x2: pELV)
            {
                v724leftY = x
                if v724leftY > 4000 || v724leftY < 500 {
                    NSLog("7.24: column 1 left Y out of range [500-4000]" )}
                //let margin = -2065.9758711 * log (pAOT) + 9595.3971359
                if self.deltaISA > 30
                {
                   LogItem.warningTitle += ["7.24 Delta T ISA Deviation, double check"]
                   LogItem.warningDetail += [" "] 
                }

                NSLog("7.24: column 1 left Y=\(v724leftY)")
                LogItem.title += ["7.24/1"]
				LogItem.detail += ["\(self.v724leftY)"]
            }
                
            else
            {
                NSLog("7.24: column 1 error")
            }
        }
        
        //7 Столбец
       		
		LogItem.title += ["7.24/8"]
		LogItem.detail += ["\(pDST)"]
		
        let v7247rightleft = pDST + 300 - 1000 //TODA переводим в левую шкалу
        NSLog("7.24 column 7 TORA+300-1000=\(v7247rightleft)")
        		
		LogItem.title += ["7.24/7"]
		LogItem.detail += ["\(v7247rightleft)"]
        
        //Учитываем уклон (Столбец 6)
        var v7246rightleft :Double = 0
        if let n724062 = getNom2D(724062){
            NSLog("7.24: 724062 loaded")
            if let x = n724062.getFByValues(pSLP, x2: v7247rightleft + 1000) //В графике 724062 ошибочно учтена дистанция по крайнему справа столбцу
            {
                v7246rightleft = x - 1000//вычитаем 1000 по описанной выше причине (переводим в основную, левую шкалу)
                if v7246rightleft > 4000 || v7246rightleft < 0 
				{
                    NSLog("7.24: column 6 after SLOPE right to left out of range [0-4000]" )
				}
                NSLog("7.24: column 6 Dist after Slope right to left =\(v7246rightleft)")
                
				LogItem.title += ["7.24/6"]
				LogItem.detail += ["\(v7246rightleft)"]
            }
                
            else
            {
                NSLog("7.24: column 6 error")
            }
        }
        
        //Столбец 5 не учитываем при прохождении справа налево. Учитывается при опред РУД отдельно
    
		LogItem.title += ["7.24/5"]
		LogItem.detail += ["not used"]
        
        //Столбец 4 Учитываем ветер
        //Есть погрешности, переделать на формулы перебором как в 723
        
        var v7244rightleft :Double = 0
        if let n724042 = getNom2D(724042){
            NSLog("7.24: 724042 loaded")
            if let x = n724042.getFByValues(pHWND, x2: v7246rightleft)// в ветер пошла дистанция по левой шкале
            {
                v7244rightleft = x // вышла тоже по левой
                if v7244rightleft > 4000 || v7244rightleft < 0 
				{
                    NSLog("7.24: column 4 after HWND right to left out of range [0-4000]" )
				}
                NSLog("7.24: column 4 Dist after HWND right to left =\(v7244rightleft)")
				
				LogItem.title += ["7.24/4"]
				LogItem.detail += ["\(v7244rightleft)"]
            }
                
            else
            {
                NSLog("7.24: column 4 error")
            }
        }
        
        //Столбец 3. Отношение V1/Vr
        
        var v724rightY :Double = 0
        
        if pVV == 1 {
            v724rightY = v7244rightleft
        }
        else {
            if let n724032 = getNom2D(724032){
                NSLog("7.24: 724032 loaded")
                if let x = n724032.getFByValues(pVV, x2: v7244rightleft + 1000) //в V1Vr пошла дистанция по правой шкале
                {
                    v724rightY = x - 1000//вышла по левой
                    if v724rightY > 4000 || v724rightY < 1300 
					{
                        NSLog("7.24: column 3 after VV right to left out of range [1300-4000]" )
					}
                    NSLog("7.24: column 3 Dist after VV right to left =\(v724rightY)")
                }
                    
                else
                {
                    NSLog("7.24: column 3 error")
                }
            }   // if let
            
        }   //else
        
		LogItem.title += ["7.24/3"]
		LogItem.detail += ["\(v724rightY)"]
        
        //Столбец 2. Определяем массу (основной расчет)
        
        var v724G :Double = 0
        if let n724023 = getNom2D(724023){
            NSLog("7.24: 724023 loaded")
            if let x = n724023.getFByValues(v724leftY, x2: v724rightY) // в основной расчет пошла дистанция по левой шкале
            {
                v724G = x
                if v724G > 400 || v724G < 200 
				{
                    NSLog("7.24: column 2 Gmax out of range [200-400]" )
				}
                NSLog("7.24: column 2 Gmax =\(v724G)")
                
				LogItem.title += ["7.24/2"]
				LogItem.detail += ["\(v724G)"]
            }
                
            else
            {
                NSLog("7.24: column 2 error")
            }
        }
        if (v724G > 400) 
		{ 
			v724G = 400 
		}
        
		LogItem.title += ["7.24 G max by TODA"]
		LogItem.detail += ["\(v724G)"]
        return v724G
        
    }//724
    
    //Определяем Максимальную массу по градиенту (7.17)
    func f717toweight (_ pAOT :Double, pELV :Double, pHWND :Double, pSID :Double) -> Double {
        
        //Определяем левый Y по первому столбцу
        var v717leftY :Double = 0
        if let n717011 = getNom2D(717011){
            NSLog("7.17: 717011 loaded")
            if let x = n717011.getFByValues(pAOT, x2: pELV)
            {
                v717leftY = x
                if v717leftY > 17 || v717leftY < 4
				{
                    NSLog("7.17: column 1 left Y out of range [4-17]" )
				
				}
                //let rightmargin = 0.15 * pAOT + 1.65
                //let leftmargin = 0.222 * pAOT + 25.74
				
                if self.deltaISA > 30 || self.deltaISA < -60  
				{
                   LogItem.warningTitle += ["7.17 Delta T ISA Deviation, double check"]
                   LogItem.warningDetail += [" "] 
                }

                NSLog("7.17: column 1 left Y=\(v717leftY)")
                
				LogItem.title += ["7.17/1"]
				LogItem.detail += ["\(v717leftY)"]
            }
                
            else
            {
                NSLog("7.17: column 1 error")
            }
        }
        
        //Учитываем ветер справа налево
        var v717rightY :Double = 0
        if let n717032 = getNom2D(717032){
            NSLog("7.17: 724032 loaded")
            if let x = n717032.getFByValues(pHWND, x2: pSID)
            {
                v717rightY = x
                if v717rightY > 22 || v717rightY < 0 
				{
                    NSLog("7.17: column 3 after Wind right to left out of range [0-22]" )
				}
                NSLog("7.17: column 3 Right Y  =\(v717rightY)")
                
				LogItem.title += ["7.17/3"]
				LogItem.detail += ["\(v717rightY)"]
            }
                
            else
            {
                NSLog("7.17: column 3 error")
            }
        }   // if let
        
        //Столбец 2. Определяем массу (основной расчет)
        var v717G :Double = 0
        if let n717023 = getNom2D(717023){
            NSLog("7.17: 717023 loaded")
            if let x = n717023.getFByValues(v717leftY, x2: v717rightY)
            {
                v717G = x
					if v717G > 400 || v717G < 200 
					{
						NSLog("7.17: column 2 Gmax out of range [200-400]" )
					}
                NSLog("7.17: column 2 Gmax =\(v717G)")
				LogItem.title += ["7.17/2"]
				LogItem.detail += ["\(v717G)"]
            }
                
            else
            {
                NSLog("7.17: column 2 error")
            }
        }
        
        if v717G > 400 
		{
			v717G = 400
		}
        
		LogItem.title += ["7.17 G max by SID gradient"]
		LogItem.detail += ["\(v717G)"]
		
        return v717G
    }//717
    
    func compare (_ pweights :[Double]) -> (Gcalc :Double, Gabs :Double) {
        let maxindex :Int = 4
        var Gcalc :Double = 400
        var Gabs :Double = 400
        for index in (0...maxindex) { //var index = 0; index <= maxindex; ++index {
            if pweights[index] < Gcalc {
                Gcalc = pweights[index]
                NSLog("Gcalc= \(Gcalc)")
            }
        }
        //Если какая то из масс меньше 200, присваиваем 200, чтобы не выйти за массивы в файлах?? ПЕРЕДЕЛАТЬ!
        if Gcalc < 220 {
            Gcalc = 220
           
        }
        
        //Если введена какая то масса, используем для расчетов ее.
        if pweights[5] == 0 {
            Gabs = Gcalc
        }
        else if pweights[5] >= 220 && pweights[5] <= 400 {
            Gabs = pweights[5]
        }
        NSLog("COMPARE: RETURNING Gcalc= \(Gcalc) Gabs= \(Gabs)")
        return (Gcalc, Gabs)
    }//compare
    
     //Определяем VVrequired по графику 7,25
    var v725column1 :Double = 0
    var v725rightY :Double = 0
   
    func f725toVV (_ pDST :Double, pG :Double, pAOT :Double, pELV :Double, pKSC :Double, pSLP :Double, pRUD :Double, pHWND :Double) -> Double {
        
        //Column 1, AOT,ELV
        
        if let n725011 = getNom2D(725011){
            NSLog("7.25tv: 725011 loaded")
            if let x = n725011.getFByValues(pAOT, x2: pELV)
            {
                v725column1 = x
                if v725column1 > 4500 || v725column1 < 1300 
				{
                    NSLog("7.25tv: column 1 Dist out of range [1300-4500]" )
				}
                //let margin = -2480.6378698 * log (pAOT) + 11949.71
                if self.deltaISA > 30 
				{
                   LogItem.warningTitle += ["7.25 Delta T ISA Deviation, double check"]
                   LogItem.warningDetail += [" "] 
                }

                NSLog("7.25tv: column 1 Dist=\(v725column1)")
               	LogItem.title += ["7.25/1"]
				LogItem.detail += ["\(v725column1)"]
            }
                
            else
            {
                NSLog("7.25tv: column 1 error")
            }
        }
        
        //Column 2, Gabs
        var v725leftY :Double = 0
        if let n725021 = getNom2D(725021){
            NSLog("7.25tv: 725021 loaded")
            if let x = n725021.getFByValues(pG, x2: v725column1)
            {
                v725leftY = x
                if v725leftY > 5000 || v725leftY < 1200 
				{
                    NSLog("7.25tv: column 2 left Y out of range [1200-5000]" )
				}
                NSLog("7.25tv: column 2 leftY=\(v725leftY)")
                
				LogItem.title += ["7.25/2 Point B"]
				LogItem.detail += ["\(v725leftY)"]
            }
                
            else
            {
                NSLog("7.25tv: column 2 error")
            }
        }
        
        //Column 7, KSC
        let deltaleft :Double = pDST * 1.065 - pDST
        let deltaright :Double = pDST * 1.15 - pDST
        var v725column7 :Double = 0
        switch pKSC {
        case let x where x < 0.6 && x >= 0.5:
            v725column7 = pDST - (deltaleft / 0.1) * (0.6 - pKSC)
        case let x where x < 0.5 && x > 0.4:
            v725column7 = pDST - deltaleft - (deltaright / 0.1) * (0.5 - pKSC)
        case let x where x == 0.6:
            v725column7 = pDST
        case let x where x == 0.4:
            v725column7 = pDST - deltaleft - deltaright
        default: NSLog("725tv column 7 KSC out of range")
            
        }
        
		LogItem.title += ["7.25/7"]
		LogItem.detail += ["\(v725column7)"]
		
        NSLog("7.25tv column 7 PDST=\(pDST) deltaleft=\(deltaleft) deltaright=\(deltaright)")
        NSLog("7.25tv column 7 after KSC Dist= \(v725column7)")
        
        //Column 6, SLOPE
        var v725column6 :Double = 0
        if let n725062 = getNom2D(725062){
            NSLog("7.25tv: 725062 loaded")
            if let x = n725062.getFByValues(pSLP, x2: v725column7)
            {
                v725column6 = x
                if v725column6 > 5400 || v725column6 < 1200 
				{
                    NSLog("7.25tv: column 6 after SLOPE Dist out of range [1200-5400]" )
				}
                NSLog("7.25tv: column 6 after SLOPE Dist=\(v725column6)")
                
				LogItem.title += ["7.25/6"]
				LogItem.detail += ["\(v725column6)"]
            }
                
            else
            {
                NSLog("7.25tv: column 6 error")
            }
        }
        
        //Column 5, RUD. Not used here. To be calculated later.
        let v725column5 :Double = v725column6
        
		LogItem.title += ["7.25/5"]
		LogItem.detail += ["not used"]
        
        //Column 4, Wind
        
        if let n725042 = getNom2D(725042){
            NSLog("7.25tv: 725042 loaded")
            if let x = n725042.getFByValues(pHWND, x2: v725column5)
            {
                v725rightY = x
                if v725rightY > 5400 || v725rightY < 1200 
				{
                    NSLog("7.25tv: column 4 after WIND Dist out of range [1200-5400]" )
				}
                NSLog("7.25tv: column 4 after Wind Dist=\(v725rightY)")
                
				LogItem.title += ["7.25/4 Point A"]
				LogItem.detail += ["\(v725rightY)"]
            }
                
            else
            {
                NSLog("7.25tv: column 4 error")
            }
        }
        
        //Column 3, VV calculation CHECK ARGUMENTS ORDER in TXT FILES
        var v725VVrequired :Double = 0
        
        if v725rightY > v725leftY 
		{
            v725VVrequired = 1
        	LogItem.title += ["7.25/3, A > B, so V1/Vr ="]
			LogItem.detail += ["\(v725VVrequired)"]
        } 
		else
        {
            if let n725033 = getNom2D(725033)
			{
                NSLog("7.25:tv 725033 loaded")
                if let x = n725033.getFByValues(v725leftY, x2: v725rightY)
                {
                    v725VVrequired = x
					if v725VVrequired > 1 || v725VVrequired < 0.7 
					{
                        NSLog("7.25tv: VVrequired out of range [0.7-1]" )
					}
                    NSLog("7.25tv: VVrequired=\(v725VVrequired)")
                    
					LogItem.title += ["7.25/3, A < B, so V1/Vr = "]
					LogItem.detail += ["\(v725VVrequired)"]
                }
            
                else
                {
                    NSLog("7.25tv: VVrequired error")
                }
            
            }
            
        }//else
        
        return v725VVrequired
    }//725toVV
    
    //Определяем VV по графику 7,27
    func f727toVV (_ pG :Double, pAOT :Double, pELV :Double, pKSC :Double, pRUD: Double) -> Double {
        
        //Column 1, AOT,ELV
        var v727column1 :Double = 0
        if let n727011 = getNom2D(727011){
            NSLog("7.27: 727011 loaded")
            if let x = n727011.getFByValues(pAOT, x2: pELV)
            {
                v727column1 = x
                if v727column1 > 0.97 || v727column1 < 0.7
                {
                    NSLog("7.27: column 1 VV min out of range [0.7-0.97]" )
                }
                
                    if self.deltaISA > 30 
					{
						LogItem.warningTitle += ["7.27 Delta T ISA Deviation, double check"]
						LogItem.warningDetail += [" "] 
					}
					
                NSLog("7.27: column 1 VVmin=\(v727column1)")
                
				LogItem.title += ["7.27/1"]
				LogItem.detail += ["\(v727column1)"]
            }
                
            else
            {
                NSLog("7.27: column 1 error")
            }
        }
        
        //Column 2, G
        var v727column2 :Double = 0
        if let n727021 = getNom2D(727021){
            NSLog("7.27: 727021 loaded")
            if let x = n727021.getFByValues(pG, x2: v727column1)
            {
                v727column2 = x
                if v727column2 > 0.97 || v727column2 < 0.7 
				{
                    NSLog("7.27: column 2 column 2 out of range [0.7-0.97]" )
				}
                NSLog("7.27: column 2=\(v727column2)")
                
				LogItem.title += ["7.27/2"]
				LogItem.detail += ["\(v727column2)"]
            }
                
            else
            {
                NSLog("7.27: column 2 error")
            }
        }
        
        //Column 3, KSC
        var v727column3 :Double = 0
        var v727VVmin :Double = 0
        if let n727031 = getNom2D(727031) {
            NSLog("7.27: 727031 loaded")
            if let x = n727031.getFByValues(pKSC, x2: v727column2)
            {
                v727column3 = x
                if v727column3 > 0.97 || v727column3 < 0.7 
				{
                    NSLog("7.27: column 3 out of range [0.7-0.97]" )
				}
                NSLog("7.27: column 3=\(v727column3)")
                
				LogItem.title += ["7.27/3"]
				LogItem.detail += ["\(v727column3)"]
            }
                
            else
            {
                NSLog("7.27: column 3 error")
            }
            
            //Column 4, RUD, not used here
            v727VVmin = v727column3
			LogItem.title += ["7.27 minimum V1/Vr"]
			LogItem.detail += ["\(v727VVmin)"]
        }
        return v727VVmin
        }				//727
        
        //Определяем Gmax по графику 7,25
        func f725toweight (_ pDST :Double, pVV :Double, pAOT :Double, pELV :Double, pKSC :Double, pSLP :Double, pRUD :Double, pHWND :Double) -> Double {
            
            //Column 1, AOT,ELV
            // v725column1 already defined
            let v725leftYtw :Double = v725column1
            
            //let margin = -2480.6378698 * log (pAOT) + 11949.71
            if self.deltaISA > 30 
			{
				LogItem.warningTitle += ["7.27 Delta T ISA Deviation, double check"]
				LogItem.warningDetail += [" "] 
			}

            NSLog("7.25 to weight, column 1=\(v725leftYtw)")
            
			LogItem.title += ["7.25/1"]
			LogItem.detail += ["\(v725leftYtw)"]
            
            
            //Column 3, V1Vr
            var v725rightYtw :Double = 0
            if let n725032 = getNom2D(725032){
                NSLog("7.25tw: 725032 loaded")
                NSLog("7.25tw: column 3, right Y from 7.25tv=\(v725rightY)")
                if let x = n725032.getFByValues(pVV, x2: v725rightY)
                {
                    v725rightYtw = x
                    if v725rightYtw > 5000 || v725rightYtw < 1200 
					{
                        NSLog("7.25tw: column 3, D out of range [1200-5000]" )
					}
                    NSLog("7.25tw: column 3 725rightYtw=\(v725rightYtw)")
                    
					LogItem.title += ["7.25/3"]
					LogItem.detail += ["\(v725rightYtw)"]
                    
                }
                else
                {
                    NSLog("7.25tw: column 3 error")
                }
            }
            
            
            // Calculating Gmax, column 2
            var v725Gmax :Double = 0
            if let n725023 = getNom2D(725023){
                NSLog("7.25tw: 725023 loaded")
                if let x = n725023.getFByValues(v725leftYtw, x2: v725rightYtw)
                {
                    v725Gmax = x
                    if v725Gmax > 400 || v725Gmax < 200
					{
                        NSLog("7.25: Gmax of range [200-400]" )
					}
                    NSLog("7.25: Gmax=\(v725Gmax)")
                    
					LogItem.title += ["7.25/2"]
					LogItem.detail += ["\(v725Gmax)"]
                }
                    
                else
                {
                    NSLog("7.25tw: VVrequired error")
                }
            }
            if v725Gmax > 400 
			{ 
				v725Gmax = 400 
			}
            
			LogItem.title += ["7.25 TOW max"]
			LogItem.detail += ["\(v725Gmax)"]
            return v725Gmax
        }	//725toWeight
    
    
    func f724todist (_ pG :Double, pVV :Double, pAOT :Double, pELV :Double, pHWND :Double, pSLP :Double, pRUD: Double) -> (vLrun3req :Double?, vD3req :Double?) {
        
        // Column 1
        // calculated in f724toweight = v724leftY
		
        let v724tdcolumn1 = v724leftY
        
		
        if self.deltaISA > 30 
		{
				LogItem.warningTitle += ["7.24 Delta T ISA Deviation, double check"]
				LogItem.warningDetail += [" "] 
		}

        
		LogItem.title += ["7.24/1"]
		LogItem.detail += ["\(v724tdcolumn1)"]
        
        
        //Column 2, Weight
        var v724tdcolumn2 :Double = 0
        if let n724021 = getNom2D(724021){
            NSLog("7.24td: 724021 loaded")
            if let x = n724021.getFByValues(pG, x2: v724tdcolumn1)
            {
                v724tdcolumn2 = x
                if v724tdcolumn2 > 5000 || v724tdcolumn2 < 1000 
				{
                    NSLog("7.24td: column 2 DIST out of range [1000-5000]" )
				}
                NSLog("7.24td: column 2 DIST= \(v724tdcolumn2)")
                
				LogItem.title += ["7.24/2"]
				LogItem.detail += ["\(v724tdcolumn2)"]
            }
            else
            {
                NSLog("7.24td: column 2 error")
            }
        } 
		else
        {
            NSLog("7.24td: FILE 724021 NOT FOUND")
        }
        
        //Column 3, V1/Vr
        var v724tdcolumn3 :Double = 0
        if let n724031 = getNom2D(724031){
            NSLog("7.24: 724031 loaded")
            if let x = n724031.getFByValues(pVV, x2: v724tdcolumn2 + 1000) //переводим под правую шкалу
            {
                v724tdcolumn3 = x - 1000 //на выходе переводим обратно в левую
                if v724tdcolumn3 > 5000 || v724tdcolumn3 < 1000 
				{
                    NSLog("7.24: column 3 DIST out of range [1000-5000]" )
				}
                NSLog("7.24: column 3 DIST= \(v724tdcolumn3)")
                
				LogItem.title += ["7.24/3"]
				LogItem.detail += ["\(v724tdcolumn3)"]
            }
            else
            {
                NSLog("7.24: column 3 error")
            }
        } 
		else
        {
            NSLog("7.24: FILE 724031 NOT FOUND")
        }
        
        //Column 4, Wind
        var v724tdcolumn4 :Double = 0
        if let n724041 = getNom2D(724041){
            NSLog("7.24: 724041 loaded")
            if let x = n724041.getFByValues(pHWND, x2: v724tdcolumn3)
            {
                v724tdcolumn4 = x
                if v724tdcolumn4 > 5000 || v724tdcolumn4 < 1000 
				{
                    NSLog("7.24: column 4 DIST out of range [1000-5000]" )
				}
                NSLog("7.24: column 4 DIST= \(v724tdcolumn4)")
                
				LogItem.title += ["7.24/4"]
				LogItem.detail += ["\(v724tdcolumn4)"]
            }
            else
            {
                NSLog("7.24: column 4 error")
            }
        } 
		else
        {
            NSLog("7.24: FILE 724041 NOT FOUND")
        }
        
        //Столбец 5 не учитываем. Учитывается при опред РУД отдельно
      
		LogItem.title += ["7.24/5"]
		LogItem.detail += ["not used"]
        
        
        // Column 6, Slope
        var v724tdcolumn6 :Double = 0
        if let n724061 = getNom2D(724061){
            NSLog("7.24: 724061 loaded")
            if let x = n724061.getFByValues(pSLP, x2: v724tdcolumn4 + 1000) //+1000 тк в графике 724061 724062 дистанция по крайнему столбцу ошибочно
            {
                v724tdcolumn6 = x - 1000 //тк в графике 724061 724062 дистанция по крайнему столбцу ошибочно
                if v724tdcolumn6 > 5000 || v724tdcolumn6 < 1000 
				{
                    NSLog("7.24: column 6 DIST out of range [1000-5000]" )
				}
                NSLog("7.24: column 6 DIST= \(v724tdcolumn6)")
               
				LogItem.title += ["7.24/6"]
				LogItem.detail += ["\(v724tdcolumn6)"]
            }
            else
            {
                NSLog("7.24: column 6 error")
            }
        } 
		else
        {
            NSLog("7.24: FILE 724061 NOT FOUND")
        }
        
        // 7 Столбец. Потребная длина разбега при продолженном взлете
        let v724tdcolumn7 = v724tdcolumn6 + 340
       	LogItem.title += ["7.24/7 L run 3 required"]
		LogItem.detail += ["\(v724tdcolumn7)"]
        
        // 8 Столбец. Потребная Дистанция продолженного взлета
        let v724tdcolumn8 = v724tdcolumn6 + 1000 - 310
        		
		LogItem.title += ["7.24/8 D tkf 3 required"]
		LogItem.detail += ["\(v724tdcolumn8)"]
		
        NSLog("7.24 RETURNING Lrun3req=\(v724tdcolumn7)")
        NSLog("7.24 RETURNING D3req=\(v724tdcolumn8)")

        return (v724tdcolumn7, v724tdcolumn8)
        
    }//724td
    
func f723todist (_ pG :Double, pAOT :Double, pELV :Double, pHWND :Double, pSLP :Double, pRUD: Double) -> (vLrun :Double?, vLrunreq :Double?, vLdist :Double?, vLdistreq :Double?) {
        
        var vLrun :Double
        var vLrunreq :Double
        var vLdist :Double
        var vLdistreq :Double
        
        // Column 1
        // calculated in f723toweight = v723leftY
        let v723tdcolumn1 = v723leftY
		
        //let margin = -2209.8491355 * log (pAOT) + 10072.1533977
		
        if self.deltaISA > 30
		{
				LogItem.warningTitle += ["7.23 Delta T ISA Deviation, double check"]
				LogItem.warningDetail += [" "] 
		}

        LogItem.title += ["7.23/1"]
		LogItem.detail += ["\(v723tdcolumn1)"]
        
        
        //Column 2, Weight
        var v723tdcolumn2 :Double = 0
        if let n723021 = getNom2D(723021){
            NSLog("7.23td: 723021 loaded")
            if let x = n723021.getFByValues(pG, x2: v723tdcolumn1)
            {
                v723tdcolumn2 = x
                if v723tdcolumn2 > 4000 || v723tdcolumn2 < 0 
				{
                    NSLog("7.23td: column 2 DIST out of range [0-4000]" )
				}
                NSLog("7.23td: column 2 DIST= \(v723tdcolumn2)")
             
				LogItem.title += ["7.23/2"]
				LogItem.detail += ["\(v723tdcolumn2)"]
            }
            else
            {
                NSLog("7.23td: column 2 error")
            }
        } 
		else
        {
            NSLog("7.23td: FILE 723021 NOT FOUND")
        }
        
        
        //Column 3, Wind
        var v723tdcolumn3 :Double = 0
        if let n723031 = getNom2D(723031){
            NSLog("7.23: 724031 loaded")
            if let x = n723031.getFByValues(pHWND, x2: v723tdcolumn2)
            {
                v723tdcolumn3 = x
                if v723tdcolumn3 > 4000 || v723tdcolumn3 < 0 
				{
                    NSLog("7.23: column 3 DIST out of range [0-4000]" )
				}
                NSLog("7.23: column 3 DIST= \(v723tdcolumn3)")
              
				LogItem.title += ["7.23/3"]
				LogItem.detail += ["\(v723tdcolumn3)"]
            }			
            else
            {
                NSLog("7.23: column 3 error")
            }
        } 
		else
        {
            NSLog("7.23: FILE 723031 NOT FOUND")
        }
        
        // Column 4, not used here
        LogItem.title += ["7.23/4"]
		LogItem.detail += ["not used"]
        
        
        // Column 5, Slope
        var v723tdcolumn5 :Double = 0
        if let n723051 = getNom2D(723051){
            NSLog("7.23: 723051 loaded")
            if let x = n723051.getFByValues(pSLP, x2: v723tdcolumn3)
            {
                v723tdcolumn5 = x
                if v723tdcolumn5 > 4000 || v723tdcolumn5 < 0 
				{
                    NSLog("7.23: column 5 DIST out of range [0-4000]" )
				}
                NSLog("7.23: column 5 DIST= \(v723tdcolumn5)")
               
				LogItem.title += ["7.23/5"]
				LogItem.detail += ["\(v723tdcolumn5)"]
            }
            else
            {
                NSLog("7.23: column 5 error")
            }
        } 
		else
        {
            NSLog("7.23: FILE 723051 NOT FOUND")
        }
        
        // Column 5-6, L run
        vLrun = v723tdcolumn5
        NSLog("7.23 RETURNING vLrun =\(vLrun)")
       	LogItem.title += ["7.23/6 L run"]
		LogItem.detail += ["\(vLrun)"]
        
        // Column 6-7, L run required
        vLrunreq = 1.15 * (vLrun + 300)
        NSLog("7.23 RETURNING vLrunreq=\(vLrunreq)")
        LogItem.title += ["7.23/7 L run req"]
		LogItem.detail += ["\(vLrunreq)"]
        
        // Column 7-8, L tkf distance
        vLdist = vLrun + 1000 - 400
        NSLog("7.23 RETURNING vLdist=\(vLdist)")
        LogItem.title += ["7.23/8 D tkf"]
		LogItem.detail += ["\(vLdist)"]
        
        // Column 8, L tkf distance required
        vLdistreq = vLdist * 1.15
        NSLog("7.24 RETURNING vLdistreq =\(vLdistreq)")
        LogItem.title += ["7.23/9 D tkf required"]
		LogItem.detail += ["\(vLdistreq)"]
        
        return (vLrun, vLrunreq, vLdist,vLdistreq)
        
}//723td
    
    
    // Calculating V1 speed
    func f726 (_ pG :Double, pVV :Double) -> Double {
        
        
        var vV:Double = 0
        if let n726011 = getNom2D(726011)
		{
            NSLog("7.26: 726011 loaded")
            if let x = n726011.getFByValues(pG, x2: pVV)
            {
                NSLog("7.26: INPUT PRM: G=\(pG) VV=\(pVV)")
                vV = floor(x)
                if vV > 280 || vV < 212 
				{
                    NSLog("7.26: V1 out of range [212-280]" )
				}
                NSLog("7.26: RETURNING V1= \(vV)")
                LogItem.title += ["7.26 V1"]
				LogItem.detail += ["\(vV)"]
            }
            else
            {
                NSLog("7.26: calculation error")
            }
        } 
		else
        {
            NSLog("7.26: FILE 726011 NOT FOUND")
        }
    return vV
    }
    
    //7.25 Располагаемая дистанция прерванного взлета
    func f725toDSTOP(_ pAOT :Double, pELV :Double, pG :Double, pV1VR :Double, pHWND :Double, pRUD :Double, pSLP:Double, pKSC :Double) -> (Double?) {
        
        NSLog("here")
        //1 column
        var v725DScolumn1 :Double = 0
        if let n725011 = getNom2D(725011){
            NSLog("7.25 to DSTOP: 725011 loaded")
            if let x = n725011.getFByValues(pAOT, x2: pELV)
            {
                v725DScolumn1 = x
                if v725DScolumn1 > 5500 || v725DScolumn1 < 1000 
				{
                    NSLog("7.25 to DSTOP: column 1 DIST out of range [1000-5500]" )
				}
                //let margin = -2480.6378698 * log (pAOT) + 11949.71
                
				if self.deltaISA > 30 
				{
				LogItem.warningTitle += ["7.25 Delta T ISA Deviation, double check"]
				LogItem.warningDetail += [" "] 
				}

                NSLog("7.25 to DSTOP: column 1 DIST=\(v725DScolumn1)")
               
				LogItem.title += ["7.25/1"]
				LogItem.detail += ["\(v725DScolumn1)"]
            }
            else
            {
                NSLog("7.25 to DSTOP: column 1 error")
            }
        }
        else
        {
            NSLog("7.25 to DSTOP: 725011 not found")
        }

        
        //2 column
        var v725DScolumn2 :Double = 0
        if let n725021 = getNom2D(725021){
            NSLog("7.25 to DSTOP: 725021 loaded")
            if let x = n725021.getFByValues(pG, x2: v725DScolumn1)
            {
                v725DScolumn2 = x
                if v725DScolumn2 > 5000 || v725DScolumn2 < 1500 
				{
                    NSLog("7.25 to DSTOP: column 2 DIST out of range [1500-5000]" )
				}
                NSLog("7.25 to DSTOP: column 2 DIST=\(v725DScolumn2)")
               
				LogItem.title += ["7.25/2"]
				LogItem.detail += ["\(v725DScolumn2)"]
            }
            else
            {
                NSLog("7.25 to DSTOP: column 2 error")
            }
        }
        else
        {
            NSLog("7.25 to DSTOP: 725021 not found")
        }


        //3 column, V1VR
        var v725DScolumn3 :Double = 0
        if let n725031 = getNom2D(725031){
            NSLog("7.25 to DSTOP: 725031 loaded")
            if let x = n725031.getFByValues(pV1VR, x2: v725DScolumn2)
            {
                v725DScolumn3 = x
                if v725DScolumn3 > 5000 || v725DScolumn3 < 1500 
				{
                    NSLog("7.25 to DSTOP: column 3 DIST out of range [1500-5000]" )
				}
                NSLog("7.25 to DSTOP: column 3 DIST=\(v725DScolumn3)")
                
				LogItem.title += ["7.25/3"]
				LogItem.detail += ["\(v725DScolumn3)"]
            }
            else
            {
                NSLog("7.25 to DSTOP: column 3 error")
            }
        }
        else
        {
            NSLog("7.25 to DSTOP: 725031 not found")
        }

        
        //4 column, Headwind
        var v725DScolumn4 :Double = 0
        NSLog("7.25 to DSTOP: COLUMN 4: pHWND=\(pHWND) Param:\(v725DScolumn3)")
        if let n725041 = getNom2D(725041){
            NSLog("7.25 to DSTOP: 725041 loaded")
            if let x = n725041.getFByValues(pHWND, x2: v725DScolumn3)
            {
                v725DScolumn4 = x
                if v725DScolumn4 > 5000 || v725DScolumn4 < 1500 
				{
                    NSLog("7.25 to DSTOP: column 4 DIST out of range [1500-5000]" )
				}
                NSLog("7.25 to DSTOP: x=\(x) column 4 DIST=\(v725DScolumn4)")
                
				LogItem.title += ["7.25/4"]
				LogItem.detail += ["\(v725DScolumn4)"]
            }
            else
            {
                NSLog("7.25 to DSTOP: column 4 error")
            }
        }
        else
        {
            NSLog("7.25 to DSTOP: 725041 not found")
        }

        
        //5 column, RUD, nothing
        let v725DScolumn5 :Double = v725DScolumn4
        NSLog("7.25 to DSTOP: column 5 (No change) DIST=\(v725DScolumn5)")

		LogItem.title += ["7.25/5"]
		LogItem.detail += ["not used"]
        
        //6 column, Slope
        var v725DScolumn6 :Double = 0
        if let n725061 = getNom2D(725061){
            NSLog("7.25 to DSTOP: 725061 loaded")
            if let x = n725061.getFByValues(pSLP, x2: v725DScolumn5)
            {
                v725DScolumn6 = x
                if v725DScolumn6 > 5000 || v725DScolumn6 < 1500 
				{
                    NSLog("7.25 to DSTOP: column 6 DIST out of range [1500-5000]" )
				}
                NSLog("7.25 to DSTOP: column 6 DIST=\(v725DScolumn6)")
                
				LogItem.title += ["7.25/6"]
				LogItem.detail += ["\(v725DScolumn6)"]
            }
                
            else
            {
                NSLog("7.25 to DSTOP: column 6 error")
            }
        }
        else
        {
            NSLog("7.25 to DSTOP: 725061 not found")
        }

        
        //Column 7, KSC
        let deltaleft :Double = v725DScolumn6 * 1.065 - v725DScolumn6
        let deltaright :Double = v725DScolumn6 * 1.15 - v725DScolumn6
        var v725DScolumn7 :Double = 0
        switch pKSC {
        case let x where x < 0.6 && x >= 0.5:
            v725DScolumn7 = v725DScolumn6 + (deltaleft / 0.1) * (0.6 - pKSC)
        case let x where x < 0.5 && x > 0.4:
            v725DScolumn7 = v725DScolumn6 + deltaleft + (deltaright / 0.1) * (0.5 - pKSC)
        case let x where x >= 0.6:
            v725DScolumn7 = v725DScolumn6
        case let x where x == 0.4:
            v725DScolumn7 = v725DScolumn6 + deltaleft + deltaright
        default:
            v725DScolumn7 = 9999.9
            NSLog("725tv column 7 KSC out of range")
        }
        
		LogItem.title += ["7.25/7"]
		LogItem.detail += ["\(v725DScolumn7)"]
        
        return v725DScolumn7
    }
    
   // Pressure altitude parameters
    
    func Pressure ()
    {
        var h_array :[Double] = [-200,-100,0,100,200,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2100,2200,2300,2400,2500,2600,2700,2800,2900,3000,3100,3200,3300,3400,3500,3600,3700,3800,3900,4000,4100,4200,4300,4400]
        var p_array :[Double] = [1036,1026,1013.25,1002,990,979,968,956,943,933,921,909,899,888,878,868,855,845,835,825,815,805,795,786,775,765,756,748,738,729,719,710,701,692,682,675,666,658,649,641,632,625,617,609,601,594,585]
        
        if self.Elevation > -200 && self.Elevation < 4400
        {
            for index in ( 0...46) { //var index = 0; index <= 46; index += 1
                
                if self.Elevation <= h_array[index]
                {
                    NSLog("Index=\(index)")
                    self.ISAPhpa = p_array[index - 1]
                    let m = (self.Elevation - h_array[index - 1]) * (p_array[index - 1] - p_array[index]) / (h_array[index] - h_array[index - 1])
                    self.ISAPhpa = self.ISAPhpa - m
                    break
                }
            }
            self.ISAPmmhg = self.ISAPhpa * 0.750061683
        }
        
        NSLog("ISAP=\(self.ISAPhpa)")
        NSLog("QNH=\(self.QNH)")
        self.ISAT = -0.0065 * self.Elevation + 15
        
        LogItem.title += ["ISA pressure, hpa "]
        LogItem.detail += ["\(self.ISAPhpa)"]
        
        LogItem.title += ["ISA pressure, mmhg"]
        LogItem.detail += ["\(self.ISAPmmhg)"]
        
        LogItem.title += ["ISA temperature"]
        LogItem.detail += ["\(self.ISAT)"]
        
        
        if (self.isPressureAltitude) && (self.ISAPhpa > 695) && (self.ISAPhpa < 1036 )  {
            let ZeroDev :Double = 1013.25 - self.ISAPhpa
            self.QFE  = self.QNH  - ZeroDev
            if let qfe = self.QFE {
                LogItem.title += ["QFE"]
                LogItem.detail += ["\(qfe)"]
                NSLog("QFE=\(qfe)")
                
                if 585...1036 ~= qfe
                {
                    for index in (0...46).reversed() { //var index = 46; index >= 0; index -= 1
                        
                        if qfe <= p_array[index]  {
                            NSLog("Index2=\(index)")
                            let mult = (h_array[index] - h_array[index+1]) / (p_array[index] - p_array[index + 1])
                            self.PressureAltitude = h_array[index] - (p_array[index] - qfe) * mult
                            break
                        }
                    }
                    
                    LogItem.title += ["Pressure Altitude"]
                    let pa_info = self.PressureAltitude ?? 0
                    LogItem.detail += ["\(pa_info)"]
                }
            }
        }
        else
        {
            
            LogItem.title += ["Pressure Altitude has not been calculated"]
            LogItem.detail += [" "]
            self.PressureAltitude = nil
            self.QFE = nil
        }
        
    }
    
    //7.18 Минимальный РУД для обеспечения градиента 3% для заданной массы
    func f718RUD(_ pAOT :Double, pELV :Double, pHWND :Double, pWeight :Double) -> Double? {
        
        //1 column
        var v718column1 :Double = 0
        var v718column2 :Double = 0
        var v718column3 :Double = 0
        var dm718c2 :Double
        
        if let n718011 = getNom2D(718011){
            NSLog("7.18: 718011 for RUD loaded")
            if let x = n718011.getFByValues(pAOT, x2: pELV)
            {
                v718column1 = x
                
                if v718column1 > 400 || v718column1 < 250
                {
                    NSLog("7.18: column 1 G out of range [250-400]" )
                }
                
                if self.deltaISA > 30
                {
                    LogItem.warningTitle += ["7.18 Delta T ISA Deviation, double check"]
                    LogItem.warningDetail += [" "]
                }
                
                NSLog("7.18: RUD: column 1 G=\(v718column1)")
                LogItem.title += ["7.18/1 (RUD)"]
                LogItem.detail += ["\(v718column1)"]
            }
            else
            {
                NSLog("7.18: RUD: column 1 error")
            }
        }
        //2 column
        switch pHWND {
        case let x where x >= 0:
            dm718c2 = pHWND * (0.003159 * v718column1 - 0.6703644)
        case let x where x < 0:
            dm718c2 = -pHWND * (-0.002653 * v718column1 + 1.6451307)
        default: dm718c2 = 0
        }
        NSLog("7.18: RUD: delta 2 column = \(dm718c2)")
        v718column2 = v718column1 + dm718c2
        NSLog("7.18: returning =\(v718column2)")
        
        LogItem.title += ["7.18/2 Left weight"]
        LogItem.detail += ["\(v718column2)"]
        
        //3 column
        if let n718033 = getNom2D(718033){
            NSLog("7.18: 718033 for RUD loaded")
            if let x = n718033.getFByValues(v718column2, x2: pWeight)
            {
                v718column3 = x - 5
                
                if v718column3 > 122 || v718column3 < 95
                {
                    NSLog("7.18: column 1 RUD out of range [95-122]" )
                }
                
                NSLog("7.18: RUD: column 3 RUD=\(v718column3)")
                LogItem.title += ["7.18/3 Right weight"]
                LogItem.detail += ["\(pWeight)"]
            }
            else
            {
                NSLog("7.18: RUD: column 1 error")
            }
        }

        
        
        
        
        LogItem.title += ["7.18/3"]
        LogItem.detail += ["\(v718column3)"]
        
        
        LogItem.title += ["7.18 RUD by Gradient "]
        LogItem.detail += ["\(v718column3)"]
        return v718column3
    }

    
    
    
        //Основная функция расчета взлета
    func calcTakeoff () {
    
            //Checking incoming parameters
            
            NSLog("T=\(self.AOT)")
            NSLog("ELV=\(self.Elevation)")
            NSLog("WSPD=\(self.WindSpeed)")
            NSLog("WDIR=\(self.WindDirection)")
            NSLog("Slope=\(self.Slope)")
            NSLog("RWY=\(self.RWYHeading)")
            NSLog("KSC=\(self.NFC)")
            NSLog("time=\(self.Time)")
            NSLog("FORCED=\(self.vvIsOne)")
            NSLog("G Entered=\(self.ActualTOW)")
            NSLog("SID=\(self.SID)")
            NSLog("RUD=\(self.RUD)")
            NSLog("TORA=\(self.TORA)")
            NSLog("TODA=\(self.TODA)")
            NSLog("ASDA=\(self.ASDA)")
        
        if (self.isPressureAltitude == false) 
		{  
			self.AltitudeForCalculation = self.Elevation 
		}
        else  
		{ 
			self.AltitudeForCalculation = self.PressureAltitude! 
		}

        var Gtkfmaxarray :[Double] = [400,400,400,400,400,400] 			//Массив для определения максимальной массы
        var vVVreq :Double = 1
        var vVVmin :Double = 1
        //var vVVabsolute : Double = 1
            
        self.deltaISA = self.AOT - (-0.0065 * self.AltitudeForCalculation + 15 )
        NSLog("DeltaISA=\(self.deltaISA)")
        
        LogItem.wrnmsg = nil
        
        if self.deltaISA > 30 {
            if LogItem.wrnmsg != nil
            {
                LogItem.wrnmsg = LogItem.wrnmsg! + "\nISA TEMP DEVIATION IS OUT OF FLIGHT MANUAL, calculation on your responsibility (\(self.deltaISA))"
            }
            else
            {
                LogItem.wrnmsg = "\nISA TEMP DEVIATION IS OUT OF FLIGHT MANUAL, calculation on your responsibility (\(self.deltaISA))"
            }
        }
        
        

        
            
            //Создание массива расчитанных макс масс
            //Определяем Макс массу для обеспечения градиента 3% по графикам 7.18 7.19 7.20
        
        LogItem.title += ["TAKEOFF REPORT"]
        LogItem.detail += [" "]
		
		LogItem.title += ["Delta T ISA Deviation"]
        LogItem.detail += ["\(self.deltaISA)"]
        
        switch self.Time 
		{
			case 2: if let x =  f718toweight (self.AOT, pELV :self.AltitudeForCalculation, pHWND :self.Headwind, pRUD :self.RUD)
			{ 
				Gtkfmaxarray[0] = x
				NSLog("Gtkfmax (7.18) [0] \(Gtkfmaxarray[0])")
			}
			case 4: if let x =  f719toweight (self.AOT, pELV :self.AltitudeForCalculation, pHWND :self.Headwind, pRUD :self.RUD)
			{ 
				Gtkfmaxarray[0] = x
				NSLog("Gtkfmax (7.19) [0] \(Gtkfmaxarray[0])")
			}
			case 6: if let x =  f720toweight (self.AOT, pELV :self.AltitudeForCalculation, pHWND :self.Headwind, pRUD :self.RUD)
			{ 
				Gtkfmaxarray[0] = x
				NSLog("Gtkfmax (7.20) [0] \(Gtkfmaxarray[0])")
			}
        default: Gtkfmaxarray[0] = 200
        }
        
        
		LogItem.title += ["7.23 CALC TOW BY TORA"]
		LogItem.detail += [" "]
        Gtkfmaxarray[1] = f723toweight (self.TORA, pAOT :self.AOT, pELV :self.AltitudeForCalculation, pHWND :self.Headwind, pSLP :self.Slope, pRUD: self.RUD)
        NSLog("Gtkfmax[1] \(Gtkfmaxarray[1])")
        
		LogItem.title += ["7.24 CALC TOW BY TODA"]
		LogItem.detail += [" "]        
		Gtkfmaxarray[2] = f724toweight (self.TODA, pVV :1, pAOT :self.AOT, pELV :self.AltitudeForCalculation, pHWND :self.Headwind, pSLP :self.Slope, pRUD: self.RUD)//VVabsolute changed to 1
        NSLog("Gtkfmax[2] \(Gtkfmaxarray[2])")
        
		LogItem.title += ["7.17 CALC TOW BY SID GRADIENT"]
		LogItem.detail += [" "]        
		Gtkfmaxarray[3] = f717toweight (self.AOT, pELV :self.AltitudeForCalculation, pHWND :self.Headwind, pSID :self.SID)
        NSLog("Gtkfmax[3] \(Gtkfmaxarray[3])")
            
		Gtkfmaxarray[4] = 400
        NSLog("Gtkfmax[4] \(Gtkfmaxarray[4])")
            
		Gtkfmaxarray[5] = self.ActualTOW
		LogItem.title += ["Actual entered TOW "]
		LogItem.detail += ["\(Gtkfmaxarray[5])"]        
		NSLog("Gtkfmax[5] \(Gtkfmaxarray[5])")
            
		//Определение максимальной массы из массива и введенной массы
        (self.CalculatedTOW, self.AbsoluteTOW) = compare (Gtkfmaxarray)
		NSLog("Gabsolute=\(AbsoluteTOW)")
        
        //Определяем потребное отношение
		LogItem.title += ["7.25 CALC  V1/Vr REQUIRED"]
		LogItem.detail += [" "]
		self.VVrequired = f725toVV (self.ASDA, pG :self.AbsoluteTOW, pAOT :self.AOT, pELV :self.AltitudeForCalculation, pKSC :self.NFC, pSLP :self.Slope, pRUD :self.RUD, pHWND :self.Headwind)
        NSLog("VVreq=\(self.VVrequired)")
            
        //If VV is not forced and VVreq < 1
        if self.VVrequired < 1 && self.vvIsOne == false 
		{
            LogItem.title += ["V1/Vr < 1, SO TOW BY TODA WILL BE RECALC BASED ON"]
			LogItem.detail += ["\(self.VVrequired!)"]
			Gtkfmaxarray[2] = f724toweight (self.TODA, pVV :self.VVrequired!, pAOT :self.AOT, pELV :self.AltitudeForCalculation, pHWND :self.Headwind, pSLP :self.Slope, pRUD: self.RUD)
            NSLog("Gtkfmax[2] on VVreq= \(Gtkfmaxarray[2])")
            (self.CalculatedTOW, self.AbsoluteTOW) = compare (Gtkfmaxarray)
            NSLog("Gabsolute after VVreq=\(self.AbsoluteTOW)")
        }//if
            
        // Calculating minimal V1Vr
		LogItem.title += ["7.27 CALC MINIMAL V1/Vr"]
		LogItem.detail += [" "]
		self.VVmin = f727toVV (self.AbsoluteTOW, pAOT: self.AOT, pELV :self.AltitudeForCalculation, pKSC :self.NFC, pRUD: self.RUD)
        NSLog("V1Vr minimal=\(self.VVmin)")
            
        // Comparing VV, choosing maximal
        if self.VVmin >= self.VVrequired 
		{ 
			self.VVabsolute = self.VVmin! 
		}
        else 
		{ 
			self.VVabsolute = self.VVrequired! 
		}
        NSLog("V1VrTYPE= \(self.vvIsOne)")
        if self.vvIsOne == true
		{ 
			self.VVabsolute = 1
            
		}
        NSLog("V1Vr absolute = \(self.VVabsolute)")
			
        LogItem.title += ["V1/Vr in use"]
		LogItem.detail += ["\(self.VVabsolute)"]
            
		// Calculating G max on V1Vr absolute
		LogItem.title += ["7.25 CALC TOW BY ASDA"]
		LogItem.detail += [" "]
		Gtkfmaxarray[4] = f725toweight (self.ASDA, pVV: self.VVabsolute, pAOT: self.AOT, pELV: self.AltitudeForCalculation, pKSC: self.NFC, pSLP: self.Slope, pRUD: self.RUD, pHWND: self.Headwind )
        NSLog("Gtkfmaxarray[4]= \(Gtkfmaxarray[4])")
        (self.CalculatedTOW, self.AbsoluteTOW) = compare (Gtkfmaxarray)
        
        //If calculated TOW < 220 display error (7.27 out of range)
        if self.CalculatedTOW == 220 {
            let a = UIAlertView(title: "Warning", message: "Calculated TOW is less tnan 220t, further calculation is based on TOW = 220t", delegate: nil, cancelButtonTitle: "OK ")
            a.show()
        }
        
        NSLog("Gabsolute based on VVabsolute=\(self.AbsoluteTOW)")
			
		self.TOWTORA = Gtkfmaxarray[1]
		self.TOWTODA = Gtkfmaxarray[2]
		self.TOWASDA = Gtkfmaxarray[4]
		self.TOWSID = Gtkfmaxarray[3]
		self.TOWgrad = Gtkfmaxarray[0]
        
		LogItem.title += ["CALCULATION WILL BE BASED ON"]
		LogItem.detail += ["\(self.AbsoluteTOW)"]
        // Calculating of maximum weights completed. Start calculation other parameters based on Max weight (Gabsolute)
        
		// Checking Continiued takeoff distance
		LogItem.title += ["7.24 CALC DISTANCES, 3 ENGINES ARE OPERATING"]
		LogItem.detail += [" "]
		(self.Lrun3req, self.Dtakeoff3req)  = f724todist (self.AbsoluteTOW, pVV :self.VVabsolute, pAOT :self.AOT, pELV :self.AltitudeForCalculation, pHWND :self.Headwind, pSLP :self.Slope, pRUD: self.RUD)
        
        // Calculating Lrun, Lrunreq, Ldist, Ldistreq, nomogramm 7.23
		LogItem.title += ["7.23 CALC DISTANCES, 4 ENGINES ARE OPERATING"]
		LogItem.detail += [" "]
		(self.Lrun4, self.Lrun4req, self.Dtkf, self.Dtkfreq) = f723todist (self.AbsoluteTOW, pAOT :self.AOT, pELV :self.AltitudeForCalculation, pHWND :self.Headwind, pSLP :self.Slope, pRUD: self.RUD)
        
        //Calculating DSTOP
		LogItem.title += ["7.25 CALC DSTOP"]
		LogItem.detail += [" "]
		self.DStop = f725toDSTOP (self.AOT, pELV :self.AltitudeForCalculation, pG :self.AbsoluteTOW, pV1VR :self.VVabsolute, pHWND :self.Headwind, pRUD :self.RUD, pSLP:self.Slope, pKSC: self.NFC )
        
        // Calculating all speeds
                
        // Calculating V1 speed
        LogItem.title += ["CALC SPEEDS"]
		LogItem.detail += [" "]
		self.V1 = f726 (self.AbsoluteTOW, pVV: self.VVabsolute)
                
        if self.AbsoluteTOW <= 382 
		{ 
			self.Vr = 0.1318681 * self.AbsoluteTOW + 203.6263736
            if self.VVabsolute == 1 
			{
				self.Vr = self.V1
			}
        }
        else if self.AbsoluteTOW > 382 
		{ 
			self.Vr = 0.3157895 * self.AbsoluteTOW + 133.3684211
            if self.VVabsolute == 1 
			{
				self.Vr = self.V1
			}
        }
        LogItem.title += ["Vr"]
		LogItem.detail += ["\(self.Vr!)"]
		
        self.Votr = 0.1626096 * self.AbsoluteTOW + 211.0238219
        LogItem.title += ["Votr"]
		LogItem.detail += ["\(self.Votr!)"]
		
		if self.AbsoluteTOW <= 362 
		{ 
			self.V2 = 0.1604938 * self.AbsoluteTOW + 229.9012346 
		}
        else if self.AbsoluteTOW > 362 
		{ 
		self.V2 = 0.2241379 * self.AbsoluteTOW + 206.862069 
		}
        LogItem.title += ["V2"]
		LogItem.detail += ["\(self.V2!)"]
		       
        if self.AbsoluteTOW <= 280 
		{ 
			self.V2n = 0.1604938 * self.AbsoluteTOW + 229.9012346 
		}
        else if self.AbsoluteTOW > 280 
		{ 
			self.V2n = 0.4071429 * self.AbsoluteTOW + 161 
		}
        LogItem.title += ["V2n"]
		LogItem.detail += ["\(self.V2n!)"]
		        
        if self.AbsoluteTOW <= 237 
		{ 
			self.V3015 = 0.1604938 * self.AbsoluteTOW + 229.9012346 
		}
        else if self.AbsoluteTOW > 237 
		{ 
			self.V3015 = 0.4972678 * self.AbsoluteTOW + 151.147541 
		}
		LogItem.title += ["V 30-15"]
		LogItem.detail += ["\(self.V3015!)"]
		
        self.V152 = 0.5681818 * self.AbsoluteTOW + 171.3636364
		LogItem.title += ["V 15-2"]
		LogItem.detail += ["\(self.V152!)"]

        self.V20 = 0.6 * self.AbsoluteTOW + 200
		LogItem.title += ["V 2-0"]
		LogItem.detail += ["\(self.V20!)"]
		
        self.V4 = 0.6272727 * self.AbsoluteTOW + 219.5454545
		LogItem.title += ["V4"]
		LogItem.detail += ["\(self.V4!)"]
		
        if self.AbsoluteTOW <= 370 
		{ 
			self.Vcircle = 450 
		}
        else if self.AbsoluteTOW > 370 
		{ 
			self.Vcircle =  0.6272727 * self.AbsoluteTOW + 219.5454545 
		}
        LogItem.title += ["V circle"]
		LogItem.detail += ["\(self.Vcircle!)"]
        
        if let x = f718RUD (self.AOT, pELV :self.AltitudeForCalculation, pHWND :self.Headwind, pWeight:self.AbsoluteTOW) {
            self.RUD = x
        }
        
		       
//        return (nil, Gtkfcalculated, Gtkfabsolute, vtkfHeadwind, -vtkfCrosswind, Gtkfmaxarray[0], Gtkfmaxarray[1], Gtkfmaxarray[2], Gtkfmaxarray[3], Gtkfmaxarray[4], vVVabsolute, vVVmin, vRCTOD, vV1, vVr, vVotr, vV2, vV2n, vV15n, vV15k, vVub, vV4, vVcircle, vLrun, vLrunreq, vLrun3req, vLdist, vLdistreq, vDstop)
        
        }//maintkf
    
    func flightLevels()
    {
		LogItem.title += ["CALC FLIGHT LEVELS"]
		LogItem.detail += [" "]
		
        let std = -22.2797927 * self.AbsoluteTOW + 17255.9585
        NSLog("STD = \(std)")
		LogItem.title += ["ISA H"]
		LogItem.detail += ["\(std)"]
		
        let std10 = -1.2001692e-6 * pow (self.AbsoluteTOW, 4) + 0.0014418 * pow(self.AbsoluteTOW, 3) - 0.6611653 * pow (self.AbsoluteTOW,2) + 109.626376 * self.AbsoluteTOW + 7613.92432
        NSLog("STD10 = \(std10)")
        LogItem.title += ["ISA+10 H"]
		LogItem.detail += ["\(std10)"]
		
		let std20 = -31.0526316 * self.AbsoluteTOW + 18610.526316
        NSLog("STD20 = \(std20)")
		LogItem.title += ["ISA+20 H"]
		LogItem.detail += ["\(std20)"]
		
        let std30 = -30.5 * self.AbsoluteTOW + 16700
		NSLog("STD30 = \(std30)")
        LogItem.title += ["ISA+30 H"]
		LogItem.detail += ["\(std30)"]
		
        switch self.deltaISA 
		{
        case let x where  x <= 0: self.FL4 = round (std * 0.032808399)
        case let x where (x > 0) && (x <= 10): self.FL4 = round ((std + (deltaISA) * (std10 - std) / 10) * 0.032808399)
        case let x where (x > 10) && (x <= 20): self.FL4 = round ((std10 + (deltaISA - 10) * (std20 - std10)/10) * 0.032808399)
        case let x where (x > 20) && (x <= 30): self.FL4 = round ((std20 + (deltaISA - 20) * (std30 - std20)/10) * 0.032808399)
        default: self.FL4 = 0
        }
        LogItem.title += ["FL max on 4 engines"]
		LogItem.detail += ["\(self.FL4!)"]
		
		
        let std3 = -0.0362762 * pow (self.AbsoluteTOW,2) - 11.8479 * self.AbsoluteTOW + 13541.6083916
        NSLog("3STD = \(std3)")
		LogItem.title += ["ISA H (3eng)"]
		LogItem.detail += ["\(std3)"]
		
        let std103 = -37.5 * self.AbsoluteTOW + 16500
        NSLog("3STD10 = \(std103)")
		LogItem.title += ["ISA+10 H (3eng)"]
		LogItem.detail += ["\(std103)"]
		
        let std203 = -44.7222 * self.AbsoluteTOW + 16994.4
        NSLog("3STD20 = \(std203)")
		LogItem.title += ["ISA+20 H (3eng)"]
		LogItem.detail += ["\(std203)"]
		
        let std303 = -48.0769231 * self.AbsoluteTOW + 15865.385
        NSLog("3STD30 = \(std303)")
		LogItem.title += ["ISA+30 H (3eng)"]
		LogItem.detail += ["\(std303)"]
		
        
        switch self.deltaISA 
		{
        case let x where  x <= 0: self.FL3 = round (std3 * 0.032808399)
        case let x where (x > 0) && (x <= 10): self.FL3 = round ((std3 + (deltaISA) * (std103 - std3) / 10) * 0.032808399)
        case let x where (x > 10) && (x <= 20): self.FL3 = round ((std103 + (deltaISA - 10) * (std203 - std103)/10) * 0.032808399)
        case let x where (x > 20) && (x <= 30): self.FL3 = round ((std203 + (deltaISA - 20) * (std303 - std203)/10) * 0.032808399)
        default: self.FL3 = 0
        }
		
		if self.FL3 < 0 
		{
			self.FL3 = 0
		}
		
		LogItem.title += ["FL max on 3 engines"]
		LogItem.detail += ["\(self.FL3!)"]
        LogItem.title += [" "]
        LogItem.detail += [" "]

     
    }
    
    
    //Gradients calculations
    func Gradients ()
    {
        
		LogItem.title += ["CALC GRADIENTS"]
		LogItem.detail += [" "]
        //----------------GRAD3-120 7.12-7.14
        switch self.Time {
        case 6:
            var v714column1 :Double = 0
            if let n714011 = getNom2D(714011)
			{
                NSLog("7.14 : 714011 loaded")
                if let x = n714011.getFByValues(self.AOT, x2: self.AltitudeForCalculation)
                    {
                        v714column1 = x
                        if self.deltaISA > 30
                            {
								LogItem.warningTitle += ["7.14 Delta T ISA Deviation, double check"]
								LogItem.warningDetail += [" "] 
							}
							NSLog("7.14 : column 1 GRAD=\(v714column1)")
                            LogItem.title += ["7.14/1"]
							LogItem.detail += ["\(v714column1)"]
                            }
                            else
                            {
                            NSLog("7.14 : column 1 error")
                            }
                    }
                    else
                    {
                    NSLog("7.14 : 714011 not found")
                    }
                
                var v714column2 :Double = 0
                if let n714021 = getNom2D(714021)
                {
                    NSLog("7.14 : 714021 loaded")
                    if let x = n714021.getFByValues(self.AbsoluteTOW, x2: v714column1)
                    {
                        v714column2 = x
                        if v714column2 > 19 || v714column2 < 0.2
                        {
                            NSLog("7.14 : column 2 GRAD out of range [0-19]" )
                        }
                        NSLog("7.14 : column 2 GRAD=\(v714column2)")
                        
                        LogItem.title += ["7.14/2"]
                        LogItem.detail += ["\(v714column2)"]
                    }
                    else
                    {
                        NSLog("7.14 : column 2 error")
                    }
                }
                else
                {
                    NSLog("7.14 : 714021 not found")
                }
               
               var v714column3 :Double = 0
               if let n714031 = getNom2D(714031)
               {
                    NSLog("7.14 : 714031 loaded")
                    if let x = n714031.getFByValues(self.Headwind, x2: v714column2)
                    {
                        v714column3 = x
                        if v714column3 > 21 || v714column3 < 0.2
                        {
                            NSLog("7.14 : column 3 GRAD out of range [0-21]" )
                        }
                        NSLog("7.14 : column 3 GRAD=\(v714column3)")
                    
                        LogItem.title += ["7.14/3"]
                        LogItem.detail += ["\(v714column3)"]
                    }
                    else
                    {
                        NSLog("7.14 : column 3 error")
                    }
                }
                else
                {
                    NSLog("7.14 : 714031 not found")
                }
               
               
               if let n714041 = getNom2D(714041)
               {
                    NSLog("7.14 : 714041 loaded")
                    if let x = n714041.getFByValues(self.RUD, x2: v714column3)
                    {
                        self.Grad3120 = x
                        if self.Grad3120 > 21 || self.Grad3120 < 0.2
                        {
                            NSLog("7.14 : column 4 GRAD out of range [0-21]" )}
                            NSLog("7.14 : column 4 GRAD=\(self.Grad3120)")
                            LogItem.title += ["7.14/4 Gradient 3 eng 120 m"]
                            LogItem.detail += ["\(self.Grad3120!)"]
                        }
                        else
                        {
                            NSLog("7.14 : column 4 error")
                        }
                }
                else
                {
                    NSLog("7.14 : 714041 not found")
                }
            
        case 4:
            
            var v713column1 :Double = 0
            if let n713011 = getNom2D(713011)
            {
                NSLog("7.13 : 713011 loaded")
                if let x = n713011.getFByValues(self.AOT, x2: self.AltitudeForCalculation)
                {
                    v713column1 = x
                    if self.deltaISA > 30
                    {
                        LogItem.warningTitle += ["7.13 Delta T ISA Deviation, double check"]
                        LogItem.warningDetail += [" "]
                    }
                    NSLog("7.13 : column 1 GRAD=\(v713column1)")
                    LogItem.title += ["7.13/1"]
                    LogItem.detail += ["\(v713column1)"]
                }
                else
                {
                    NSLog("7.13 : column 1 error")
                }
            }
            else
            {
                NSLog("7.13 : 713011 not found")
            }
            
            var v713column2 :Double = 0
            if let n713021 = getNom2D(713021)
            {
                NSLog("7.13 : 713021 loaded")
                if let x = n713021.getFByValues(self.AbsoluteTOW, x2: v713column1)
                {
                    v713column2 = x
                    if v713column2 > 19 || v713column2 < 0.2
                    {
                        NSLog("7.13 : column 2 GRAD out of range [0-19]" )
                    }
                    NSLog("7.13 : column 2 GRAD=\(v713column2)")
                    LogItem.title += ["7.13/2"]
                    LogItem.detail += ["\(v713column2)"]
                }
                else
                {
                    NSLog("7.13 : column 2 error")
                }
            }
            else
            {
                NSLog("7.13 : 713021 not found")
            }
            
            var v713column3 :Double = 0
            if let n713031 = getNom2D(713031)
            {
                NSLog("7.13 : 713031 loaded")
                if let x = n713031.getFByValues(self.Headwind, x2: v713column2)
                {
                    v713column3 = x
                    if v713column3 > 21 || v713column3 < 0.2
                    {
                        NSLog("7.13 : column 3 GRAD out of range [0-21]" )
                    }
                    NSLog("7.13 : column 3 GRAD=\(v713column3)")
                    LogItem.title += ["7.13/3"]
                    LogItem.detail += ["\(v713column3)"]
                }
                else
                {
                    NSLog("7.13 : column 3 error")
                }
            }
            else
            {
                NSLog("7.13 : 713031 not found")
            }
            
            
            if let n713041 = getNom2D(713041)
            {
                NSLog("7.13 : 713041 loaded")
                if let x = n713041.getFByValues(self.RUD, x2: v713column3)
                {
                    self.Grad3120 = x
                    if self.Grad3120 > 21 || self.Grad3120 < 0.2
                    {
                        NSLog("7.13 : column 4 GRAD out of range [0-21]" )
                    }
                    NSLog("7.13 : column 4 GRAD=\(self.Grad3120)")
                    LogItem.title += ["7.13/4 Gradient 3 eng 120 m"]
                    LogItem.detail += ["\(self.Grad3120!)"]
                }
                else
                {
                    NSLog("7.13 : column 4 error")
                }
            }
            else
            {
                NSLog("7.13 : 713041 not found")
            }
            
        case 2:
            
            var v712column1 :Double = 0
            if let n712011 = getNom2D(712011)
            {
                NSLog("7.12 : 712011 loaded")
                if let x = n712011.getFByValues(self.AOT, x2: self.AltitudeForCalculation)
                {
                    v712column1 = x
                    
                    //let margin = 0.1538462 * self.AOT + 0.6769231
                    
                    if self.deltaISA > 30
                    {
                        LogItem.warningTitle += ["7.12 Delta T ISA Deviation, double check"]
                        LogItem.warningDetail += [" "]
                    }
                    NSLog("7.12 : column 1 GRAD=\(v712column1)")
                    LogItem.title += ["7.12/1"]
                    LogItem.detail += ["\(v712column1)"]
                }
                else
                {
                NSLog("7.12 : column 1 error")
                }
            }
            else
            {
            NSLog("7.12 : 712011 not found")
            }
            
            var v712column2 :Double = 0
            if let n712021 = getNom2D(712021)
            {
                NSLog("7.12 : 712021 loaded")
                if let x = n712021.getFByValues(self.AbsoluteTOW, x2: v712column1)
                {
                    v712column2 = x
                    if v712column2 > 19 || v712column2 < 0.2
                    {
                        NSLog("7.12 : column 2 GRAD out of range [0-19]" )
                    }
                    NSLog("7.12 : column 2 GRAD=\(v712column2)")
                    LogItem.title += ["7.12/2"]
                    LogItem.detail += ["\(v712column2)"]
                }
                else
                {
                    NSLog("7.12 : column 2 error")
                }
            }
            else
            {
                NSLog("7.12 : 712021 not found")
            }
            
            var v712column3 :Double = 0
            if let n712031 = getNom2D(712031)
            {
                NSLog("7.12 : 712031 loaded")
                if let x = n712031.getFByValues(self.Headwind, x2: v712column2)
                {
                    v712column3 = x
                    if v712column3 > 21 || v712column3 < 0.2
                    {
                        NSLog("7.12 : column 3 GRAD out of range [0-21]" )
                    }
                    NSLog("7.12 : column 3 GRAD=\(v712column3)")
                    LogItem.title += ["7.12/3"]
                    LogItem.detail += ["\(v712column3)"]
                }
                else
                {
                    NSLog("7.12 : column 3 error")
                }
            }
            else
            {
                NSLog("7.12 : 712031 not found")
            }
            
            
            if let n712041 = getNom2D(712041)
            {
                NSLog("7.12 : 712041 loaded")
                if let x = n712041.getFByValues(self.RUD, x2: v712column3)
                {
                    self.Grad3120 = x
                    if self.Grad3120 > 21 || self.Grad3120 < 0.2
                    {
                        NSLog("7.12 : column 4 GRAD out of range [0-21]" )
                    }
                    NSLog("7.12 : column 4 GRAD=\(self.Grad3120)")
                    LogItem.title += ["7.12/4 Gradient 3 eng 120 m"]
                    LogItem.detail += ["\(self.Grad3120!)"]
                }
                else
                {
                    NSLog("7.12 : column 4 error")
                }
            }
            else
            {
                NSLog("7.12 : 712041 not found")
            }

            
        default: self.Grad3120 = 99
        
    }//End of switch, End of Grad3-120 7.12-7.14
        
    //-----------------------GRAD4-120m 7.16
        
        var v716column1 :Double = 0
        if let n716011 = getNom2D(716011)
        {
            NSLog("7.16 : 716011 loaded")
            if let x = n716011.getFByValues(self.AOT, x2: self.AltitudeForCalculation)
            {
                v716column1 = x
                if self.deltaISA > 30
                {
                    LogItem.warningTitle += ["7.16 Delta T ISA Deviation, double check"]
                    LogItem.warningDetail += [" "]
                }
                NSLog("7.16 : column 1 GRAD=\(v716column1)")
                LogItem.title += ["7.16/1"]
                LogItem.detail += ["\(v716column1)"]
            }
            else
            {
             NSLog("7.16 : column 1 error")
            }
        }
        else
        {
         NSLog("7.16 : 716011 not found")
        }
        
        var v716column2 :Double = 0
        if let n716021 = getNom2D(716021)
        {
            NSLog("7.16 : 716021 loaded")
            if let x = n716021.getFByValues(self.AbsoluteTOW, x2: v716column1)
            {
                v716column2 = x
                if v716column2 > 19 || v716column2 < 0.2
                {
                    NSLog("7.16 : column 2 GRAD out of range [0-19]" )
                }
                NSLog("7.16 : column 2 GRAD=\(v716column2)")
                LogItem.textmsg = LogItem.textmsg + "\n7.16/2 = \(v716column2)"
                LogItem.title += ["7.16/2"]
                LogItem.detail += ["\(v716column2)"]
            }
            else
            {
                NSLog("7.16 : column 2 error")
            }
        }
        else
        {
            NSLog("7.16 : 716021 not found")
        }
        
        var v716column3 :Double = 0
        if let n716031 = getNom2D(716031)
        {
            NSLog("7.16 : 716031 loaded")
            if let x = n716031.getFByValues(self.Headwind, x2: v716column2)
            {
                v716column3 = x
                if v716column3 > 21 || v716column3 < 0.2
                {
                    NSLog("7.16 : column 3 GRAD out of range [0-21]" )
                }
                NSLog("7.16 : column 3 GRAD=\(v716column3)")
                LogItem.textmsg = LogItem.textmsg + "\n7.16/3 = \(v716column3)"
                LogItem.title += ["7.16/3"]
                LogItem.detail += ["\(v716column3)"]
            }
            else
            {
                NSLog("7.16 : column 3 error")
            }
        }
        else
        {
            NSLog("7.16 : 716031 not found")
        }
        
        
        if let n716041 = getNom2D(716041)
        {
            NSLog("7.16 : 716041 loaded")
            if let x = n716041.getFByValues(self.RUD, x2: v716column3)
            {
                self.Grad4120 = x
                if self.Grad4120 > 21 || self.Grad4120 < 0.2
                {
                    NSLog("7.16 : column 4 GRAD out of range [0-21]" )
                }
                NSLog("7.16 : column 4 GRAD=\(self.Grad4120)")
                LogItem.title += ["7.16/4 Gradient 4 eng 120 m"]
                LogItem.detail += ["\(self.Grad4120!)"]
            }
            else
            {
                NSLog("7.16 : column 4 error")
            }
        }
        else
        {
            NSLog("7.16 : 716041 not found")
        }

        //-----------------------GRAD3-400m 7.15
        
        var v715column1 :Double = 0
        if let n715011 = getNom2D(715011)
        {
            NSLog("7.15 : 715011 loaded")
            if let x = n715011.getFByValues(self.AOT, x2: self.AltitudeForCalculation)
            {
                v715column1 = x
                
                //let margin = 0.1115385 * self.AOT + 0.1192308
                
                if self.deltaISA > 30
                {
                    LogItem.warningTitle += ["7.15 Delta T ISA Deviation, double check"]
                    LogItem.warningDetail += [" "]
                }
                NSLog("7.15 : column 1 GRAD=\(v716column1)")
                LogItem.title += ["7.15/1"]
                LogItem.detail += ["\(v715column1)"]
            }
            else
            {
             NSLog("7.15 : column 1 error")
            }
        }
        else
        {
         NSLog("7.15 : 715011 not found")
        }
        
        var v715column2 :Double = 0
        if let n715021 = getNom2D(715021)
        {
            NSLog("7.15 : 715021 loaded")
            if let x = n715021.getFByValues(self.AbsoluteTOW, x2: v715column1)
            {
                v715column2 = x
                if v715column2 > 14.5 || v715column2 < 0.2
                {
                    NSLog("7.15 : column 2 GRAD out of range [0-14.5]" )
                }
                NSLog("7.15 : column 2 GRAD=\(v715column2)")
                LogItem.title += ["7.15/2"]
                LogItem.detail += ["\(v715column2)"]

            }
            else
            {
                NSLog("7.15 : column 2 error")
            }
        }
        else
        {
            NSLog("7.15 : 715021 not found")
        }
        
        
        if let n715031 = getNom2D(715031)
        {
            NSLog("7.15 : 715031 loaded")
            if let x = n715031.getFByValues(self.Headwind, x2: v715column2)
            {
                self.Grad3400 = x
                if self.Grad3400 > 14.5 || self.Grad3400 < 0.2
                {
                    NSLog("7.15 : column 3 GRAD out of range [0-14.5]" )
                }
                NSLog("7.15 : column 3 GRAD=\(self.Grad3400)")
                LogItem.title += ["7.15/3 Gradient 3 eng 400"]
                LogItem.detail += ["\(self.Grad3400!)"]

            }
            else
            {
                NSLog("7.15 : column 3 error")
            }
        }
        else
        {
            NSLog("7.15 : 715031 not found")
        }

        //-----------------------GRAD4-400m 7.17
        
        var v717column1 :Double = 0
        if let n717011 = getNom2D(717011)
        {
            NSLog("7.17 : 717011 loaded")
            if let x = n717011.getFByValues(self.AOT, x2: self.AltitudeForCalculation)
            {
                v717column1 = x
                
                if self.deltaISA > 30
                {
                    LogItem.warningTitle += ["7.17 Delta T ISA Deviation, double check"]
                    LogItem.warningDetail += [" "]
                }
                NSLog("7.17 : column 1 GRAD=\(v716column1)")
                LogItem.title += ["7.17/1"]
                LogItem.detail += ["\(v717column1)"]
            }
            else
            {
             NSLog("7.17 : column 1 error")
            }
        }
        else
        {
         NSLog("7.17 : 717011 not found")
        }
        
        var v717column2 :Double = 0
        if let n717021 = getNom2D(717021)
        {
            NSLog("7.17 : 717021 loaded")
            if let x = n717021.getFByValues(self.AbsoluteTOW, x2: v717column1)
            {
                v717column2 = x
                if v717column2 > 14.5 || v717column2 < 0.2
                {
                    NSLog("7.17 : column 2 GRAD out of range [0-14.5]" )
                }
                NSLog("7.17 : column 2 GRAD=\(v717column2)")
                LogItem.title += ["7.17/2"]
                LogItem.detail += ["\(v717column2)"]
            }
            else
            {
                NSLog("7.17 : column 2 error")
            }
        }
        else
        {
            NSLog("7.17 : 717021 not found")
        }
        
        
        if let n717031 = getNom2D(717031)
        {
            NSLog("7.17 : 717031 loaded")
            if let x = n717031.getFByValues(self.Headwind, x2: v717column2)
            {
                self.Grad4400 = x
                if self.Grad4400 > 14.5 || self.Grad4400 < 0.2
                {
                    NSLog("7.17 : column 3 GRAD out of range [0-14.5]" )
                }
                NSLog("7.17 : column 3 GRAD=\(self.Grad4400)")
                LogItem.title += ["7.17/3 Gradient 4 eng 400"]
                LogItem.detail += ["\(self.Grad4400!)"]
            }
            else
            {
                NSLog("7.17 : column 3 error")
            }
        }
        else
        {
            NSLog("7.17 : 717031 not found")
        }

        
    }
        
        //----------------------------------------------------------------------------------------------
        

}
