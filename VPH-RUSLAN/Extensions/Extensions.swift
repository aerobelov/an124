//
//  commonFunctions.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 07.05.17.
//  Copyright © 2017 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation
import SystemConfiguration

let intFormatter = NumberFormatter()
let oneFormatter = NumberFormatter()
let weightFormatter = NumberFormatter()
let distanceFormatter = NumberFormatter()
let twoFormatter = NumberFormatter()


let dec = NSLocale.current.decimalSeparator! as String
let build: String = version()


extension String {
    static let numFormatter = NumberFormatter()
    var DoubleValue :Double? {
        if let result = String.numFormatter.number(from: self) {
            return Double(truncating: result)
        } else {
            return nil
        }
    }
}


enum fractions {
    case zero
    case two
    case one
    case weight
    case distance
}

func version() -> String {
    let dictionary = Bundle.main.infoDictionary!
    let version = dictionary["CFBundleShortVersionString"] as! String
    //let build = dictionary["CFBundleVersion"] as! String
    return "\(version)"
}

protocol Utilities {
    
}

extension NSObject: Utilities {
    
    enum ReachabilityStatus {
        case notReachable
        case reachableViaWWAN
        case reachableViaWiFi
    }
    
    var currentReachabilityStatus: ReachabilityStatus {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return .notReachable
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return .notReachable
        }
        
        if flags.contains(.reachable) == false {
            // The target host is not reachable.
            return .notReachable
        }
        else if flags.contains(.isWWAN) == true {
            // WWAN connections are OK if the calling application is using the CFNetwork APIs.
            return .reachableViaWWAN
        }
        else if flags.contains(.connectionRequired) == false {
            // If the target host is reachable and no connection is required then we'll assume that you're on Wi-Fi...
            return .reachableViaWiFi
        }
        else if (flags.contains(.connectionOnDemand) == true || flags.contains(.connectionOnTraffic) == true) && flags.contains(.interventionRequired) == false {
            // The connection is on-demand (or on-traffic) if the calling application is using the CFSocketStream or higher APIs and no [user] intervention is needed
            return .reachableViaWiFi
        }
        else {
            return .notReachable
        }
    }
    
}





//----------------------------------------------------------------------------------------------


