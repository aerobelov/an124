//
//  TkfExtensions.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 24.06.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController: ButtonBackProtocol {
    
    func setTitle(for sender: UIButton, with text: String) {
        sender.setTitle(text, for: .normal)
    }
  
}
