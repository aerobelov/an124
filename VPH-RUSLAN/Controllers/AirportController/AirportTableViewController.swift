//
//  AirportTableViewController.swift
//  IL76
//
//  Created by Pavel on 16/09/2016.
//  Copyright © 2016 Pavel. All rights reserved.
//

import UIKit

class airportTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title:UILabel!
    @IBOutlet weak var subtitle:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}


class AirportTableViewController: UITableViewController , UISearchResultsUpdating {

    @IBOutlet weak var airportSelectTable :UITableView!
    
    var resultSearchController = UISearchController()
    var filteredIcao:[String] = []
    var selectedCodeIndex: Int = 0
    var number: NSNumber = 0
    
    var infoToSend: [Int: String] = [:]
    var delegate: TransferIcaoCodeProtocol?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        airportSelectTable.dataSource = self
        airportSelectTable.delegate = self
        
        self.resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.obscuresBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            self.airportSelectTable.tableHeaderView = controller.searchBar
            return controller
        })()
        airportSelectTable.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if (self.resultSearchController.isActive) {
            return filteredIcao.count
        } else {
            return databaseHandler.shared.icaoCode.count
        }

    }
    
    

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = airportSelectTable.dequeueReusableCell(withIdentifier: "airportCell", for: indexPath) as! airportTableViewCell 
        let row = (indexPath as NSIndexPath).row
        
        if(resultSearchController.isActive) {
            
            cell.title.text = filteredIcao[row]
            cell.subtitle.text = databaseHandler.shared.airportName[findIndex(filteredIcao[row])]
        }
        else
        {
            cell.title.text = databaseHandler.shared.icaoCode[row]
            cell.subtitle.text = databaseHandler.shared.airportName[row]
        }
       
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        var selectedIcao: String
        if (resultSearchController.isActive) {
            selectedIcao = databaseHandler.shared.icaoCode[findIndex(filteredIcao[(indexPath as NSIndexPath).row])]
            self.dismiss(animated: true, completion: {})
            
        }
        else   {
            selectedIcao = databaseHandler.shared.icaoCode[(indexPath as NSIndexPath).row]
        }
        
        super.dismiss(animated: true, completion: {})
        
        if let delegate = delegate {
            delegate.setIcao(code: selectedIcao )
            delegate.queryRunways()
        }
        
    }
    
    func updateSearchResults(for searchController: UISearchController)
    {
        filteredIcao.removeAll(keepingCapacity: false)
        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchController.searchBar.text!)
        let array = (databaseHandler.shared.icaoCode as NSArray).filtered(using: searchPredicate)
        filteredIcao = array as! [String]
        self.airportSelectTable.reloadData()
    }
    
    //finding Airport Name index by Icao code
    func findIndex(_ icao: String) -> Int {
        var res:Int = 0
        for (index, value) in databaseHandler.shared.icaoCode.enumerated() {
            if (icao == value) { res = index ; break }
        }
        return res
    }
    

    
    
}
