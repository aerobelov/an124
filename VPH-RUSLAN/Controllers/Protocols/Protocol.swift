//
//  Protocol.swift
//  VPH-AN124
//
//  Created by Pavel Belov on 15.04.17.
//  Copyright © 2017 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation
import FMDB

protocol TransferIcaoCodeProtocol {
    func setIcao (code: String)
    func queryRunways ()
}

protocol ExecuteQueryProtocol {
    func executeQuery(query: String) -> FMResultSet
}

protocol DatabaseDataStatusProtocol {
    var db: FMDatabase? {get}
    var airac: Int {get}
    var revision: Int {get}
    var startDate: String {get}
    var endDate: String {get}
}

protocol TransferRunwayIdentificatorProtocol {
    func setRunway(id: String)
    func queryRunwayData()
}

protocol DownloadCompleteProtocol {
    func settingProgress(value: Float)
    func compareVersions(cNumber: Int)
    func displayDatabaseStatus()
    var controlNumber: Int? { get set }
}

protocol ReloadDatabaseProtocol {
    func reload()
}

protocol Updater {
    func getJsonFromSao(completion: @escaping ()->Void)
    var remoteControlNumber: Int? {get}
}

protocol versionCompareProtocol {
    var oldIndex: Int? {get set}
    var newIndex: Int? {get set}
    func compare(old: Int, new: Int) -> Bool
}

protocol CanDisplayDatabaseStatus {
    func displayDatabaseStatus (status: String, cycle: String, start: String, end: String, rev: String) -> Void
    var statusLabel: UILabel! {get set}
    var airac: UILabel! {get set}
    var startDate: UILabel! {get set}
    var endDate: UILabel! {get set}
    var revision: UILabel! {get set}
}

protocol CanDisplayUpdatesAvailable {
    func displayUpdatesAvailable()
}

protocol CanDisplayAlert {
    func displayAlert(AlertTitle title:String, AlertText message:String, sender: UIViewController)
}

protocol ButtonBackProtocol {
    func setTitle(for sender: UIButton, with text: String)
}

protocol ControllerCommonMethodsProtocol {
    func setTextValue(_ field: UITextField, n: Double?, precision: formatterType)
}
