//
//  SelectorViewController.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 09.06.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import UIKit

class SelectorViewController: UIViewController, SelectSheetControllerProtocol {
    
    @IBOutlet weak var minLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var maxLabel: UILabel!
    @IBOutlet weak var increase: UIButton!
    @IBOutlet weak var decrease: UIButton!
    
    weak var recepient: UIButton?
    var buttonBackController: ButtonBackProtocol?
    
    
    var data: SelectSheetDataProtocol?
    var controlledButton: UIButton!
    var step: Float = 0.1
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        if let data = data {
            self.maxLabel.text = self.data!.format(data.maximum)
            self.minLabel.text = self.data!.format(data.minimum)
            valueLabel.text = self.data!.indicatedValue
            titleLabel.text = self.data!.title
            slider?.minimumValue = Float(self.data!.minimum)
            slider?.maximumValue = Float(self.data!.maximum)
            self.step = Float(self.data!.step)
            slider.value = Float(self.data!.value)
        }
    }
    
    func initSegment() -> Void {
        print("")
    }

    @IBAction func saveAndClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        buttonBackController?.setTitle(for: recepient!, with: valueLabel.text ?? "")
    }
    
    @IBAction func sliderValueChanged(_ sender: Any) {
        let value = slider.value
        let valueraunded  = round(value / step) * step
        updateSliderAndLabel(valueraunded)
    }
    
    @IBAction func increase(_ sender: UIButton) {
           plus()
       }
       
       @IBAction func decrease(_ sender: UIButton) {
           minus()
       }
       
       func plus() {
           let value = Double( slider.value + step )
           if let max = data?.maximum, value <= max {
               updateSliderAndLabel(Float(value))
           }
       }
       
       func minus() {
           let value = Double( slider.value - step )
           if let min = data?.minimum, value >= min {
               updateSliderAndLabel(Float(value))
           }
       }
    
    func updateSliderAndLabel(_ value: Float) {
        slider.setValue(value, animated: true)
        valueLabel.text = self.data?.format(Double(value))
    }
}
