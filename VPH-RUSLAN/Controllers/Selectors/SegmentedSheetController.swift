//
//  ViewController.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 18.09.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import UIKit

class SegmentedSheetViewController: UIViewController, SelectSheetControllerProtocol{
    
    
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var segment: UISegmentedControl!
    weak var recepient: UIButton?
    var buttonBackController: ButtonBackProtocol?
    var data: SelectSheetDataProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSegment()
    }
    
    func initSegment() -> Void {
        if self.data != nil {
            self.segment.replaceSegments(segments: self.data!.values)
            self.segment.selectedSegmentIndex = findIndex(val: self.data!.indicatedValue, from: self.data!.values)
            valueLabel.text = self.data!.indicatedValue
            titleLabel.text = self.data!.title
        }
    }
    

    @IBAction func segmentChanged(_ sender: Any) {
        self.valueLabel.text = self.data?.values[self.segment.selectedSegmentIndex]
    }
    
    @IBAction func saveAndClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        buttonBackController?.setTitle(for: recepient!, with: valueLabel.text ?? "")
    }
    
    func findIndex(val: String, from valueArray: [String]) -> Int {
        var myindex = 0
        for (index, item) in valueArray.enumerated() {
            if item == val {
                myindex = index
                break
            }
        }
        return myindex
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UISegmentedControl {
    func replaceSegments(segments: Array<String>) {
        self.removeAllSegments()
        for segment in segments {
            self.insertSegment(withTitle: segment, at: self.numberOfSegments, animated: false)
        }
    }
}
