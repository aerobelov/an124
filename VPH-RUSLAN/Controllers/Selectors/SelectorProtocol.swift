//
//  SelectorProtocol.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 09.06.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation
import UIKit



protocol SelectSheetControllerProtocol: AnyObject {
    var valueLabel: UILabel! { get set }
    var data: SelectSheetDataProtocol? { get set }
    var buttonBackController: ButtonBackProtocol? {get set}
    var recepient: UIButton? { get set }
    func saveAndClose(_ sender: UIButton) -> Void
    func initSegment() -> Void
}

protocol ButtonProtocol {
    var button: UIButton {get set}
}
