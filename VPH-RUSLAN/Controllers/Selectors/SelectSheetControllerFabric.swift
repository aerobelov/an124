//
//  SelectSheetFabric.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 19.09.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation

class SelectSheetControllerFabric {
    
    func createSheetController(from tag: Int, with text: String) -> SelectSheetControllerProtocol {
        
        var vc: SelectSheetControllerProtocol?
        let value = Double(text)
        
        switch tag {
        
            case 10:
                vc = SelectorViewController(nibName: "SelectorViewController", bundle: nil)
                vc?.data = SelectorSheetData(value: value ?? 0, maximum: 400, minimum: 200, title: "ATOW", step: 1, formatterType: .one)
            case 11:
                vc = SelectorViewController(nibName: "SelectorViewController", bundle: nil)
                vc?.data = SelectorSheetData(value: value ?? 0, maximum: 359, minimum: 0, title: "Runway Heading", step: 10, formatterType: .int)
            case 12:
                vc = SelectorViewController(nibName: "SelectorViewController", bundle: nil)
                vc?.data = SelectorSheetData(value: value ?? 0, maximum: 5000, minimum: 1000, title: "TORA", step: 20, formatterType: .int)
            case 13:
                vc = SelectorViewController(nibName: "SelectorViewController", bundle: nil)
                vc?.data = SelectorSheetData(value: value ?? 0, maximum: 5000, minimum: 1000, title: "TODA", step: 20, formatterType: .int)
            case 14:
                vc = SelectorViewController(nibName: "SelectorViewController", bundle: nil)
                vc?.data = SelectorSheetData(value: value ?? 0, maximum: 5000, minimum: 1000, title: "ASDA", step: 20, formatterType: .int)
            case 15:
                vc = SelectorViewController(nibName: "SelectorViewController", bundle: nil)
                vc?.data = SelectorSheetData(value: value ?? 0, maximum: 17, minimum: 3, title: "SID gradient", step: 0.1, formatterType: .one)
            case 16:
                vc = SelectorViewController(nibName: "SelectorViewController", bundle: nil)
                vc?.data = SelectorSheetData(value: value ?? 0, maximum: 3000, minimum: 0, title: "Elevation", step: 5, formatterType: .int)
            case 17:
                vc = SelectorViewController(nibName: "SelectorViewController", bundle: nil)
                vc?.data = SelectorSheetData(value: value ?? 0, maximum: 1036, minimum: 695, title: "QNH", step: 1, formatterType: .int)
            case 18:
                vc = SelectorViewController(nibName: "SelectorViewController", bundle: nil)
                vc?.data = SelectorSheetData(value: value ?? 0, maximum: 45, minimum: -45, title: "Temperature", step: 1, formatterType: .int)
            case 19:
                vc = SelectorViewController(nibName: "SelectorViewController", bundle: nil)
                vc?.data = SelectorSheetData(value: value ?? 0, maximum: 2, minimum: -2, title: "Runway slope", step: 0.1, formatterType: .one)
            case 20:
                vc = SelectorViewController(nibName: "SelectorViewController", bundle: nil)
                vc?.data = SelectorSheetData(value: value ?? 0, maximum: 359, minimum: 0, title: "Wind direction", step: 1, formatterType: .int)
            case 21:
                vc = SelectorViewController(nibName: "SelectorViewController", bundle: nil)
                vc?.data = SelectorSheetData(value: value ?? 0, maximum: 30, minimum: 0, title: "Wind speed", step: 1, formatterType: .int)
            case 22:
                vc = SelectorViewController(nibName: "SelectorViewController", bundle: nil)
                vc?.data = SelectorSheetData(value: value ?? 0, maximum: 0.6, minimum: 0.4, title: "Normative friction coefficient", step: 0.01, formatterType: .two)
            case 23:
                vc = SegmentedSheetViewController(nibName: "SegmentedSheetController", bundle: nil)
                vc?.data = SegmentedSheetData(value: value ?? 0, title: "Engine warm time", values: ["2","4","6"], formatterType: .int)
            case 24:
                vc = SegmentedSheetViewController(nibName: "SegmentedSheetController", bundle: nil)
                vc?.data = SegmentedSheetData(value: value ?? 0, title: "RUD for takeoff", values: ["96","100","103","106","109","112","115","120"], formatterType: .int)
            case 80:
                vc = SelectorViewController(nibName: "SelectorViewController", bundle: nil)
                vc?.data = SelectorSheetData(value: value ?? 0, maximum: 400, minimum: 200, title: "Landing Weight", step: 1.0, formatterType: .one)
            case 81:
                vc = SelectorViewController(nibName: "SelectorViewController", bundle: nil)
                vc?.data = SelectorSheetData(value: value ?? 0, maximum: 359, minimum: 0, title: "Runway Heading", step: 10, formatterType: .int)
            case 82:
                vc = SelectorViewController(nibName: "SelectorViewController", bundle: nil)
                vc?.data = SelectorSheetData(value: value ?? 0, maximum: 5000, minimum: 1000, title: "LDA", step: 20, formatterType: .int)
            case 83:
                vc = SelectorViewController(nibName: "SelectorViewController", bundle: nil)
                vc?.data = SelectorSheetData(value: value ?? 0, maximum: 20, minimum: 0, title: "Missed approach gradient", step: 0.1, formatterType: .one)
            case 84:
                vc = SelectorViewController(nibName: "SelectorViewController", bundle: nil)
                vc?.data = SelectorSheetData(value: value ?? 0, maximum: 3000, minimum: 0, title: "Elevation", step: 5, formatterType: .int)
            case 85:
                vc = SelectorViewController(nibName: "SelectorViewController", bundle: nil)
                vc?.data = SelectorSheetData(value: value ?? 0, maximum: 1036, minimum: 695, title: "QNH", step: 1, formatterType: .int)
            case 86:
                vc = SelectorViewController(nibName: "SelectorViewController", bundle: nil)
                vc?.data = SelectorSheetData(value: value ?? 0, maximum: 45, minimum: -45, title: "Temperature", step: 1, formatterType: .int)
            case 87:
                vc = SelectorViewController(nibName: "SelectorViewController", bundle: nil)
                vc?.data = SelectorSheetData(value: value ?? 0, maximum: 2, minimum: -2, title: "Runway slope", step: 0.1, formatterType: .one)
            case 88:
                vc = SelectorViewController(nibName: "SelectorViewController", bundle: nil)
                vc?.data = SelectorSheetData(value: value ?? 0, maximum: 359, minimum: 0, title: "Wind direction", step: 1, formatterType: .int)
            case 89:
                vc = SelectorViewController(nibName: "SelectorViewController", bundle: nil)
                vc?.data = SelectorSheetData(value: value ?? 0, maximum: 30, minimum: 0, title: "Wind speed", step: 1, formatterType: .int)
            case 90:
                vc = SelectorViewController(nibName: "SelectorViewController", bundle: nil)
                vc?.data = SelectorSheetData(value: value ?? 0, maximum: 0.6, minimum: 0.4, title: "Normative friction coefficient", step: 0.01, formatterType: .two)
                
            default: print("")
        }
        
        return vc!
        
    }
}
