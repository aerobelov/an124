//
//  UpdateViewController.swift
//  IL76
//
//  Created by Pavel on 10/09/2016.
//  Copyright © 2016 Pavel. All rights reserved.
//

import UIKit
import AVFoundation
import Foundation
import Zip

class UpdateViewController: UIViewController,  URLSessionDownloadDelegate, URLSessionDelegate, CanDisplayDatabaseStatus, CanDisplayUpdatesAvailable {
    
    @IBOutlet weak var checkUpdatesButton: UIButton!
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var airac: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var endDate: UILabel!
    @IBOutlet weak var revision: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var item: UITabBarItem!
    
   // let jsonData = jsonChecker()
   
    //Constants
    let fileName = "versions.json"
    let dbName = "vph.sqlite"
    var urlString: String = ""
    let pswString = "vdaefb:h98VE4nBQ3u"
    let zipPassword = "0cc79876e5b9a2263186d7757da1a406"
    
    //Session variables
    var baseSession: URLSession!
    let baseSessionConfig = URLSessionConfiguration.background(withIdentifier: "baseSession")
    var baseTask: URLSessionDownloadTask!
    var authString:String!
    var progressValue: Float = 0
    var vphsqliteURL: URL!
    var zipURL: URL!
    var oldURL: URL!
    var reloadDelegate: ReloadDatabaseProtocol = databaseHandler.shared
    var statusDelegate: DatabaseDataStatusProtocol = databaseHandler.shared
    let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last!

    var writableURL: URL!
    var writablePath: String!
    var controlNumber: Int?
    
    override func viewDidLoad() {
    
        super.viewDidLoad()
        
        ExpirationChecker.shared.delegate = self
        
        JsonDowloader.sharedInstance.getJsonFromSao {
            print("ESCAPING")
        }
        reloadDelegate = databaseHandler.shared
        updateButton.isHidden = true
        progressView.isHidden = true
        
        //Set Database URL
        writableURL = documentsDirectory.appendingPathComponent(dbName)
        writablePath = writableURL.path
        
        //Set Zip Archive URL
        zipURL = documentsDirectory.appendingPathComponent("temporaryDatabase.zip")
       
        //Display AIRAC status on tab
        ExpirationChecker.shared.checkDatabaseExpired()
        //displayDatabaseStatus()
        
        //IF VCOMPARER INDEXES EXISTS AND NEW VERSION AVLB
        displayUpdatesAvailable()
       
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        ExpirationChecker.shared.checkDatabaseExpired()
        
    }
    
    func displayDatabaseStatus (status: String, cycle: String, start: String, end: String, rev: String) {
        
        statusLabel.text = status
        airac.text = cycle
        startDate.text = start
        endDate.text = end
        revision.text = rev

    }
    
   
    func displayUpdatesAvailable() {
        if let oldIndex = vComparer.shared.oldIndex  {
            if let newIndex = vComparer.shared.newIndex {
                if vComparer.shared.compare(old: oldIndex, new: newIndex) {
                    let revisionString = String(newIndex)
                    let right = revisionString.suffix(1)
                    let left = revisionString.prefix(4)
                    statusLabel.text = "Update is available \(left) (Rev.\(right))"
                    //print("Updates avlb")
                    updateButton.isHidden = false
                    updateButton.isEnabled = true
                    updateButton.backgroundColor = UIColor.red
                    BadgeController.show(state: .update)
                   
                } else {
                    statusLabel.text = "No updates available"
                    BadgeController.show(state: .empty)
                    
                }
            }
        }
    }
 
    func settingProgress (value: Float) {
        progressView.setProgress(value, animated: true)

    }
    
    @IBAction func checkUpdatesPressed () {
        if currentReachabilityStatus != .notReachable {
            displayUpdatesAvailable()
        } else {
            statusLabel.text = "No internet connection"
            updateButton.isHidden = true
        }
    }
    
    @IBAction func updatePressed () {
        progressView.isHidden = false
        baseSession = Foundation.URLSession(configuration: baseSessionConfig, delegate: self, delegateQueue: OperationQueue.main)
        baseSessionConfig.allowsCellularAccess = true
        progressView.setProgress(0.0, animated: false)
        if let url = JsonDowloader.sharedInstance.newUrl {
            baseTask = baseSession.downloadTask(with: url)
            statusLabel.text = "Downloading"
            baseTask.resume()
        }
    }
    
    
    func urlSession(_ session: URLSession,
                    downloadTask: URLSessionDownloadTask,
                    didWriteData bytesWritten: Int64,
                    totalBytesWritten: Int64,
                    totalBytesExpectedToWrite: Int64){
        progressValue = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        progressView.setProgress(progressValue, animated: true)
    }
    
    
    
    func urlSession(_ session: URLSession,
                    task: URLSessionTask,
                    didCompleteWithError error: Error?) {
        
        progressView.setProgress(0.0, animated: true)
        if (error != nil) {
            
        } else {
            baseTask.cancel()
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
        do {
       
            //IF ARCHIVE EXISTS, REMOVE IT
            if FileManager.default.fileExists(atPath: zipURL.path) {
                try FileManager.default.removeItem(at: zipURL)
            }
            
            
            //IF DOWLOADED, COPY ARCHIVE TO DOCUMENTS DIRECTORY, /temporaryDatabase.zip
            try FileManager.default.moveItem (at: location, to: zipURL)
            
            
            if let status = statusDelegate.db {
                if status.open() {
                    status.close()
                }
            }
            
            
            //UNZIPPING ARCHIVE TO DOCUMENTSDIRECTORY /vph.sqlite
            try Zip.unzipFile(zipURL, destination: documentsDirectory, overwrite: true,
                                   password: zipPassword, progress: { (progress) -> () in print(progress) })
            
            statusLabel.text = "Unzipping"
            reloadDelegate.reload()
            ExpirationChecker.shared.checkDatabaseExpired()
            UpdatesChecker.shared.initiateUpdatesCheck()
            displayUpdatesAvailable()
            updateButton.isEnabled = false
            updateButton.backgroundColor = UIColor.lightGray
            updateButton.isHidden = true
            progressView.isHidden = true
            BadgeController.show(state: .empty)
            
            
        } catch let error as NSError {
            print ("ERROR UNZIPPING \(error.localizedDescription)")
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
 }
