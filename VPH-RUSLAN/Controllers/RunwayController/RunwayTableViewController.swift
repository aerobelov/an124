//
//  ShowRunwaysTVC.swift
//  IL76
//
//  Created by Pavel on 08/01/2016.
//  Copyright © 2016 Pavel. All rights reserved.
//

import UIKit

class RunwayCell: UITableViewCell {
    
    @IBOutlet weak var title:UILabel!
    @IBOutlet weak var subtitle:UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}


class RunwayTableViewController: UITableViewController {
    
    var runways: [String] = []
    var delegate: TransferRunwayIdentificatorProtocol?
    
    @IBOutlet weak var RunwayTable: UITableView!
    var infoToSend: [Int: String] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        RunwayTable.delegate = self
        RunwayTable.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return runways.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RunwayCell", for: indexPath) as! RunwayCell
        cell.title.text = runways[(indexPath as NSIndexPath).row]
        // Configure the cell...
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        var selectedRunway: String
        
        selectedRunway = runways[(indexPath as NSIndexPath).row]
        
        if let delegate = delegate {
            delegate.setRunway(id: selectedRunway)
            delegate.queryRunwayData()
        }
        self.dismiss(animated: true, completion: {})
        
    }
    
    
}
