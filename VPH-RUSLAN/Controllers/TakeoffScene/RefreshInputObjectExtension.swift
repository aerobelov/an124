//
//  RefreshInputObject.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 11.07.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation

extension TakeoffViewController {
    
    func refreshTakeoffInputObject( writeTo object: inout TakeoffEntered) {
        
        object.RWYHeading = Int((buttonRunwayHeading.currentTitle! as NSString).doubleValue)
        object.TORA = Int((buttonTora.currentTitle! as NSString).doubleValue)
        object.TODA = Int((buttonToda.currentTitle! as NSString).doubleValue)
        object.ASDA = Int((buttonAsda.currentTitle! as NSString).doubleValue)
        object.rud = Int((buttonDesiredRud.currentTitle! as NSString).intValue)
        object.elevation = Int((buttonElevation.currentTitle! as NSString).doubleValue)
        object.slope = (buttonSlope.currentTitle! as NSString).doubleValue
        object.AOT = (buttonTemperature.currentTitle! as NSString).doubleValue
        object.windDirection = Int((buttonWindDirection.currentTitle! as NSString).doubleValue)
        object.windSpeed = (buttonWindSpeed.currentTitle! as NSString).doubleValue
        object.NFC = (buttonBraking.currentTitle! as NSString).doubleValue
        print("OBJECT KSC \(object.NFC)")
        object.actualTOW = (buttonActualWeight.currentTitle! as NSString).doubleValue
        object.desiredRud = Int((buttonDesiredRud.currentTitle! as NSString).intValue)
    
        object.time = warmTime(from: buttonWarm.currentTitle!)
        print("OBJECT TIME \(object.time)")
       // Int((buttonWarm.currentTitle! as NSString).intValue)
        object.SID = (buttonSidGradient.currentTitle! as NSString).doubleValue
        object.QNH = (buttonQNH.currentTitle! as NSString).doubleValue
        
    }
}
