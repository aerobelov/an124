//
//  CleanTakeoffProtocols.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 30.11.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation

//presenter out - viewcontroller input
protocol DisplayResultsProtocol {
    func displayResults(from model: TakeoffFormatted)
}

//interactor out - presenter input
protocol FormatResultsProtocol {
    func formatResults(from model: TakeoffLayout)
}
