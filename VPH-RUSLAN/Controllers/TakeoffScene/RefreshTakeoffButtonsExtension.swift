//
//  RefreshButtonsExtension.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 11.07.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation

extension TakeoffViewController {
    
    func refreshButtons(from object: TakeoffEntered) {
        
        //Left column buttons
        buttonRunwayHeading.setTitle(String(object.RWYHeading), for: .normal)
        buttonTora.setTitle(String(object.TORA), for: .normal)
        buttonToda.setTitle(String(object.TODA), for: .normal)
        buttonAsda.setTitle(String(object.ASDA), for: .normal)
        buttonDesiredRud.setTitle(String(object.rud), for: .normal)
        buttonElevation.setTitle(String(object.elevation), for: .normal)
        buttonSlope.setTitle(String(object.slope), for: .normal)
        buttonTemperature.setTitle(String(object.AOT), for: .normal)
        buttonWindDirection.setTitle(String(object.windDirection), for: .normal)
        buttonWindSpeed.setTitle(String(object.windSpeed), for: .normal)
        buttonBraking.setTitle(String(object.NFC), for: .normal)
        buttonActualWeight.setTitle(String(object.actualTOW), for: .normal)
        buttonWarm.setTitle(String(object.time.rawValue), for: .normal)
        buttonSidGradient.setTitle(String(object.SID), for: .normal)
        buttonQNH.setTitle(String(object.QNH), for: .normal)
        buttonDesiredRud.setTitle(String(object.desiredRud), for: .normal)
        
        //Upper buttons
        vvButton.setTitle(object.vCurrentLabel, for: .normal)
        altitudeTypeButton.setTitle(object.altitudeCurrentlabel, for: .normal)
        weightTypeButton.setTitle(object.weightCurrentLabel, for: .normal)
    }
}
