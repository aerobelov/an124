
//
//  SecondViewController.swift
//  VDAVPHCalc
//
//  Created by Admin on 14.08.14.
//  Copyright (c) 2014 Volga-Dnepr Airline. All rights reserved.
//

import UIKit

class TakeoffViewController: UIViewController , UIPopoverPresentationControllerDelegate, UITextFieldDelegate {
  
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var buttonRunwayHeading: UIButton!
    @IBOutlet weak var buttonTora: UIButton! 
    @IBOutlet weak var buttonToda: UIButton!
    @IBOutlet weak var buttonAsda: UIButton!
    @IBOutlet weak var buttonDesiredRud: UIButton!
    @IBOutlet weak var buttonElevation: UIButton!
    @IBOutlet weak var buttonSlope: UIButton!
    @IBOutlet weak var buttonTemperature: UIButton!
    @IBOutlet weak var buttonWindDirection: UIButton!
    @IBOutlet weak var buttonWindSpeed: UIButton!
    @IBOutlet weak var buttonBraking: UIButton!
    @IBOutlet weak var buttonActualWeight: UIButton!
    @IBOutlet weak var buttonWarm: UIButton!
    @IBOutlet weak var buttonSidGradient: UIButton!
    @IBOutlet weak var buttonQNH: UIButton!
    @IBOutlet weak var textMinimumRUD: UITextField!
    @IBOutlet weak var textQFE :UITextField!
    @IBOutlet weak var textpISA :UITextField!
    @IBOutlet weak var textpISAmm :UITextField!
    @IBOutlet weak var texttISA :UITextField!
    @IBOutlet weak var texthbaro :UITextField!
    @IBOutlet weak var textDstop:UITextField!
    @IBOutlet weak var textHeadwind: UITextField!
    @IBOutlet weak var textCrosswind: UITextField!
    @IBOutlet weak var textCrossmax:UITextField!
    @IBOutlet weak var textGrad1:UITextField!
    @IBOutlet weak var textGrad2:UITextField!
    @IBOutlet weak var textGrad3:UITextField!
    @IBOutlet weak var textGrad4:UITextField!
    @IBOutlet weak var textGmax0:UITextField!
    @IBOutlet weak var textGmax1:UITextField!
    @IBOutlet weak var textGmax2:UITextField!
    @IBOutlet weak var textGmax3:UITextField!
    @IBOutlet weak var textGmax4:UITextField!
    @IBOutlet weak var textVV:UITextField!
    @IBOutlet weak var textVVmin:UITextField!
    @IBOutlet weak var textRCTOD:UITextField!
    @IBOutlet weak var textV1:UITextField!
    @IBOutlet weak var textLrun:UITextField!
    @IBOutlet weak var textLrunreq:UITextField!
    @IBOutlet weak var textLrun3req:UITextField!
    @IBOutlet weak var textVr:UITextField!
    @IBOutlet weak var textVotr:UITextField!
    @IBOutlet weak var textV2:UITextField!
    @IBOutlet weak var textV2n:UITextField!
    @IBOutlet weak var textV15n:UITextField!
    @IBOutlet weak var textV15k:UITextField!
    @IBOutlet weak var textVVub:UITextField!
    @IBOutlet weak var textV4:UITextField!
    @IBOutlet weak var textGmax: UITextField!
    @IBOutlet weak var textGabs: UITextField!
    @IBOutlet weak var labelMsgs: UILabel!
    @IBOutlet weak var Calculate: UIButton!
    @IBOutlet weak var QFElabel :UILabel!
    @IBOutlet weak var act :UIActivityIndicatorView!
    @IBOutlet weak var deltaTISA :UITextField!
    @IBOutlet weak var textFL4 :UITextField!
    @IBOutlet weak var textFL3 :UITextField!
    @IBOutlet weak var altitudeTypeButton: UIButton!
    @IBOutlet weak var vvButton: UIButton!
    @IBOutlet weak var weightTypeButton: UIButton!
    @IBOutlet weak var icaoButton: UIButton!
    @IBOutlet weak var runwayButton : UIButton!
    @IBOutlet weak var wxButton : UIButton!
    @IBOutlet weak var expButton : UIButton!
    
    var newIcaoCode: String = ""
    var queryDelegate: ExecuteQueryProtocol = databaseHandler.shared
    var value :String = ""
    
    var tkf = TakeoffLayout()               //TAKEOF CLASS OBJECT
    var tkfEntered = TakeoffEntered()       //USER ENTERED VALUES OBJECT
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tkf.delegate = self
        refreshButtons(from: tkfEntered)
        buttonActualWeight.isEnabled = tkfEntered.isUserWeight
        act.stopAnimating()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        displayStatus()
    }
    
    override  func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "callRunwayFromTakeoff" {
            if self.newIcaoCode != "" {
                let rw = segue.destination as! RunwayTableViewController
                rw.modalPresentationStyle = UIModalPresentationStyle.popover
                rw.preferredContentSize = CGSize(width: 300, height: tkf.allRunways.count * 44)
                rw.popoverPresentationController!.delegate = self
                rw.runways = tkf.allRunways
                rw.delegate = self
            } else {
                displayAlert(AlertTitle: "Error", AlertText: "Select ICAO code", sender: self)
            }
        }
        
        if segue.identifier == "callAirportFromTakeoff" {
            let ad = segue.destination as! AirportTableViewController
            ad.modalPresentationStyle = UIModalPresentationStyle.popover
            ad.popoverPresentationController!.delegate = self
            ad.delegate = self
        }
    }

}

