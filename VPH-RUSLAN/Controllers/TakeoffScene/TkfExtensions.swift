//
//  TkfExtensions.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 01.08.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation
import UIKit



extension TakeoffViewController {
    
    func displayStatus () {
        versionLabel.text = "AIRAC \(databaseHandler.shared.airac)"
        heading.text = "An-124-100 Takeoff performance (ver. \(build))"
    }
    
    
    
    func ShowSlider(_ sender: UIButton) {
        let fabric = SelectSheetControllerFabric()
        
        let selectorVC = fabric.createSheetController(
            from: sender.tag,
            with: sender.titleLabel?.text ?? "0"
        )
        selectorVC.buttonBackController = self
        selectorVC.recepient = sender
        (selectorVC as! UIViewController).modalPresentationStyle = .formSheet
        (selectorVC as! UIViewController).modalTransitionStyle = .crossDissolve
        present(selectorVC as! UIViewController, animated: true, completion: nil)
    }
    
   
    
    func resetWeightColors() {
        textGmax0.textColor = UIColor.black
        textGmax1.textColor = UIColor.black
        textGmax2.textColor = UIColor.black
        textGmax3.textColor = UIColor.black
        textGmax4.textColor = UIColor.black
        textCrossmax.textColor = UIColor.black
    }
    
    func resetRud() {
        if ( !buttonDesiredRud.isEnabled ) {
            buttonDesiredRud.setTitle("120", for: .normal)
        }
    }
    
    func markMinWeightRed() {
        switch tkf.minWeightIndex() {
           case 0: textGmax0.textColor = UIColor.red
           case 1: textGmax1.textColor = UIColor.red
           case 2: textGmax2.textColor = UIColor.red
           case 3: textGmax3.textColor = UIColor.red
           case 4: textGmax4.textColor = UIColor.red
           default: NSLog("")
        }
    }
    
    func markCrossWindIfExeeds() {
        if tkf.crosswindExeeded {
            textCrossmax.textColor = UIColor.red
        }
    }
    
    func checkPressureAltitudePositive() {
        if let pa = tkf.PressureAltitude {
            if (pa < 0) {
                altitudeTypeChanged()
                displayAlert(AlertTitle: "Warning", AlertText: "Pressure altitude is below zero (\(round(pa))), unable to use Pressure Altitude due AN124 Flight Manual restrictions. ELV-PA switch was forced to ELV, result is based on Elevation", sender: self)
            }
        }
    }
    
   
    
    
    func checkErrorBadge(_ logItem: ErrorDataSource) {
        
        let tabArray = self.tabBarController?.tabBar.items as NSArray?
        let logTab = tabArray?.object(at: 3) as! UITabBarItem
        
        if logItem.warningTitle.count > 2 {
            logTab.badgeValue = "E"
        }
        else {
            logTab.badgeValue = nil
        }
    }
    
    func indicationWeightType() {
        
        self.buttonQNH.isEnabled = tkfEntered.isPressureAltitude
        
        switch tkfEntered.isPressureAltitude {
        case true:
            
            texthbaro.backgroundColor = UIColor.orange
            texthbaro.textColor = UIColor.darkGray
            texthbaro.isEnabled = false
            textQFE.isEnabled = false
            textQFE.backgroundColor = .systemTeal
            
            if let qfe = tkf.QFE {
                self.QFElabel.text=("QFE, hpa  (\(Int(floor (qfe * 0.750061683)))) mm")
            } else {
                self.QFElabel.text=("QFE, hpa  (---) mm")
            }
        case false:
            texthbaro.backgroundColor = UIColor.systemGray
            textQFE.backgroundColor = UIColor.systemGray
            buttonQNH.setTitle(String(tkfEntered.QNH), for: .normal)
            setTextValue(textQFE, n: tkf.QFE, precision: .int)
        }
    }
}
