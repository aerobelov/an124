//
//  DatabaseFunctions.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 17.07.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation

extension TakeoffViewController: TransferIcaoCodeProtocol {
    
    func setIcao (code: String) {
        self.newIcaoCode = code
        icaoButton.setTitle(self.newIcaoCode, for: .normal)
        runwayButton.setTitle("SEL RWY", for: UIControl.State())
        
        //!!!MAKE RESET FUNC
        buttonSidGradient.setTitle("3", for: .normal)
        buttonWindSpeed.setTitle("0", for: .normal)
    }
    
    func queryRunways () {
        let reqRwy = "SELECT RW FROM rw_thres where ICAO='\(self.newIcaoCode)'"
        let rsRW = queryDelegate.executeQuery(query: reqRwy)
        tkf.allRunways.removeAll()
        
        while (rsRW.next() == true) {
            tkf.allRunways.append(rsRW.string(forColumn: "RW")!)
        }
    }
}



extension TakeoffViewController:  TransferRunwayIdentificatorProtocol {
    
    func setRunway (id: String)  {
        tkf.myRunway = id
        runwayButton.setTitle(tkf.myRunway, for: UIControl.State())
    }
 
    func queryRunwayData() {
        let reqData = "SELECT LEN, MC, ELEV, SLOPE from rw_thres where ICAO='\(self.newIcaoCode)' and RW='\(tkf.myRunway)'"
        let rsData = queryDelegate.executeQuery(query: reqData)
        while (rsData.next() == true) {
            let LEN:String = rsData.string(forColumn: "LEN")!
            let ELEV:String = rsData.string(forColumn: "ELEV")!
            let SLOPE:String = rsData.string(forColumn: "SLOPE")!
            let COURSE:String = rsData.string(forColumn: "MC")!
            
            if let s = Double(LEN) {
                self.tkfEntered.TORA = Int(round(s * 0.3048))
                self.tkfEntered.TODA = Int(round(s * 0.3048))
                self.tkfEntered.ASDA = Int(round(s * 0.3048))
            } else {
                self.tkfEntered.TORA = 0
            }
            if let e = Double(ELEV) {
                self.tkfEntered.elevation = Int(e * 0.3048)
            } else {
                self.tkfEntered.elevation = 0
            }
            if let p = Double(SLOPE) {
                self.tkfEntered.slope = round(p*10)/10
            } else {
                self.tkfEntered.slope = 0
            }
            if let c = Double(COURSE) {
                self.tkfEntered.RWYHeading = Int(c/10)
            } else {
                self.tkfEntered.RWYHeading = 0
            }
            
        }
        self.refreshButtons(from: self.tkfEntered)
    }

}


