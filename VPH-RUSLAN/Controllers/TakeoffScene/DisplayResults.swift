//
//  DisplayResults.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 13.07.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation

extension TakeoffViewController {
    
    func displayResults() {
        
        //Indication
        setTextValue(textVV, n: tkf.VVabsolute, precision: .two)
        setTextValue(textVVmin, n: tkf.VVmin, precision: .two)
        setTextValue(textHeadwind, n: tkf.Headwind, precision: .two)
        setTextValue(textCrosswind, n: tkf.Crosswind, precision: .two)
        setTextValue(textGabs, n: tkf.AbsoluteTOW, precision: .one)
        
        switch tkf.isUserWeight {
            case true:
                textGmax.text = "---"
                textGmax0.text = "---"
                textGmax1.text = "---"
                textGmax2.text = "---"
                textGmax3.text = "---"
                textGmax4.text = "---"
            case false:
                setTextValue(textGmax, n: tkf.CalculatedTOW, precision: .one)
                setTextValue(textGmax0, n: tkf.TOWgrad, precision: .one)
                setTextValue(textGmax1, n: tkf.TOWTORA, precision: .one)
                setTextValue(textGmax2, n: tkf.TOWTODA, precision: .one)
                setTextValue(textGmax3, n: tkf.TOWSID, precision: .one)
                setTextValue(textGmax4, n: tkf.TOWASDA, precision: .one)
        }
        
        setTextValue(textCrossmax, n:tkf.MaxCrosswind, precision: .one)
        setTextValue(textpISA, n:tkf.ISAPhpa, precision: .int)
        setTextValue(texttISA, n:tkf.ISAT, precision: .one)
        setTextValue(textGrad1, n:tkf.Grad3120, precision: .one)
        setTextValue(textGrad2, n:tkf.Grad4120, precision: .one)
        setTextValue(textGrad3, n:tkf.Grad3400, precision: .one)
        setTextValue(textGrad4, n:tkf.Grad4400, precision: .one)
        setTextValue(textRCTOD, n: tkf.Dtakeoff3req, precision: .distance)
        setTextValue(textV1, n:tkf.V1, precision: .int)
        setTextValue(textLrun, n: tkf.Lrun4, precision: .distance)
        setTextValue(textLrunreq, n: tkf.Lrun4req, precision: .distance)
        setTextValue(textVr, n: tkf.Vr, precision: .int)
        setTextValue(textV2, n: tkf.V2, precision: .int)
        setTextValue(textV2n, n: tkf.V2n, precision: .int)
        setTextValue(textV15n, n: tkf.V3015, precision: .int)
        setTextValue(textV15k, n: tkf.V152, precision: .int)
        setTextValue(textVVub, n: tkf.V20, precision: .int)
        setTextValue(textV4, n: tkf.V4, precision: .int)
        setTextValue(textDstop, n: tkf.DStop, precision: .int)
        setTextValue(textLrun3req, n: tkf.Lrun3req, precision: .distance)
        setTextValue(textVotr, n: tkf.Votr, precision: .int)
        setTextValue(texthbaro, n: tkf.PressureAltitude, precision: .int)
        setTextValue(textpISAmm, n: tkf.ISAPmmhg, precision: .int)
        setTextValue(deltaTISA, n: tkf.deltaISA, precision: .int)
        setTextValue(textFL4, n:tkf.FL4, precision: .int)
        setTextValue(textFL3, n:tkf.FL3, precision: .int)
        setTextValue(textMinimumRUD, n:tkf.minimumRUD, precision: .int)
        //EXCLUDED BY LS
        //setTextValue(textLdist, n: tkf.Dtkf, precision: .distance)
        //setTextValue(textLdistreq, n: tkf.Dtkfreq, precision: .distance)
        //setTextValue(textVkr, n: tkf.Vcircle, precision: .int)
        
        //indicating QFE
        if let qfe = tkf.QFE {
            QFElabel.text=("QFE, hpa  (\(Int(floor (qfe * 0.750061683)))) mm")
            setTextValue(textQFE, n: floor(qfe), precision: .int)
        } else {
            QFElabel.text=("QFE, hpa  (---) mm")
            textQFE.text = "---"
        }
        labelMsgs.text = "Calculation completed"
    }
}
