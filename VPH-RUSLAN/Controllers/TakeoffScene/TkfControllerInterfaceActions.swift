//
//  TkfControllerInterfaceActions.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 28.06.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation
import UIKit

extension TakeoffViewController {
    
    @IBAction func altitudeTypeChanged() {
        tkfEntered.altitudeToggle()
        altitudeTypeButton.setTitle(tkfEntered.altitudeCurrentlabel, for: .normal)
        indicationWeightType()
    }
    
    @IBAction func weightTypeChanged() {
        tkfEntered.weightToggle()
        weightTypeButton.setTitle(tkfEntered.weightCurrentLabel, for: .normal)
        buttonActualWeight.isEnabled = tkfEntered.isUserWeight
        buttonDesiredRud.isEnabled = tkfEntered.isUserWeight
        if ( !buttonDesiredRud.isEnabled ) {
            buttonDesiredRud.setTitle("120", for: .normal)
        }
        buttonActualWeight.setTitle(tkfEntered.weightCurrentValue, for: .normal)
    }
    
    @IBAction private func showSelector(_ sender: UIButton) {
        ShowSlider(sender)
    }
    
    @IBAction func VVchanged () {
        tkfEntered.vvToggle()
        vvButton.setTitle(tkfEntered.vCurrentLabel, for: .normal)
    }
    
    @IBAction func CalculateTouched(_ sender: UIButton) {
        act.startAnimating()
        labelMsgs.text = "Progress. Please wait...."
    }
    
    @IBAction func TORAchanged (_ sender: UIButton) {
        
        if let n = distanceFormatter.number(from: buttonTora.title(for: .normal) ?? "")
        {
            buttonToda.setTitle(intFormatter.string(from: n), for: .normal)
            buttonAsda.setTitle(intFormatter.string(from: n), for: .normal)
        }
    }
    
    @IBAction func ColorToGray () {
        Calculate.backgroundColor = UIColor.green
        labelMsgs.text = "Calculation completed"
    }
    
    @IBAction func ColorToRed () {
        Calculate.backgroundColor = UIColor.red
        labelMsgs.text = "Inputs have been changed. RECALCULATE"
    }
    
    
    //TEMPORARY ALERTS
    @IBAction func wx() {
        displayAlert(AlertTitle: "This feature is being developed", AlertText: "METAR request will be available in next version", sender: self)
    }
    
    @IBAction func exp() {
        displayAlert(AlertTitle: "This feature is being developed", AlertText: "Export will be available in next version", sender: self)
    }
   
}


