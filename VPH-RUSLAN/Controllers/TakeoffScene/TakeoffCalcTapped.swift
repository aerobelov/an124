//
//  CalcTapped.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 13.07.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation
import UIKit

extension TakeoffViewController {
    
    @IBAction func CalculateTapped(_ sender: UIButton) {
       
        resetWeightColors()
        refreshTakeoffInputObject(writeTo: &tkfEntered)
        logInstance.arraysReset()
        
        if tkfEntered.tailWindOK {
            
            tkf.upload(from: tkfEntered)
            tkf.Pressure ()
            checkPressureAltitudePositive()
            tkf.calcTakeoff()
            tkf.Gradients()
            tkf.flightLevels()
            logInstance.appendWarning()
            checkErrorBadge(logInstance)
            displayResults()
            markMinWeightRed()
            markCrossWindIfExeeds()
            ColorToGray()
            act.stopAnimating()
       } else {
            act.stopAnimating()
            ColorToRed()
            displayAlert(AlertTitle: "Are you sure?", AlertText: "Headwind or Tailwind is out of range", sender: self)
            logInstance.textmsg = "Check parameters and try again"
       }

     
   }//tapped
}
