//
//  Extensions.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 30.11.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController: CanDisplayAlert {
    
    func displayAlert(AlertTitle title:String, AlertText message:String, sender: UIViewController) {
        let alertController = UIAlertController (title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title:"Roger", style: .default, handler: nil))
        sender.present(alertController, animated: true)
    }
    
}


extension UIViewController: ControllerCommonMethodsProtocol {
    
    func setTextValue(_ field: UITextField, n: Double?, precision: formatterType){
        let formSwitcher = FormatterConfigurator(type: precision)
        
        if let res = n {
            field.text = formSwitcher.formatter.string(from: NSNumber(value: res))
        }
        else {
            field.text = "---"
        }
    }
}
