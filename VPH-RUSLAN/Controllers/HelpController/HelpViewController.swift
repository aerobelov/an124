//
//  HelpViewController.swift
//  VDAVPHCalc
//
//  Created by Admin on 13.01.15.
//  Copyright (c) 2015 Volga-Dnepr Airline. All rights reserved.
//

import UIKit
import WebKit

class HelpViewController: UIViewController {
    
 
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet var helpview: WKWebView?
      
    override func viewDidLoad() {
        super.viewDidLoad()
        helpview = WKWebView()
        self.view = helpview
        if let myURL = Bundle.main.url(forResource: "help", withExtension: "html") {
            helpview?.loadFileURL(myURL, allowingReadAccessTo: myURL.deletingLastPathComponent())
        }
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
   

}

