//
//  LOG.swift
//  VDAVPHCalc
//
//  Created by Pavel on 18/08/2015.
//  Copyright (c) 2015 Volga-Dnepr Airline. All rights reserved.
//

import UIKit

//class ErrorHandler {
//    var textmsg :String =  ""
//    var errormessage :String = ""
//    var wrnmsg :String?
//    init (){
//        self.textmsg = "\nNO LOG\n"
//        self.wrnmsg = nil
//            }
//}


class LOG: UIViewController , UITextFieldDelegate {
    
   // @IBOutlet weak var LogMessage :UILabel!
    @IBOutlet weak var text :UITextField!
    @IBOutlet weak var LogMessage :UITextView!
    
    @IBOutlet weak var tableView :UITableView!
       
    
    @IBOutlet weak var logscroller: UIScrollView! {
        didSet {
            logscroller.contentSize = CGSize(width: 768,height: 3000)
        }
    }
       
    override func viewDidLoad() {
        
        super.viewDidLoad()
      //  self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "logCell")
        LogMessage.text = logInstance.textmsg
        
        
    }
    
    override func viewWillAppear(_ Animated :Bool) {
        super.viewWillAppear(Animated)
        if logInstance.wrnmsg == nil { LogMessage.text =  "NO ERRORS" + logInstance.textmsg
        } else {
            LogMessage.text = "ERRORS FOUND:" + logInstance.wrnmsg! + logInstance.textmsg
        }
   }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
}
