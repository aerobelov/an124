//
//  logTableViewCell.swift
//  VDAVPHCalc
//
//  Created by Pavel on 09/09/2015.
//  Copyright (c) 2015 Volga-Dnepr Airline. All rights reserved.
//

import UIKit

class logTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title:UILabel!
    @IBOutlet weak var detail:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
