//
//  LandCalcTapped.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 03.08.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation

extension LandingViewController {
    
    @IBAction func CalcTapped (_ sender: UIButton) {
            
        resetWeightColors()
        refreshLandingInputObject(writeTo: &ldgEntered)
        logInstance.arraysReset()
        
        if ldgEntered.tailWindOK {
            ldg.upload(from: ldgEntered)
            ldg.Pressure()
            checkPressureAltitudePositive()
            ldg.calcLanding ()
            logInstance.appendWarning()
            checkErrorBadge(logInstance)
            displayResults()
            markMinWeightRed()
            activityIcon.stopAnimating()
        } else {
            activityIcon.stopAnimating()
            LandColorToRed()
            displayAlert(AlertTitle: "Are you sure?", AlertText: "Headwind or Tailwind is out of range", sender: self)
            logInstance.textmsg = "Check parameters and try again"
        }
    }
}
