//
//  LandingIBActions.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 01.08.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation

extension LandingViewController {
    
    @IBAction func CalculateTouched(_ sender :UIButton) {
        activityIcon.startAnimating()
        labelMsgs.text = "Progress. Please wait...."
    }
    
    @IBAction func LandColorToGray () {
        goLandButton.backgroundColor = UIColor.green
        labelMsgs.text = "Calculation completed"
    }
    
    @IBAction func LandColorToRed () {
        goLandButton.backgroundColor = UIColor.red
        labelMsgs.text = "Inputs have been changed. RECALCULATE"
    }

    @IBAction func ColorToGray () {
        goLandButton.backgroundColor = UIColor.green
        labelMsgs.text = "Calculation completed"
    }
    
  
    @IBAction func airportTypeChanged() {
        ldgEntered.destToggle()
        airportTypeButton.setTitle(ldgEntered.destinationCurrentLabel, for: .normal)
    }
    
    @IBAction func altitudeTypeChanged() {
        ldgEntered.altitudeToggle()
        altitudeTypeButton.setTitle(ldgEntered.altitudeCurrentlabel, for: .normal)
        indicationWeightType()
    }
    
    @IBAction func weightTypeChanged() {
        ldgEntered.weightToggle()
        weightTypeButton.setTitle(ldgEntered.weightCurrentLabel, for: .normal)
        buttonActualWeight.isEnabled = ldgEntered.isUserWeight
        buttonActualWeight.setTitle(ldgEntered.weightCurrentValue, for: .normal)
    }
    
    @IBAction private func showSelector(_ sender: UIButton) {
        ShowSlider(sender)
    }
    
    //TEMPORARY ALERTS
    @IBAction func wx() {
        displayAlert(AlertTitle: "This feature is being developed", AlertText: "METAR request will be available in next version", sender: self)
    }
    
    @IBAction func exp() {
        displayAlert(AlertTitle: "This feature is being developed", AlertText: "Export will be available in next version", sender: self)
    }
    //-----------------

}
