//
//  FirstViewController.swift
//  VDAVPHCalc
//
//  Created by Admin on 14.08.14.
//  Copyright (c) 2014 Volga-Dnepr Airline. All rights reserved.
//

import UIKit



class LandingViewController: UIViewController , UIPopoverPresentationControllerDelegate, UITextFieldDelegate {
    
    
    let nfc: [Double:Double] = [0.4:8.0,0.41:8.4,0.42:8.8,0.43:9.2,0.44:9.6,0.45:10.0,0.46:10.5,0.47:10.8,0.48:11.3,0.49:11.8,0.50:12.0,0.51:12.4,0.52:12.6,0.53:13.1,0.54:13.2,0.55:13.8,0.56:14.0,0.57:14.4,0.58:14.6,0.59:14.8,0.6:15.0]
   
    var newIcaoCode: String = ""
    var queryDelegate: ExecuteQueryProtocol = databaseHandler.shared
    
    var value :String = ""
    @IBOutlet weak var WetLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var header: UILabel!
    @IBOutlet weak var textHeadwind: UITextField!
    @IBOutlet weak var textCrosswind: UITextField!
    @IBOutlet weak var textFieldGGrad: UITextField!
    @IBOutlet weak var textGMA3: UITextField!
    @IBOutlet weak var textGMA4: UITextField!
    @IBOutlet weak var textGLDA: UITextField!
    @IBOutlet weak var textGmax: UITextField!
    @IBOutlet weak var textGcalc: UITextField!
    @IBOutlet weak var textGmax2eng: UITextField!
    @IBOutlet weak var textDH2engines: UITextField!
    @IBOutlet weak var textLRUN: UITextField!
    @IBOutlet weak var textLLAND: UITextField!
    @IBOutlet weak var textLLANDreq: UITextField!
    @IBOutlet weak var textLLANDreqwet: UITextField!
    @IBOutlet weak var textVdesc: UITextField!
    @IBOutlet weak var textVland: UITextField!
    @IBOutlet weak var textVbrake: UITextField!
    @IBOutlet weak var textAutoBrake: UITextField!
    @IBOutlet weak var textGrad3: UITextField!
    @IBOutlet weak var textGrad4: UITextField!
    @IBOutlet weak var textLandQFE: UITextField!
    @IBOutlet weak var textLandTISA: UITextField!
    @IBOutlet weak var textLandPISA: UITextField!
    @IBOutlet weak var textLandPISAmm: UITextField!
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var labelMsgs: UILabel!
    @IBOutlet weak var actInd: UIActivityIndicatorView!
    @IBOutlet weak var LandCrossmax: UITextField!
    @IBOutlet weak var textLandHbaro: UITextField!
    @IBOutlet weak var LQFElabel :UILabel!
    @IBOutlet weak var activityIcon :UIActivityIndicatorView!
    @IBOutlet weak var goLandButton: UIButton!
    @IBOutlet weak var altitudeTypeButton: UIButton!
    @IBOutlet weak var airportTypeButton: UIButton!
    @IBOutlet weak var weightTypeButton: UIButton!
    @IBOutlet weak var icaoButton: UIButton!
    @IBOutlet weak var runwayButton : UIButton!
    @IBOutlet weak var wxButton : UIButton!
    @IBOutlet weak var expButton : UIButton!
    @IBOutlet weak var unitsButton : UIButton!
    //SliderButtons
    @IBOutlet weak var buttonLDA: UIButton! //textLDA
    @IBOutlet weak var buttonActualWeight: UIButton! //textGmaxforcalc
    @IBOutlet weak var buttonRunwayHeading: UIButton!
    @IBOutlet weak var buttonWindDirection: UIButton!
    @IBOutlet weak var buttonWindSpeed: UIButton!
    @IBOutlet weak var buttonMissedGradient: UIButton! //textmaxgradient
    @IBOutlet weak var buttonElevation: UIButton!
    @IBOutlet weak var buttonQNH: UIButton! //LQNH
    @IBOutlet weak var buttonSlope: UIButton! //textSlope
    @IBOutlet weak var buttonBraking: UIButton! //textBrakingAction
    @IBOutlet weak var buttonTemperature: UIButton! //textFielTemp

    // creating landing class object
    var ldg  = LandingLayout()
    var ldgEntered = LandingEntered()
    
    @IBOutlet weak var content: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshButtons(from: ldgEntered)
        activityIcon.stopAnimating()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        displayStatus()
    }
      
    override  func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "callRunwayFromLanding" {
            if self.newIcaoCode != "" {
                let rw = segue.destination as! RunwayTableViewController
                rw.modalPresentationStyle = UIModalPresentationStyle.popover
                rw.preferredContentSize = CGSize(width: 300, height: ldg.allRunways.count * 44)
                rw.popoverPresentationController!.delegate = self
                rw.runways = ldg.allRunways
                rw.delegate = self
            } else {
                displayAlert(AlertTitle: "Error", AlertText: "Select ICAO code", sender: self)
            }
        }
        
        if segue.identifier == "callAirportFromLanding" {
            let ad = segue.destination as! AirportTableViewController
            ad.modalPresentationStyle = UIModalPresentationStyle.popover
            ad.popoverPresentationController!.delegate = self
            ad.delegate = self            
        }
        
    }
  
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
