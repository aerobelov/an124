//
//  RefreshButtonExtension.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 03.08.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation

extension LandingViewController {
   
    func refreshButtons(from object: LandingEntered) {
        buttonLDA.setTitle(String(object.LDA), for: .normal)
        buttonActualWeight.setTitle(String(object.actualLDW), for: .normal)
        buttonRunwayHeading.setTitle(String(object.RWYHeading), for: .normal)
        buttonWindDirection.setTitle(String(object.windDirection), for: .normal)
        buttonWindSpeed.setTitle(String(object.windSpeed), for: .normal)
        buttonQNH.setTitle(String(object.QNH), for: .normal)
        buttonSlope.setTitle(String(object.slope), for: .normal)
        buttonTemperature.setTitle(String(object.AOT), for: .normal)
        buttonBraking.setTitle(String(object.NFC), for: .normal)
        buttonMissedGradient.setTitle(String(object.gradient), for: .normal)
        buttonElevation.setTitle(String(object.elevation), for: .normal)
        airportTypeButton.setTitle(object.destinationCurrentLabel, for: .normal)
        altitudeTypeButton.setTitle(object.altitudeCurrentlabel, for: .normal)
        weightTypeButton.setTitle(object.weightCurrentLabel, for: .normal)
    }
}
