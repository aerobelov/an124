//
//  RefrreshLandInputObhectExtension.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 03.08.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation


extension LandingViewController {
    
    func refreshLandingInputObject( writeTo object: inout LandingEntered) {
        
        object.LDA = Int((buttonLDA.currentTitle! as NSString).intValue)
        object.actualLDW = Double(Int((buttonActualWeight.currentTitle! as NSString).doubleValue))
        object.RWYHeading = Int((buttonRunwayHeading.currentTitle! as NSString).intValue)
        object.windDirection = Int((buttonWindDirection.currentTitle! as NSString).intValue)
        object.windSpeed = (buttonWindSpeed.currentTitle! as NSString).doubleValue
        object.QNH = (buttonQNH.currentTitle! as NSString).doubleValue
        object.slope = (buttonSlope.currentTitle! as NSString).doubleValue
        object.AOT = (buttonTemperature.currentTitle! as NSString).doubleValue
        object.NFC = (buttonBraking.currentTitle! as NSString).doubleValue
        object.gradient = (buttonMissedGradient.currentTitle! as NSString).doubleValue
        object.elevation = Int((buttonElevation.currentTitle! as NSString).intValue)
        
    }
}
