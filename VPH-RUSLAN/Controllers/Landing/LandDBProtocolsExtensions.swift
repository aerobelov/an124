//
//  LandDatabaseFunctions.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 03.08.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation
import UIKit

extension LandingViewController: TransferIcaoCodeProtocol {
    
   
    func setIcao (code: String) {
        self.newIcaoCode = code
        icaoButton.setTitle(self.newIcaoCode, for: UIControl.State())
        runwayButton.setTitle("SEL RWY", for: UIControl.State())
        buttonMissedGradient.setTitle("3", for: .normal)
        buttonWindSpeed.setTitle("0", for: .normal)
    }
    
    func queryRunways () {
        let reqRwy = "SELECT RW FROM rw_thres where ICAO='\(self.newIcaoCode)'"
        let rsRW = queryDelegate.executeQuery(query: reqRwy)
        ldg.allRunways.removeAll()
        
        while (rsRW.next() == true) {
            ldg.allRunways.append(rsRW.string(forColumn: "RW")!)
        }
    }
}
   

extension LandingViewController: TransferRunwayIdentificatorProtocol {
    
    func setRunway (id: String)  {
        ldg.myRunway = id
        runwayButton.setTitle(ldg.myRunway, for: UIControl.State())
    }
    
    func queryRunwayData() {
        
        let reqData = "SELECT LEN, MC, ELEV, SLOPE from rw_thres where ICAO='\(self.newIcaoCode)' and RW='\(ldg.myRunway)'"
        let rsData = queryDelegate.executeQuery(query: reqData)
        while (rsData.next() == true) {
            
            let LEN:String = rsData.string(forColumn: "LEN")!
            let ELEV:String = rsData.string(forColumn: "ELEV")!
            let SLOPE:String = rsData.string(forColumn: "SLOPE")!
            let COURSE:String = rsData.string(forColumn: "MC")!
            
            if let s = Double(LEN) {
                self.ldgEntered.LDA = Int(round(s * 0.3048))
            } else {
                self.ldgEntered.LDA = 0
            }
            if let e = Double(ELEV) {
                self.ldgEntered.elevation = Int(e * 0.3048)
            } else {
                self.ldgEntered.elevation = 0
            }
            if let p = Double(SLOPE) {
                self.ldgEntered.slope = round(p*10)/10
            } else {
                self.ldgEntered.slope = 0
            }
            if let c = Double(COURSE) {
                self.ldgEntered.RWYHeading = Int(c/10)
            } else {
                self.ldgEntered.RWYHeading = 0
            }
            
        }
        refreshButtons(from: self.ldgEntered)
    }
}
