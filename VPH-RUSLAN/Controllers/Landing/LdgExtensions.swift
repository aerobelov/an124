//
//  LdgExtensions.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 01.08.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation

extension LandingViewController {
    
    func displayStatus () {
        header.text = "An-124-100 Landing performance (ver. \(build))"
        versionLabel.text = "AIRAC \(databaseHandler.shared.airac)"
    }
    
    func markMinWeightRed() {
        switch ldg.minWeightIndex() {
            case 0: textFieldGGrad.textColor = UIColor.red
            case 1: textGLDA.textColor = UIColor.red
            case 2: textGMA3.textColor = UIColor.red
            case 3: textGMA4.textColor = UIColor.red
            default: NSLog("")
        }
    }
    
    func markCrossWindIfExeeds() {
        if ldg.crosswindExeeded {
            LandCrossmax.textColor = UIColor.red
        }
    }
    
    func ShowSlider(_ sender: UIButton) {
        let fabric = SelectSheetControllerFabric()
        
        let selectorVC = fabric.createSheetController(from: sender.tag, with: sender.titleLabel?.text ?? "0")
            //(nibName: "SelectorViewController", bundle: nil)
        //selectorVC.button = makeTakeoffSlider(from: sender.tag, with: sender.titleLabel?.text ?? "0")
        selectorVC.buttonBackController = self
        selectorVC.recepient = sender
        (selectorVC as! UIViewController).modalPresentationStyle = .formSheet
        (selectorVC as! UIViewController).modalTransitionStyle = .crossDissolve
        present(selectorVC as! UIViewController, animated: true, completion: nil)
    }
    
    func checkPressureAltitudePositive() {
        if let pa = ldg.PressureAltitude {
            if (pa < 0) {
                altitudeTypeChanged()
                displayAlert(AlertTitle: "Warning", AlertText: "Pressure altitude is below zero (\(round(pa))), unable to use Pressure Altitude due AN124 Flight Manual restrictions. ELV-PA switch was forced to ELV, result is based on Elevation", sender: self)
            }
        }
    }
    
    func resetWeightColors() {
        textFieldGGrad.textColor = UIColor.black
        textGLDA.textColor = UIColor.black
        textGMA3.textColor = UIColor.black
        textGMA4.textColor = UIColor.black
        LandCrossmax.textColor = UIColor.black
    }
    
    func checkErrorBadge(_ logItem: ErrorDataSource) {
        
        let tabArray = self.tabBarController?.tabBar.items as NSArray?
        let logTab = tabArray?.object(at: 3) as! UITabBarItem
        
        if logItem.warningTitle.count > 2 {
            logTab.badgeValue = "E"
        }
        else {
            logTab.badgeValue = nil
        }
    }
    
    func indicationWeightType() {
        
        self.buttonQNH.isEnabled = ldgEntered.isPressureAltitude
        
        switch ldgEntered.isPressureAltitude {
        case true:
            textLandHbaro.backgroundColor = UIColor.orange
            textLandHbaro.textColor = UIColor.darkGray
            textLandHbaro.isEnabled = false
            textLandQFE.isEnabled = false
            textLandQFE.backgroundColor = UIColor.systemTeal
            if let qfe = ldg.QFE {
                self.LQFElabel.text=("QFE, hpa  (\(Int(floor (qfe * 0.750061683)))) mm")
            } else {
                self.LQFElabel.text=("QFE, hpa  (---) mm")
            }
        case false:
            textLandHbaro.backgroundColor = UIColor.systemGray
            textLandQFE.backgroundColor = UIColor.systemGray
            buttonQNH.setTitle(String(ldgEntered.QNH), for: .normal)
            setTextValue(textLandQFE, n: ldg.QFE, precision: .int)
        }
    }
}
