//
//  DisplayLandingResults.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 03.08.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation

extension LandingViewController {
    
    func displayResults() {
        
        setTextValue(textHeadwind, n: ldg.Headwind, precision: .two)
        setTextValue(textCrosswind, n: ldg.Crosswind, precision: .two)
        
        setTextValue(textFieldGGrad, n: ldg.LDWmax27, precision: .weight)
        setTextValue(textGLDA, n: ldg.LDWmaxLDA, precision: .weight)
        setTextValue(textGcalc, n: ldg.AbsoluteLDW, precision: .weight)
        setTextValue(textGMA4, n: ldg.LDWmaxMAP4, precision: .weight)
        setTextValue(textGMA3, n: ldg.LDWmaxMAP3, precision: .weight)
        setTextValue(textGmax, n: ldg.CalculatedLDW, precision: .weight)
        setTextValue(textGmax2eng, n: ldg.LDWmaxMAP2, precision: .weight)
        setTextValue(textLandTISA, n: ldg.ISAT, precision: .int)
        setTextValue(textLandPISA, n: ldg.ISAPhpa, precision: .int)
        
        setTextValue(LandCrossmax, n: ldg.MaxCrosswind, precision: .one)
        setTextValue(textGrad4, n: ldg.Gradient4engines, precision: .one)
        setTextValue(textGrad3, n: ldg.Gradient3engines, precision: .one)
        
        setTextValue(textLRUN, n: ldg.Lrun, precision: .distance)
        setTextValue(textLLAND, n: ldg.Dland, precision: .distance)
        setTextValue(textVbrake, n: ldg.BrakeSpeed, precision: .int)
        setTextValue(textAutoBrake, n: ldg.AutobrakeLDW, precision: .weight)
        setTextValue(textVdesc, n: ldg.ApproachSpeed, precision: .int)
        setTextValue(textVland, n: ldg.TouchdownSpeed, precision: .int)
        setTextValue(textDH2engines, n: ldg.DescisionHeight2, precision: .int)
        setTextValue(textLLANDreq, n: ldg.Dlandreq, precision: .distance)
        //setTextValue(textLLANDreqwet, n: ldg.Dlandreqwet, precision: .distance)
        setTextValue(textLandHbaro, n: ldg.PressureAltitude, precision: .int)
        setTextValue(textLandPISAmm, n: ldg.ISAPmmhg, precision: .int)
        
        //indicating QFE
        if let qfe = ldg.QFE {
            LQFElabel.text=("QFE, hpa  (\(Int(floor (qfe * 0.750061683)))) mm")
            setTextValue(textLandQFE, n: floor(qfe), precision: .int)
        } else {
            LQFElabel.text=("QFE, hpa  (---) mm")
            textLandQFE.text = "---"
        }
        
        labelMsgs.text = "Calculation completed."
        
    }
}
