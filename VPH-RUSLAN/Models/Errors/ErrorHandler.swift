//
//  ErrorHandler.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 15.05.17.
//  Copyright © 2017 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation

enum FormError: Error {
   
    case OutOfRange
    case OutOfType
}

