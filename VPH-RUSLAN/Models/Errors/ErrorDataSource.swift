//
//  ErrorDataSource.swift
//  VDAVPHCalc
//
//  Created by Pavel on 10/09/2015.
//  Copyright (c) 2015 Volga-Dnepr Airline. All rights reserved.
//

import UIKit

class ErrorDataSource: NSObject {
       
    var textmsg :String =  ""
    var errormessage :String = ""
    var wrnmsg :String?
    var title : [String] = []
    var detail : [String] = []
    var warningTitle = [" ","ATTENTION REQUIRED"]
    var warningDetail = [" "," "]
    override init (){
        self.textmsg = "\nNO LOG\n"
        self.wrnmsg = nil
     }
    
    func arraysReset() {
        self.title.removeAll()
        self.detail.removeAll()
        self.warningTitle.removeAll()
        self.warningDetail.removeAll()
        self.title = [" ","BARO PARAMETERS REPORT"]
        self.detail = [" "," "]
        self.warningTitle = [" ","ATTENTION REQUIRED"]
        self.warningDetail = [" "," "]
    }
    
    func appendWarning() {
        NSLog("APPENDING \(self.warningTitle.count)")
               
        if self.warningTitle.count > 2 
		{
            self.title = self.warningTitle + self.title
            self.detail = self.warningDetail + self.detail
        }
    }

}
