//
//  takeoffGradients.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 16.09.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation

extension TakeoffLayout {
    
    //Gradients calculations
    func Gradients ()
    {
        
        logInstance.title += ["CALC GRADIENTS"]
        logInstance.detail += [" "]
        //----------------GRAD3-120 7.12-7.14
        switch self.Time {
        case 6:
            var v714column1 :Double = 0
            if let n714011 = NomogramRetriever.shared.getNom2D(714011)
            {
                NSLog("7.14 : 714011 loaded")
                if let x = n714011.getFByValues(self.AOT, x2: self.AltitudeForCalculation)
                    {
                        v714column1 = x
                        if self.deltaISA > 30
                            {
                                logInstance.warningTitle += ["7.14 Delta T ISA Deviation, double check"]
                                logInstance.warningDetail += [" "]
                            }
                            NSLog("7.14 : column 1 GRAD=\(v714column1)")
                            logInstance.title += ["7.14/1"]
                            logInstance.detail += ["\(v714column1)"]
                            }
                            else
                            {
                            NSLog("7.14 : column 1 error")
                            }
                    }
                    else
                    {
                    NSLog("7.14 : 714011 not found")
                    }
                
                var v714column2 :Double = 0
                if let n714021 = NomogramRetriever.shared.getNom2D(714021)
                {
                    NSLog("7.14 : 714021 loaded")
                    if let x = n714021.getFByValues(self.AbsoluteTOW, x2: v714column1)
                    {
                        v714column2 = x
                        if v714column2 > 19 || v714column2 < 0.2
                        {
                            NSLog("7.14 : column 2 GRAD out of range [0-19]" )
                        }
                        NSLog("7.14 : column 2 GRAD=\(v714column2)")
                        
                        logInstance.title += ["7.14/2"]
                        logInstance.detail += ["\(v714column2)"]
                    }
                    else
                    {
                        NSLog("7.14 : column 2 error")
                    }
                }
                else
                {
                    NSLog("7.14 : 714021 not found")
                }
               
               var v714column3 :Double = 0
               if let n714031 = NomogramRetriever.shared.getNom2D(714031)
               {
                    NSLog("7.14 : 714031 loaded")
                    if let x = n714031.getFByValues(self.Headwind, x2: v714column2)
                    {
                        v714column3 = x
                        if v714column3 > 21 || v714column3 < 0.2
                        {
                            NSLog("7.14 : column 3 GRAD out of range [0-21]" )
                        }
                        NSLog("7.14 : column 3 GRAD=\(v714column3)")
                    
                        logInstance.title += ["7.14/3"]
                        logInstance.detail += ["\(v714column3)"]
                    }
                    else
                    {
                        NSLog("7.14 : column 3 error")
                    }
                }
                else
                {
                    NSLog("7.14 : 714031 not found")
                }
               
               
               if let n714041 = NomogramRetriever.shared.getNom2D(714041)
               {
                    NSLog("7.14 : 714041 loaded")
                   if let x = n714041.getFByValues(self.activeRUD, x2: v714column3)
                    {
                        self.Grad3120 = x
                        if x > 21 || x < 0.2
                        {
                            NSLog("7.14 : column 4 GRAD out of range [0-21]" )
                            
                        }
                           // NSLog("7.14 : column 4 GRAD=\(self.Grad3120)")
                            logInstance.title += ["7.14/4 Gradient 3 eng 120 m"]
                            logInstance.detail += ["\(self.Grad3120!)"]
                        }
                        else
                        {
                            NSLog("7.14 : column 4 error")
                        }
                }
                else
                {
                    NSLog("7.14 : 714041 not found")
                }
            
        case 4:
            
            var v713column1 :Double = 0
            if let n713011 = NomogramRetriever.shared.getNom2D(713011)
            {
                NSLog("7.13 : 713011 loaded")
                if let x = n713011.getFByValues(self.AOT, x2: self.AltitudeForCalculation)
                {
                    v713column1 = x
                    if self.deltaISA > 30
                    {
                        logInstance.warningTitle += ["7.13 Delta T ISA Deviation, double check"]
                        logInstance.warningDetail += [" "]
                    }
                    NSLog("7.13 : column 1 GRAD=\(v713column1)")
                    logInstance.title += ["7.13/1"]
                    logInstance.detail += ["\(v713column1)"]
                }
                else
                {
                    NSLog("7.13 : column 1 error")
                }
            }
            else
            {
                NSLog("7.13 : 713011 not found")
            }
            
            var v713column2 :Double = 0
            if let n713021 = NomogramRetriever.shared.getNom2D(713021)
            {
                NSLog("7.13 : 713021 loaded")
                if let x = n713021.getFByValues(self.AbsoluteTOW, x2: v713column1)
                {
                    v713column2 = x
                    if v713column2 > 19 || v713column2 < 0.2
                    {
                        NSLog("7.13 : column 2 GRAD out of range [0-19]" )
                    }
                    NSLog("7.13 : column 2 GRAD=\(v713column2)")
                    logInstance.title += ["7.13/2"]
                    logInstance.detail += ["\(v713column2)"]
                }
                else
                {
                    NSLog("7.13 : column 2 error")
                }
            }
            else
            {
                NSLog("7.13 : 713021 not found")
            }
            
            var v713column3 :Double = 0
            if let n713031 = NomogramRetriever.shared.getNom2D(713031)
            {
                NSLog("7.13 : 713031 loaded")
                if let x = n713031.getFByValues(self.Headwind, x2: v713column2)
                {
                    v713column3 = x
                    if v713column3 > 21 || v713column3 < 0.2
                    {
                        NSLog("7.13 : column 3 GRAD out of range [0-21]" )
                    }
                    NSLog("7.13 : column 3 GRAD=\(v713column3)")
                    logInstance.title += ["7.13/3"]
                    logInstance.detail += ["\(v713column3)"]
                }
                else
                {
                    NSLog("7.13 : column 3 error")
                }
            }
            else
            {
                NSLog("7.13 : 713031 not found")
            }
            
            
            if let n713041 = NomogramRetriever.shared.getNom2D(713041)
            {
                NSLog("7.13 : 713041 loaded")
                if let x = n713041.getFByValues(self.activeRUD, x2: v713column3)
                {
                    self.Grad3120 = x
                    if x > 21 || x < 0.2
                    {
                        NSLog("7.13 : column 4 GRAD out of range [0-21]" )
                    }
                   // NSLog("7.13 : column 4 GRAD=\(self.Grad3120)")
                    logInstance.title += ["7.13/4 Gradient 3 eng 120 m"]
                    logInstance.detail += ["\(self.Grad3120!)"]
                }
                else
                {
                    NSLog("7.13 : column 4 error")
                }
            }
            else
            {
                NSLog("7.13 : 713041 not found")
            }
            
        case 2:
            
            var v712column1 :Double = 0
            if let n712011 = NomogramRetriever.shared.getNom2D(712011)
            {
                NSLog("7.12 : 712011 loaded")
                if let x = n712011.getFByValues(self.AOT, x2: self.AltitudeForCalculation)
                {
                    v712column1 = x
                    
                    //let margin = 0.1538462 * self.AOT + 0.6769231
                    
                    if self.deltaISA > 30
                    {
                        logInstance.warningTitle += ["7.12 Delta T ISA Deviation, double check"]
                        logInstance.warningDetail += [" "]
                    }
                    NSLog("7.12 : column 1 GRAD=\(v712column1)")
                    logInstance.title += ["7.12/1"]
                    logInstance.detail += ["\(v712column1)"]
                }
                else
                {
                NSLog("7.12 : column 1 error")
                }
            }
            else
            {
            NSLog("7.12 : 712011 not found")
            }
            
            var v712column2 :Double = 0
            if let n712021 = NomogramRetriever.shared.getNom2D(712021)
            {
                NSLog("7.12 : 712021 loaded")
                if let x = n712021.getFByValues(self.AbsoluteTOW, x2: v712column1)
                {
                    v712column2 = x
                    if v712column2 > 19 || v712column2 < 0.2
                    {
                        NSLog("7.12 : column 2 GRAD out of range [0-19]" )
                    }
                    NSLog("7.12 : column 2 GRAD=\(v712column2)")
                    logInstance.title += ["7.12/2"]
                    logInstance.detail += ["\(v712column2)"]
                }
                else
                {
                    NSLog("7.12 : column 2 error")
                }
            }
            else
            {
                NSLog("7.12 : 712021 not found")
            }
            
            var v712column3 :Double = 0
            if let n712031 = NomogramRetriever.shared.getNom2D(712031)
            {
                NSLog("7.12 : 712031 loaded")
                if let x = n712031.getFByValues(self.Headwind, x2: v712column2)
                {
                    v712column3 = x
                    if v712column3 > 21 || v712column3 < 0.2
                    {
                        NSLog("7.12 : column 3 GRAD out of range [0-21]" )
                    }
                    NSLog("7.12 : column 3 GRAD=\(v712column3)")
                    logInstance.title += ["7.12/3"]
                    logInstance.detail += ["\(v712column3)"]
                }
                else
                {
                    NSLog("7.12 : column 3 error")
                }
            }
            else
            {
                NSLog("7.12 : 712031 not found")
            }
            
            
            if let n712041 = NomogramRetriever.shared.getNom2D(712041)
            {
                NSLog("7.12 : 712041 loaded")
                if let x = n712041.getFByValues(self.activeRUD, x2: v712column3)
                {
                    self.Grad3120 = x
                    if x > 21 || x < 0.2
                    {
                        NSLog("7.12 : column 4 GRAD out of range [0-21]" )
                    }
                    NSLog("7.12 : column 4 GRAD=\(String(describing: self.Grad3120))")
                    logInstance.title += ["7.12/4 Gradient 3 eng 120 m"]
                    logInstance.detail += ["\(self.Grad3120!)"]
                }
                else
                {
                    NSLog("7.12 : column 4 error")
                }
            }
            else
            {
                NSLog("7.12 : 712041 not found")
            }

            
        default: self.Grad3120 = 99
        
    }//End of switch, End of Grad3-120 7.12-7.14
        
    //-----------------------GRAD4-120m 7.16
        
        var v716column1 :Double = 0
        if let n716011 = NomogramRetriever.shared.getNom2D(716011)
        {
            NSLog("7.16 : 716011 loaded")
            if let x = n716011.getFByValues(self.AOT, x2: self.AltitudeForCalculation)
            {
                v716column1 = x
                if self.deltaISA > 30
                {
                    logInstance.warningTitle += ["7.16 Delta T ISA Deviation, double check"]
                    logInstance.warningDetail += [" "]
                }
                NSLog("7.16 : column 1 GRAD=\(v716column1)")
                logInstance.title += ["7.16/1"]
                logInstance.detail += ["\(v716column1)"]
            }
            else
            {
             NSLog("7.16 : column 1 error")
            }
        }
        else
        {
         NSLog("7.16 : 716011 not found")
        }
        
        var v716column2 :Double = 0
        if let n716021 = NomogramRetriever.shared.getNom2D(716021)
        {
            NSLog("7.16 : 716021 loaded")
            if let x = n716021.getFByValues(self.AbsoluteTOW, x2: v716column1)
            {
                v716column2 = x
                if v716column2 > 19 || v716column2 < 0.2
                {
                    NSLog("7.16 : column 2 GRAD out of range [0-19]" )
                }
                NSLog("7.16 : column 2 GRAD=\(v716column2)")
                logInstance.textmsg = logInstance.textmsg + "\n7.16/2 = \(v716column2)"
                logInstance.title += ["7.16/2"]
                logInstance.detail += ["\(v716column2)"]
            }
            else
            {
                NSLog("7.16 : column 2 error")
            }
        }
        else
        {
            NSLog("7.16 : 716021 not found")
        }
        
        var v716column3 :Double = 0
        if let n716031 = NomogramRetriever.shared.getNom2D(716031)
        {
            NSLog("7.16 : 716031 loaded")
            if let x = n716031.getFByValues(self.Headwind, x2: v716column2)
            {
                v716column3 = x
                if v716column3 > 21 || v716column3 < 0.2
                {
                    NSLog("7.16 : column 3 GRAD out of range [0-21]" )
                }
                NSLog("7.16 : column 3 GRAD=\(v716column3)")
                logInstance.textmsg = logInstance.textmsg + "\n7.16/3 = \(v716column3)"
                logInstance.title += ["7.16/3"]
                logInstance.detail += ["\(v716column3)"]
            }
            else
            {
                NSLog("7.16 : column 3 error")
            }
        }
        else
        {
            NSLog("7.16 : 716031 not found")
        }
        
        
        if let n716041 = NomogramRetriever.shared.getNom2D(716041)
        {
            NSLog("7.16 : 716041 loaded")
            if let x = n716041.getFByValues(self.activeRUD, x2: v716column3)
            {
                self.Grad4120 = x
                if x > 21 || x < 0.2
                {
                    NSLog("7.16 : column 4 GRAD out of range [0-21]" )
                }
               // NSLog("7.16 : column 4 GRAD=\(self.Grad4120)")
                logInstance.title += ["7.16/4 Gradient 4 eng 120 m"]
                logInstance.detail += ["\(self.Grad4120!)"]
            }
            else
            {
                NSLog("7.16 : column 4 error")
            }
        }
        else
        {
            NSLog("7.16 : 716041 not found")
        }

        //-----------------------GRAD3-400m 7.15
        
        var v715column1 :Double = 0
        if let n715011 = NomogramRetriever.shared.getNom2D(715011)
        {
            NSLog("7.15 : 715011 loaded")
            if let x = n715011.getFByValues(self.AOT, x2: self.AltitudeForCalculation)
            {
                v715column1 = x
                
                //let margin = 0.1115385 * self.AOT + 0.1192308
                
                if self.deltaISA > 30
                {
                    logInstance.warningTitle += ["7.15 Delta T ISA Deviation, double check"]
                    logInstance.warningDetail += [" "]
                }
                NSLog("7.15 : column 1 GRAD=\(v716column1)")
                logInstance.title += ["7.15/1"]
                logInstance.detail += ["\(v715column1)"]
            }
            else
            {
             NSLog("7.15 : column 1 error")
            }
        }
        else
        {
         NSLog("7.15 : 715011 not found")
        }
        
        var v715column2 :Double = 0
        if let n715021 = NomogramRetriever.shared.getNom2D(715021)
        {
            NSLog("7.15 : 715021 loaded")
            if let x = n715021.getFByValues(self.AbsoluteTOW, x2: v715column1)
            {
                v715column2 = x
                if v715column2 > 14.5 || v715column2 < 0.2
                {
                    NSLog("7.15 : column 2 GRAD out of range [0-14.5]" )
                }
                NSLog("7.15 : column 2 GRAD=\(v715column2)")
                logInstance.title += ["7.15/2"]
                logInstance.detail += ["\(v715column2)"]

            }
            else
            {
                NSLog("7.15 : column 2 error")
            }
        }
        else
        {
            NSLog("7.15 : 715021 not found")
        }
        
        
        if let n715031 = NomogramRetriever.shared.getNom2D(715031)
        {
            NSLog("7.15 : 715031 loaded")
            if let x = n715031.getFByValues(self.Headwind, x2: v715column2)
            {
                self.Grad3400 = x
                if x > 14.5 || x < 0.2
                {
                    NSLog("7.15 : column 3 GRAD out of range [0-14.5]" )
                }
                //NSLog("7.15 : column 3 GRAD=\(self.Grad3400)")
                logInstance.title += ["7.15/3 Gradient 3 eng 400"]
                logInstance.detail += ["\(self.Grad3400!)"]

            }
            else
            {
                NSLog("7.15 : column 3 error")
            }
        }
        else
        {
            NSLog("7.15 : 715031 not found")
        }

        //-----------------------GRAD4-400m 7.17
        
        var v717column1 :Double = 0
        if let n717011 = NomogramRetriever.shared.getNom2D(717011)
        {
            NSLog("7.17 : 717011 loaded")
            if let x = n717011.getFByValues(self.AOT, x2: self.AltitudeForCalculation)
            {
                v717column1 = x
                
                if self.deltaISA > 30
                {
                    logInstance.warningTitle += ["7.17 Delta T ISA Deviation, double check"]
                    logInstance.warningDetail += [" "]
                }
                NSLog("7.17 : column 1 GRAD=\(v716column1)")
                logInstance.title += ["7.17/1"]
                logInstance.detail += ["\(v717column1)"]
            }
            else
            {
             NSLog("7.17 : column 1 error")
            }
        }
        else
        {
         NSLog("7.17 : 717011 not found")
        }
        
        var v717column2 :Double = 0
        if let n717021 = NomogramRetriever.shared.getNom2D(717021)
        {
            NSLog("7.17 : 717021 loaded")
            if let x = n717021.getFByValues(self.AbsoluteTOW, x2: v717column1)
            {
                v717column2 = x
                if v717column2 > 14.5 || v717column2 < 0.2
                {
                    NSLog("7.17 : column 2 GRAD out of range [0-14.5]" )
                }
                NSLog("7.17 : column 2 GRAD=\(v717column2)")
                logInstance.title += ["7.17/2"]
                logInstance.detail += ["\(v717column2)"]
            }
            else
            {
                NSLog("7.17 : column 2 error")
            }
        }
        else
        {
            NSLog("7.17 : 717021 not found")
        }
        
        
        if let n717031 = NomogramRetriever.shared.getNom2D(717031)
        {
            NSLog("7.17 : 717031 loaded")
            if let x = n717031.getFByValues(self.Headwind, x2: v717column2)
            {
                self.Grad4400 = x
                if x > 14.5 || x < 0.2
                {
                    NSLog("7.17 : column 3 GRAD out of range [0-14.5]" )
                }
                NSLog("7.17 : column 3 GRAD=\(String(describing: self.Grad4400))")
                logInstance.title += ["7.17/3 Gradient 4 eng 400"]
                logInstance.detail += ["\(self.Grad4400!)"]
            }
            else
            {
                NSLog("7.17 : column 3 error")
            }
        }
        else
        {
            NSLog("7.17 : 717031 not found")
        }

        
    }
}
