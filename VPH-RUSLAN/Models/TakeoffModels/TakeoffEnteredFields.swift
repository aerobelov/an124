//
//  TakeoffEnteredFields.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 01.07.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation

enum warmTime: Int {
    case two = 2
    case four = 4
    case six = 6
    case zero = 0
    
    init(from minutes: String) {
        let number = Int(minutes)
        switch number {
            case 2: self = warmTime.two
            case 4: self = warmTime.four
            case 6: self = warmTime.six
            default: self = warmTime.zero
        }
    }
}

struct TakeoffEntered {
    
    var TORA: Int = 3000
    var TODA: Int = 3000
    var ASDA: Int = 3000
    var actualTOW: Double = 0
    var RWYHeading: Int = 180
    var windDirection: Int = 180
    var windSpeed: Double = 0
    var QNH: Double = 1013.2
    var slope: Double = 0
    var AOT: Double = 15
    var NFC: Double = 0.6
    var elevation: Int = 0
    var time: warmTime = .four
    var SID: Double = 3.3
    var rud: Int = 120
    var vvIsOne: Bool = false
    var isUserWeight: Bool = false
    var isPressureAltitude = true
    var desiredRud: Int = 120
    
    //VV1 mode button var
    let vLabelByRLE: String = "V1Vr/RLE"
    let vLabelIsOne: String = "V1Vr=1"
    var vCurrentLabel: String {
        switch self.vvIsOne {
        case true:
            return vLabelIsOne
        case false:
            return vLabelByRLE
        }
    }
    
    //Check tailwind in in range
    var tailWindOK: Bool {
        return (-5...30 ~= (self.windSpeed * cos((Double(self.windDirection - self.RWYHeading) * 3.14159265359) / 180)))
    }
    
    //IsUserWeight button var
    let userWeightLabel = "USER TOW"
    let calcWeightLabel = "CALC TOW"
    var weightCurrentLabel: String {
        switch self.isUserWeight {
        case true:
            return userWeightLabel
        case false:
            return calcWeightLabel
        }
    }
    var userValue = "350"
    var calcValue = "0"
    var weightCurrentValue: String {
        switch self.isUserWeight {
        case true:
            return userValue
        case false:
            return calcValue
        }
    }
    
    //ALTITUDE TYPE BUTTON LABELS
    let isPressureAltitudeLabel = "PR. ALT"
    let isElevationLabel = "ELEV"
    var altitudeCurrentlabel: String {
        switch self.isPressureAltitude {
        case true:
            return isPressureAltitudeLabel
        case false:
            return isElevationLabel
        }
    }
    
    init() {
        
    }
    
    mutating func vvToggle() {
        switch vvIsOne {
        case false:
            vvIsOne = true
        case true:
            vvIsOne = false
        }
    }
    
    mutating func weightToggle() {
        switch isUserWeight {
        case true:
            isUserWeight = false
        case false:
            isUserWeight = true
        }
    }
    
    mutating func altitudeToggle() {
        switch isPressureAltitude {
        case true:
            isPressureAltitude = false
        case false:
            isPressureAltitude = true
        }
    }
    
  
    
    
}
