//
//  TakeoffEnteredFields.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 01.07.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation

struct LandingEntered {
    
    var LDA: Int = 3000
    var actualLDW: Double = 300
    var RWYHeading: Int = 180
    var windDirection: Int = 180
    var windSpeed: Double = 0
    var QNH: Double = 1013.2
    var slope: Double = 0
    var AOT: Double = 15
    var NFC: Double = 0.6
    var gradient: Double = 3
    var elevation: Int = 0
    var isUserWeight: Bool = false
    var isPressureAltitude = true
    var isDestination = true
    
    //IS destination button var
    let isDestLabel: String = "DEST"
    let isAltLabel: String = "ALT"
    var destinationCurrentLabel: String {
        switch self.isDestination {
        case true:
            return isDestLabel
        case false:
            return isAltLabel
        }
    }
    
    //Check tailwind in in range
    var tailWindOK: Bool {
        return (-5...30 ~= (self.windSpeed * cos((Double(self.windDirection - self.RWYHeading) * 3.14159265359) / 180)))
    }
    
    //IsUserWeight button var
    let userWeightLabel = "USER LDW"
    let calcWeightLabel = "CALC LDW"
    var weightCurrentLabel: String {
        return self.isUserWeight ? userWeightLabel : calcWeightLabel
    }
    
    var userValue = "300"
    var calcValue = "0"
    var weightCurrentValue: String {
        switch self.isUserWeight {
        case true:
            return userValue
        case false:
            return calcValue
        }
    }
    
    //ALTITUDE TYPE BUTTON LABELS
    let isPressureAltitudeLabel = "PR. ALT"
    let isElevationLabel = "ELEV"
    var altitudeCurrentlabel: String {
        switch self.isPressureAltitude {
        case true:
            return isPressureAltitudeLabel
        case false:
            return isElevationLabel
        }
    }
    
    init() {
        
    }
    
    mutating func destToggle() {
        switch isDestination {
        case false:
            isDestination = true
        case true:
            isDestination = false
        }
    }
    
    mutating func weightToggle() {
        switch isUserWeight {
        case true:
            isUserWeight = false
        case false:
            isUserWeight = true
        }
    }
    
    mutating func altitudeToggle() {
        switch isPressureAltitude {
        case true:
            isPressureAltitude = false
        case false:
            isPressureAltitude = true
        }
    }
    
  
    
    
}
