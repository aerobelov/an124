//
//  Landingclass.swift
//  VDAVPHCalc
//
//  Created by Pavel on 09/08/2015.
//  Copyright (c) 2015 Volga-Dnepr Airline. All rights reserved.
//

import Foundation

class LandingLayout  {
    
    let nfcdictionary: [Double:Double] = [0.4:8.0,0.41:8.4,0.42:8.8,0.43:9.2,0.44:9.6,0.45:10.0,0.46:10.5,0.47:10.8,0.48:11.3,0.49:11.8,0.50:12.0,0.51:12.4,0.52:12.6,0.53:13.1,0.54:13.2,0.55:13.8,0.56:14.0,0.57:14.4,0.58:14.6,0.59:14.8,0.6:15.0]
    var value :String = ""
    
    //Form data
    var LDA :Double
    var ActualLDW :Double
    var RWYHeading :Double
    var WindDirection :Double
    var WindSpeed :Double
    var QNH :Double
    var Slope :Double
    var AOT :Double
    var NFC :Double
    var Elevation :Double
    var MAPPGradient :Double
    
    //switches
    var isPressureAltitude :Bool = true
    var IsDestination :Bool = true
    var isUserWeight :Bool = false
    
    //calculated
    var PressureAltitude :Double?
    var CalculatedLDW :Double = 10000 //need for calc
    var AltitudeForCalculation :Double = 0
    var AbsoluteLDW :Double = 250
    var Lrun :Double = 0
    var Dland :Double = 0
    var Dlandreq :Double = 0
    var Dlandreqwet :Double = 0
    var QFE :Double?
    
    //database parameters
    var allAerodromeCode:[String] = []
    var allAerodromeNames:[String] = []
    var icaoCodeIndex: Int = 5566
    var icaoCodeName: String = "UUEE"
    var allRunways: [String] = []
    var myRunway: String = ""
    
    init() {
        self.LDA = 3000
        self.ActualLDW = 250
        self.RWYHeading = 180
        self.WindDirection = 180
        self.WindSpeed = 0
        self.QNH = 1013
        self.Slope = 0
        self.AOT = 15
        self.NFC = 0.6
        self.Elevation = 0
        self.MAPPGradient = 3
    }
    
    func upload (from object: LandingEntered) {
        self.LDA = Double(object.LDA)
        self.ActualLDW = Double(object.actualLDW)
        self.RWYHeading = Double(object.RWYHeading)
        self.WindDirection = Double(object.windDirection)
        self.WindSpeed = object.windSpeed
        self.QNH = object.QNH
        self.AOT = object.AOT
        self.Slope = object.slope
        self.NFC = object.NFC
        self.Elevation = Double(object.elevation)
        self.MAPPGradient = object.gradient
        self.isUserWeight = object.isUserWeight
        self.IsDestination = object.isDestination
        self.isPressureAltitude = object.isPressureAltitude
    }
    
    var Headwind :Double {
        let x = WindSpeed * cos(((WindDirection - RWYHeading) * 3.14159265359) / 180) //Расчет попутной/встречной составляющей +=head, -=tail
        if -5...30 ~= x {
            return x
        } else { return 0 }
    }
    
    var Crosswind: Double {
        let x = -WindSpeed * sin(((WindDirection - RWYHeading) * 3.14159265359) / 180) //Расчет боковой составляющей
        return x
    }
    
    func checkTail(Heading h:Double, Winddir dir: Double, Windspd spd: Double) -> Bool {
        return (-5...30 ~= (spd * cos(((dir - h) * 3.14159265359) / 180)))
    }
    
    var MaxCrosswind: Double {
        return nfcdictionary[NFC]!
    }

    var Gradient3engines :Double = 0
    var Gradient4engines :Double = 0
    var LDWmaxLDA :Double = 0
    var LDWmax27 :Double = 0
    var LDWmaxMAP3 :Double = 0
    var LDWmaxMAP4 :Double = 0
    var ApproachSpeed :Double = 0
    var TouchdownSpeed :Double = 0
    var BrakeSpeed :Double = 0
    var AutobrakeLDW :Double = 0
    var LDWmaxMAP2 :Double = 0
    var DescisionHeight2 :Double = 0
    var ISAT :Double = 0
    var ISAPmmhg :Double = 0
    var ISAPhpa :Double = 0
    var LogText :String = ""
   
    
    //Определение коэфициента для сухой полосы
    var AirportType :Double {
        switch IsDestination {
            case true: return 1.67
            case false: return 1.43
        }
    }
    
    
    var LandingDistanceAvailable :Double = 0
    var Stop :Bool = false
    
    func Pressure () {
        let h_array :[Double] = [-200,-100,0,100,200,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2100,2200,2300,2400,2500,2600,2700,2800,2900,3000,3100,3200,3300,3400,3500,3600,3700,3800,3900,4000,4100,4200,4300,4400]
        let p_array :[Double] = [1036,1026,1013.25,1002,990,979,968,956,943,933,921,909,899,888,878,868,855,845,835,825,815,805,795,786,775,765,756,748,738,729,719,710,701,692,682,675,666,658,649,641,632,625,617,609,601,594,585]
        
        if self.Elevation > -200 && self.Elevation < 4400 
		{
            for index in ( 0...46) {
			
                if self.Elevation <= h_array[index] 
				{
                    NSLog("Index=\(index)")
                    self.ISAPhpa = p_array[index - 1]
                    let m = (self.Elevation - h_array[index - 1]) * (p_array[index - 1] - p_array[index]) / (h_array[index] - h_array[index - 1])
                    self.ISAPhpa = self.ISAPhpa - m
                    break
                }
            }
            self.ISAPmmhg = self.ISAPhpa * 0.750061683
        }
        
        NSLog("ISAP=\(self.ISAPhpa)")
        NSLog("QNH=\(self.QNH)")
        self.ISAT = -0.0065 * self.Elevation + 15
       
        logInstance.title += ["ISA pressure, hpa "]
        logInstance.detail += ["\(self.ISAPhpa)"]
        
        logInstance.title += ["ISA pressure, mmhg"]
        logInstance.detail += ["\(self.ISAPmmhg)"]
        
        logInstance.title += ["ISA temperature"]
        logInstance.detail += ["\(self.ISAT)"]

        
        if (self.isPressureAltitude) && (self.ISAPhpa > 695) && (self.ISAPhpa < 1036 )  {
        let ZeroDev :Double = 1013.25 - self.ISAPhpa
        self.QFE  = self.QNH  - ZeroDev
           if let qfe = self.QFE {
                logInstance.title += ["QFE"]
                logInstance.detail += ["\(qfe)"]
                NSLog("QFE=\(qfe)")
            
                if 585...1036 ~= qfe
                {
                    for index in (0...46).reversed() { //var index = 46; index >= 0; index -= 1
				
                        if qfe <= p_array[index]  {
                            NSLog("Index2=\(index)")
                            let mult = (h_array[index] - h_array[index+1]) / (p_array[index] - p_array[index + 1])
                            self.PressureAltitude = h_array[index] - (p_array[index] - qfe) * mult
                            break
                        }
                    }
                    
                logInstance.title += ["Pressure Altitude"]
                let pa_info = self.PressureAltitude ?? 0
                logInstance.detail += ["\(pa_info)"]
                }
            }
        }
        else 
		{
          
            logInstance.title += ["Pressure Altitude has not been calculated"]
            logInstance.detail += [" "]
            self.PressureAltitude = nil
            self.QFE = nil
        }
       
    }
    
    var crosswindExeeded: Bool {
        return abs(MaxCrosswind) < abs(Crosswind)
    }
    
    func minWeightIndex() -> Int {
        let numbers = [ LDWmax27, LDWmaxLDA, LDWmaxMAP3, LDWmaxMAP4]
        let minimum = numbers.min()
        let minIndex = numbers.firstIndex(of: minimum!)!
        return minIndex
    }

    func calcLanding()
    {
        logInstance.title += ["LANDING REPORT"]
        logInstance.detail += [" "]
        
        
        switch self.isPressureAltitude {
            case false: self.AltitudeForCalculation = self.Elevation
            case true:
                if let pa = self.PressureAltitude {
                    self.AltitudeForCalculation = pa
                }
        }
        

        let deltaISA = self.AOT - ( -0.0065 * self.AltitudeForCalculation + 15 )
        NSLog("DeltaISA=\(deltaISA)")
        
        if deltaISA > 30 
		{
            logInstance.warningTitle += ["Delta T ISA deviation, attention required"]
            logInstance.warningDetail += ["\(deltaISA)"]
        }
               
        logInstance.title += ["Delta T ISA deviation"]
        logInstance.detail += ["\(deltaISA)"]
        
        //Номограмма 7.69
        if let n76901 = NomogramRetriever.shared.getNom2D(76901){
            var dWind :Double = 0
            NSLog("7.69: Found 76901")
            
            if let x = n76901.getFByValues(self.AOT, x2: self.AltitudeForCalculation)
            {
                self.LDWmax27 = x
                
                if deltaISA > 30
                {
                    logInstance.warningTitle += ["7.69/1 Elevation and Temperature, double check"]
                    logInstance.warningDetail += [" "]
                }
            }
            else { NSLog ("7.69: Error column 1") }
            
            //Корректировка графика на ветер при встречном
            if (self.Headwind < 0)
            {
                dWind = -(0.001875 * self.LDWmax27 - 0.3118057) * self.Headwind
            }
            //Корректировка графика на ветер при попутном
            else if (self.Headwind > 0)
            {
                dWind = (0.0016126 * self.LDWmax27 + 0.795942) * self.Headwind
            }
            else 
			{ 
				dWind = 0 
			}
            
            self.LDWmax27 +=  dWind
			logInstance.title += ["7.69 LDW max by 2.7 gradient"]
            logInstance.detail += ["\(self.LDWmax27)"]
            if self.LDWmax27 > 330 
			{ 
				self.LDWmax27 = 330 
			}
            NSLog("7.69: LDW max27 = \(self.LDWmax27)")
		}
        // Конец графика 7.69
        
        // расчет потребной посадочной дистанции от Ксц для влажной ВПП 7.73
        var Kmu :Double = 1
        switch self.NFC 
		{
        case 0.6...0.7: Kmu = 1
        case 0.5...0.6: Kmu = -1.5 * self.NFC + 1.9
        case 0.3...0.49: Kmu = -3.3510204 * self.NFC + 2.825
        default: Kmu = 0
        NSLog("Ksc out of range [0.3-0.7]; Kmu= \(Kmu)")
        }
        NSLog("Kmu= \(Kmu)")
        
        logInstance.title += ["Kmu"]
        logInstance.detail += ["\(Kmu)"]
        
		self.LandingDistanceAvailable = self.LDA / Kmu
        
        logInstance.title += ["7.73 LDA"]
        logInstance.detail += ["\(self.LandingDistanceAvailable)"]
        
        
        //** Н О М О Г Р А М М А  7.68 RIGHT TO LEFT**
        //График 1 Температура и превышение
        var vLDA12 :Double = 0
        var vLDA32 :Double = 0
        if let n76801 = NomogramRetriever.shared.getNom2D(76801)
		{
            NSLog("7.68 RL: 76801 loaded")
            if  let x = n76801.getFByValues(self.AOT, x2: self.AltitudeForCalculation)
            {
                vLDA12 = x
                if vLDA12 > 2200 || vLDA12 < 1050 
				{
                NSLog("7.68 RL: VLDA12 out of range" )
				}
                NSLog("7.68 RL: vLDA12= \(vLDA12)")
               
                logInstance.title += ["7.68/1"]
                logInstance.detail += ["\(vLDA12)"]
			}
            else
            {
                NSLog("7.68 RL: column 1 error")
            }
        }
        else 
		{ 
			NSLog("7.68 RL: File 76801 not found") 
		}
        
        //График 5 Определение Потребной посадочной дистанции
        let vLDA54:Double = self.LandingDistanceAvailable / self.AirportType    // дальше расчет по потребной дистанции
        NSLog("768 :L5to4= \(vLDA54)")
       
        logInstance.title += ["7.68/5 Airport koefficient"]
        logInstance.detail += ["\(self.AirportType)"]
        
        logInstance.title += ["7.68/5 Required Landing Distance"]
        logInstance.detail += ["\(vLDA54)"]
        
		//График 4 Уклон по формуле
        //В форме ввода написать, что уклон вверх это плюс, вниз это минус
        //В данном графике для + увеличиваем, для - уменьшаем [в обратном 7.68 наоборот], в каждом графике смотреть знак!
        
        let dSlope = (2.7198609e-11 * pow(vLDA54,3) - 9.2932e-8 * pow(vLDA54,2) + 2.909128e-4 * vLDA54 - 0.1277542) * self.Slope * 200
        var vLDA43:Double = 0
        
        if self.Slope < 0
        {
            vLDA43 = vLDA54 - dSlope
        }
        else if self.Slope >= 0
        {
            vLDA43 = vLDA54 + dSlope
        }
       
        logInstance.title += ["7.68/4"]
        logInstance.detail += ["\(vLDA43)"]

        NSLog("7.68 RL: deltaSlope= \(dSlope)")
        
        // Столбец 3 Ветер
        if let n768032 = NomogramRetriever.shared.getNom2D(768032)
		{
            NSLog("7.68 RL: 768032 loaded")
            if self.Headwind < -5 || self.Headwind > 30 
			{
                NSLog("7.68 RL: WIND out of range [-5:30]") 
			}
            if let x = n768032.getFByValues(self.Headwind, x2: vLDA43)
            {
                vLDA32 = x
                if vLDA32 > 3600 || vLDA32 < 600 
				{
                    NSLog("7.68 RL: VLDA32 out of range [600-3600]" )
				}
                NSLog("7.68 RL: vLDA32=\(vLDA32)")
                logInstance.title += ["7.68/3"]
                logInstance.detail += ["\(vLDA32)"]
            }
                
            else
            {
                NSLog("7.68 RL: column 3 error")
            }
            
        }
        else { NSLog("7.68 RL: File 768032 not found") }
        
        
        // Столбец 2 Основной расчет
        if let n768023 = NomogramRetriever.shared.getNom2D(768023)
		{
            NSLog("7.68 RL: 768023 loaded")
            if let x = n768023.getFByValues(vLDA12, x2: vLDA32)
            {
                self.LDWmaxLDA = x
                if self.LDWmaxLDA < 200 || self.LDWmaxLDA > 450 
				{
                    NSLog("7.68 RL: vGLDA out of range [200-450]")
				}
                NSLog("7.68 RL: vGLDA=\(self.LDWmaxLDA)")
               
                logInstance.title += ["7.68/2"]
                logInstance.detail += ["\(self.LDWmaxLDA)"]
            }
            else
            {
                NSLog("7.68 RL: column 2 error")
            }
        }
        else 
		{ 
			NSLog("7.68 RL: File 768023 not found") 
		}
		logInstance.title += ["7.68/2 LDW max by LDA"]
        logInstance.detail += ["\(self.LDWmaxLDA)"]
        if self.LDWmaxLDA > 330 
		{ 
			self.LDWmaxLDA = 330 
		}
                
        //Конец графика 7.6.8 Right to Left
        
        //** Н О М О Г Р А М М А 7.70 МАССА ОТ ГРАДИЕНТА УХОДА НА 4 ДВИГАТЕЛЯХ**
        //Столбец 1 Высота температура
        var vGrad770_1 :Double = 0;
        if let n77001 = NomogramRetriever.shared.getNom2D (77001)
		{
            NSLog ("7.70: 77001 loaded 7.70 столбец 1, Гр ухода на 4 дв")
            if let x = n77001.getFByValues(self.AOT, x2: self.AltitudeForCalculation)
            {
                vGrad770_1 = x
                
                if deltaISA > 30
                {
                    logInstance.warningTitle += ["7.70/1 Elevation and Temperature, double check"]
                    logInstance.warningDetail += [" "]
                }
                
                NSLog("7.70: Gradient 7.70, column 1= \(vGrad770_1)")
				logInstance.title += ["7.70/1"]
                logInstance.detail += ["\(vGrad770_1)"]
            }
            else
            {
                NSLog("7.70: column 1 error")
            }
        }
        else 
		{ 
			NSLog("7.70: File 7.70 not found") 
		}
        
        //Столбец 3 Ветер
        var vGrad770_3 :Double = 0
        if let n770032 = NomogramRetriever.shared.getNom2D (770032)
		{
            NSLog ("7.70: 770032 loaded 7.70 столбец 3, Гр ухода на 4 дв")
            if let x = n770032.getFByValues(self.Headwind, x2: self.MAPPGradient)
            {
                vGrad770_3 = x
                if vGrad770_3 < 0 || vGrad770_3 > 24
				{
					NSLog("7.70: Gradient 7.70 column 3 out of range [0-24]")
				}
                NSLog("7.70: Gradient 7.70 column 3= \(vGrad770_3)")
                logInstance.title += ["7.70/3"]
                logInstance.detail += ["\(vGrad770_3)"]
            }
            else
            {
                NSLog("7.70: column 3 error")
            }
        }
        else 
		{ 
			NSLog("7.70: File 770032 not found") 
		}
        
        //Столбец 2 Определение массы
        
        if let n770023 = NomogramRetriever.shared.getNom2D (770023)
		{
            NSLog ("7.70: 770023 loaded 7.70 столбец 2, Град ухода на 4 дв")
            if let x = n770023.getFByValues(vGrad770_1, x2: vGrad770_3)
            {
                self.LDWmaxMAP4 = x
                if self.LDWmaxMAP4 < 200 || self.LDWmaxMAP4 > 400
				{
                    NSLog("7.70: Max GA G 4 en, column 2 out of range [200-400]")
				}
                NSLog("7.70: Max GA G 4 en, column 2= \(self.LDWmaxMAP4)")
                logInstance.title += ["7.70/2"]
                logInstance.detail += ["\(self.LDWmaxMAP4)"]
			}
            else
            {
                NSLog("7.70: column 2 error")
            }
        }
        else 
		{ 
			NSLog("7.70: File 770023 not found") 
		}
		logInstance.title += ["7.70/2 LDW max by MAP Gradient 4 eng"]
        logInstance.detail += ["\(self.LDWmaxMAP4)"]
        if self.LDWmaxMAP4 > 330 
		{ 
			self.LDWmaxMAP4 = 330 
		}
               
        // ** К О Н Е Ц   Н О М О Г Р А М М Ы  7.70 **
                
        //** Н О М О Г Р А М М А 7.71 МАССА ОТ ГРАДИЕНТА УХОДА НА 3 ДВИГАТЕЛЯХ**
        //Столбец 1 Высота температура
        var vGrad771_1 :Double = 0;
        if let n77101 = NomogramRetriever.shared.getNom2D (77101)
		{
            NSLog ("7.71: 77101 loaded 7.71 столбец 1, Град ухода на 3 дв")
            if let x = n77101.getFByValues(self.AOT, x2: self.AltitudeForCalculation)
            {
                vGrad771_1 = x
                           
                if deltaISA > 30
                {
                    logInstance.warningTitle += ["7.71/1 Elevation and Temperature, double check"]
                    logInstance.warningDetail += [" "]
                }
				
				NSLog("7.71: Gradient 7.71, column 1= \(vGrad771_1)")
                logInstance.title += ["7.71/1"]
                logInstance.detail += ["\(vGrad771_1)"]
            }
            else
            {
                NSLog("7.71: column 1 error")
            }
        }
        else 
		{ 
			NSLog("7.71: File 7.71 not found") 
		}
        
        //Столбец 3 Ветер
        var vGrad771_3 :Double = 0
        if let n771032 = NomogramRetriever.shared.getNom2D (771032)
		{
            NSLog ("7.71: 771032 loaded 7.71 столбец 3, Град ухода на 3 дв")
            NSLog("7.71: vHeadwind= \(self.Headwind) max grad= \(self.MAPPGradient)")
            if let x = n771032.getFByValues(self.Headwind, x2: self.MAPPGradient)
            {
                vGrad771_3 = x
                if vGrad771_3 < 0 || vGrad771_3 > 20
				{
					NSLog("7.71: Gradient 7.71 column 3 out of range [0-20]")
				}
                NSLog("7.71: Gradient 7.71 column 3= \(vGrad771_3)")
                logInstance.title += ["7.71/3"]
                logInstance.detail += ["\(vGrad771_3)"]
            }
            else
            {
                NSLog("7.71: column 3 error")
            }
        }
        else 
		{ 
			NSLog("7.71: File 771032 not found") 
		}
        
        //Столбец 2 Определение массы
       
        if let n771023 = NomogramRetriever.shared.getNom2D (771023)
		{
            NSLog ("7.71: 771023 loaded 7.71 столбец 2, Град ухода на 3 дв")
            if let x = n771023.getFByValues(vGrad771_1, x2: vGrad771_3)
            {
                self.LDWmaxMAP3 = x
                if self.LDWmaxMAP3 < 200 || self.LDWmaxMAP3 > 400
				{
                    NSLog("7.71: Max GA G  3 en, column 2 out of range [200-400]")
				}
                NSLog("7.71: Max GA G  3 en, column 2= \(self.LDWmaxMAP3)")
                logInstance.title += ["7.71/2"]
                logInstance.detail += ["\(self.LDWmaxMAP3)"]
            }
            else
            {
                NSLog("7.71: column 2 error")
            }
        }
        else 
		{ 
			NSLog("7.71: File 771023 not found") 
		}
		logInstance.title += ["7.71/2 LDW max by MAP gradient 3 eng"]
        logInstance.detail += ["\(self.LDWmaxMAP3)"]
        if self.LDWmaxMAP3 > 330 
		{ 
			self.LDWmaxMAP3 = 330 
		}
        
        
        // ** К О Н Е Ц   Н О М О Г Р А М М Ы  7.71 **
        
        
        
        //опр G, по которой ведутся расчеты (мин из [7.68 7.69 7.70 7.71] или введенная)
        
        let WeightArray: [Double] = [self.LDWmax27, self.LDWmaxLDA, self.LDWmaxMAP3, self.LDWmaxMAP4]
        self.CalculatedLDW = 10000
        for index in (0...3)   //var index = 0; index <= 3; ++index
		{
            if WeightArray[index] < self.CalculatedLDW 
			{
                self.CalculatedLDW = WeightArray[index]
            }
        }
        
        switch self.ActualLDW 
		{
        case 0: self.AbsoluteLDW = self.CalculatedLDW
        case 200...400: self.AbsoluteLDW = self.ActualLDW
        default: 	NSLog("Entered G out of range, AbsoluteG is max on RLE")
					self.ActualLDW = self.CalculatedLDW
        }
        NSLog("AbsoluteG is \(self.AbsoluteLDW)")
        logInstance.title += ["Calculation will be based on LDW"]
        logInstance.detail += ["\(self.AbsoluteLDW)"]

        
        //** Н О М О Г Р А М М А  7.67 ** Д Л И Н А  П Р О Б Е Г А
        //cтолбец 1 высота температура
      
        if let n76701 = NomogramRetriever.shared.getNom2D (76701)
		{
            NSLog ("7.67: 76701 loaded")
            if let x = n76701.getFByValues(self.AOT, x2: self.AltitudeForCalculation)
            {
                self.Lrun = x
                if self.Lrun < 0 || self.Lrun > 1800 
				{
                    NSLog("7.67: L run column 1 out of range [0-1800]")
				}
				NSLog("7.67: Lrun 7.67 column 1= \(self.Lrun)")
                logInstance.title += ["7.67/1"]
                logInstance.detail += ["\(self.Lrun)"]
            }
            else
            {
                NSLog("7.67: column 1 error")
            }
        }
        else 
		{ 
			NSLog("7.67: File 76701 not found") 
		}
        
        //столбец 2 масса
        if let n767021 = NomogramRetriever.shared.getNom2D (767021){
            NSLog ("767021 loaded")
            if let x = n767021.getFByValues(self.AbsoluteLDW, x2: self.Lrun)
            {
                self.Lrun = x
                if self.Lrun < 0 || self.Lrun > 3600 
				{
                    NSLog("7.67: L run column 2 out of range [0-3600]")
				}
                NSLog("7.67: Lrun 7.67 column 2= \(self.Lrun)")
                logInstance.title += ["7.67/2"]
                logInstance.detail += ["\(self.Lrun)"]
            }
            else
            {
                NSLog("7.67: column 2 error")
            }
        }
        else { NSLog("7.67: File 767021 not found") }
        
        //столбец 3 ветер
        if let n767031 = NomogramRetriever.shared.getNom2D (767031)
		{
            NSLog ("7.67: 767031 loaded")
            if let x = n767031.getFByValues(self.Headwind, x2: self.Lrun)
            {
                self.Lrun = x
                if self.Lrun < 200 || self.Lrun > 3600 
				{
                    NSLog("7.67: L run column 3 out of range [200-3600]")
				}
                NSLog("7.67: Lrun 7.67 column 3= \(self.Lrun)")
                logInstance.title += ["7.67/3"]
                logInstance.detail += ["\(self.Lrun)"]
            }
            else
            {
                NSLog("7.67: column 3 error")
            }
        }
        else 
		{ 
			NSLog("7.67: File 767031 not found") 
		}
        
        //Стобец 4 Уклон по формуле
        var dSlope767 :Double = 0
        let x5 :Double = -1.5126343e-14 * pow(self.Lrun, 5)
        let x4 :Double = 1.2594539e-10 * pow(self.Lrun, 4)
        let x3 :Double = -3.8793075e-7 * pow(self.Lrun, 3)
        let x2 :Double = 5.5606276e-4 * pow(self.Lrun,2)
        let x1 :Double = -0.3219258 * self.Lrun + 73.0769078
        dSlope767 = (x5 + x4 + x3 + x2 + x1) * self.Slope
        self.Lrun = self.Lrun + dSlope767
        self.Lrun = self.Lrun * Kmu
        
        logInstance.title += ["7.67/4 L run"]
        logInstance.detail += ["\(self.Lrun)"]
        //К О Н Е Ц   Н О М О Г Р А М М Ы   7.67
        
        
        // Н О М О Г Р А М М А   7.72 СКОРОСТЬ ВКЛЮЧЕНИЯ ТОРМОЗОВ
        
       
        if (self.AbsoluteLDW <= 410 && self.AbsoluteLDW >= 200) && (self.AltitudeForCalculation >= 0 && self.AltitudeForCalculation <= 3000) && (self.Headwind <= 30 && self.Headwind >= -5)
        {
            //ГРАФИК 1
            let y1 = 53167.0235896 / self.AbsoluteLDW + 92.9432318
            NSLog("7.72 график 1 результат = \(y1)")
          
            logInstance.title += ["7.72/1"]
            logInstance.detail += ["\(y1)"]
            
            //ГРАФИК 2
            var dS :Double = 0
            if self.Headwind >= 0 
			{
                dS = -0.0085714 * pow (self.Headwind,2) + 3.8857143 * self.Headwind + 0.2142857
            }
            else 
			{
                dS = (-18 / -5) * self.Headwind
            }
            let y2 = y1 + dS
            
            logInstance.title += ["7.72/2"]
            logInstance.detail += ["\(y2)"]
            NSLog("7.72 график 2 результат = \(y2)")
            
            //ГРАФИК 3
            let k = -2.457556e-5 * self.AltitudeForCalculation + 0.5004762
            var b :Double = 0
            switch self.AltitudeForCalculation
			{
            case 0...1000 :     b = 85 + (( 85.2380952 - 85 ) * ( self.AltitudeForCalculation - 0 ) / 1000 )
            case 1001...2000 :  b = 85.2380952 - (( 85.2380952 - 84.4347826 ) * ( self.AltitudeForCalculation - 1000 ) / 1000 )
            case 2001...3000 :  b = 84.4347826 - (( 84.4347826 - 83.2173913 ) * ( self.AltitudeForCalculation - 2000 ) / 1000 )
            default : NSLog("AltitudeForCalculation 7.72 график 3 out of range")
            }
            NSLog("7.72 k=\(k), b=\(b)")
            let y3 = k * y2 - b
            NSLog("7.72 график 3 результат = \(y3)")
            logInstance.title += ["7.72/3"]
            logInstance.detail += ["\(y3)"]
            
            //ГРАФИК 4
            let k1 = -0.0039899 * deltaISA + 2.0106061
            let b1 = -0.3222222 * deltaISA + 171.666667
            let y4 = k1 * y3 + b1
            NSLog("7.72 график 4 результат = \(y4)")
            logInstance.title += ["7.72/4"]
            logInstance.detail += ["\(y4)"]
            
            //КОРРЕКЦИЯ, МАЛЕНЬКИЙ ГРАФИК
            if y4 >= 150 && y4 <= 310
            {
                let m = -1.9064336e-4 * y4 + 1.0219111
                self.BrakeSpeed = m * y4
                NSLog("7.72 MaxBrakeSpeed = \(self.BrakeSpeed)")
                logInstance.title += ["7.72/5 Brakes speed"]
                logInstance.detail += ["\(self.BrakeSpeed)"]
            }
            else
            {
                logInstance.warningTitle += ["7.72/5 Brake speed out of graph, double check"]
                logInstance.warningDetail += [" "]
            }
        }
        else 
		{
            NSLog("Nomogramm 7.72 input parameters out of range")
			logInstance.title += ["7.72 Inputs out of range"]
            logInstance.detail += [""]
        }
        //К О Н Е Ц   Н О М О Г Р А М М Ы  7.72
        
        //** Н О М О Г Р А М М А 7.70 ГРАДИЕНТ УХОДА НА 4 ДВИГАТЕЛЯХ ОТ МАССЫ (Слева направо)**
        //Столбец 1 Высота температура: уже посчитано выше, переменная vGrad770_1
               
        if deltaISA > 30
        {
            logInstance.warningTitle += ["7.70/1 Elevation and Temperature, double check"]
            logInstance.warningDetail += [" "]
        }
        
        logInstance.title += ["7.70/1"]
        logInstance.detail += ["\(vGrad770_1)"]
        
        //Столбец 2 Зависимомть от массы
        var vGrad770_21 :Double = 0
        if let n770021 = NomogramRetriever.shared.getNom2D (770021)
		{
            NSLog ("7.70 LR: 770021 loaded: Номограмма 7.70 (слева направо) столбец 2, Градиент ухода на 4 двигателях")
            if let x = n770021.getFByValues(self.AbsoluteLDW, x2: vGrad770_1)
            {
                vGrad770_21 = x
                if vGrad770_21 < 0 || vGrad770_21 > 24
				{
                    NSLog("7.70 LR Max go around Gradient on 4 engines column 2 out of range [0-24]")
				}
                NSLog("7.70 LR Max go around Gradient on 4 engines column 2= \(vGrad770_21)")
                logInstance.title += ["7.70/2"]
                logInstance.detail += ["\(vGrad770_21)"]
            }
            else
            {
                NSLog("7.70 LR column 2 error")
            }
        }
        else 
		{ 
			NSLog("7.70 LR: File 770021 not found") 
		}
        
        //Столбец 3 Ветер
        
        if let n770031 = NomogramRetriever.shared.getNom2D (770031)
		{
            NSLog ("7.70 LR: 770032 loaded: Номограмма 7.70 LR столбец 3, Градиент ухода на 4 двигателях")
            if let x = n770031.getFByValues(self.Headwind, x2: vGrad770_21)
            {
                self.Gradient4engines = x
                if self.Gradient4engines < 0 || self.Gradient4engines > 24
				{
                    NSLog("7.70 LR Max go around Gradient on 4 engines column 3 out of range [0-24]")
				}
                NSLog("7.70 LR Max go around Gradient on 4 engines column 3= \(self.Gradient4engines)")
                logInstance.title += ["7.70/3 Gradient 4 eng"]
                logInstance.detail += ["\(self.Gradient4engines)"]
            }
            else
            {
                NSLog("7.70 LR column 3 error")
            }
        }
        else 
		{ 
			NSLog("7.70 LR: File 770031 not found") 
		}
        // ** К О Н Е Ц   Н О М О Г Р А М М Ы  7.70 Left to Right **
        
        
        //** Н О М О Г Р А М М А 7.71 ГРАДИЕНТ УХОДА НА 3 ДВИГАТЕЛЯХ ОТ МАССЫ (Слева направо)**
        //Столбец 1 Высота температура: уже посчитано выше, переменная vGrad771_1
        
        
        if deltaISA > 30
        {
            logInstance.warningTitle += ["7.71/1 Elevation and Temperature, double check"]
            logInstance.warningDetail += [" "]
        }
        
        NSLog("7.71 \(vGrad771_1)")
        
        logInstance.title += ["7.71/1"]
        logInstance.detail += ["\(vGrad771_1)"]

        //Столбец 2 Зависимоcть от массы
        var vGrad771_21 :Double = 0
        if let n771021 = NomogramRetriever.shared.getNom2D (771021)
		{
            NSLog ("7.71 LR: 771031 loaded: Номограмма 7.71 (слева направо) столбец 2, Градиент ухода на 3 двигателях")
            if let x = n771021.getFByValues(self.AbsoluteLDW, x2: vGrad771_1)
            {
                vGrad771_21 = x
                if vGrad771_21 < 0 || vGrad771_21 > 20
				{
                    NSLog("7.71 LR Max go around Gradient on 3 engines column 2 out of range [0-20]")
				}
                NSLog("7.71 LR Max go around Gradient on 3 engines column 2= \(vGrad771_21)")
                logInstance.title += ["7.71/2"]
                logInstance.detail += ["\(vGrad771_21)"]
			}
            else
            {
                NSLog("7.71 LR column 2 error")
            }
        }
        else 
		{ 
			NSLog("7.71 LR: File 771031 not found") 
		}
        
        //Столбец 3 Ветер
        
        if let n771031 = NomogramRetriever.shared.getNom2D (771031)
		{
            NSLog ("7.71 LR: 771031 loaded: Номограмма 7.71 LR столбец 3, Градиент ухода на 3 двигателях")
            if let x = n771031.getFByValues(self.Headwind, x2: vGrad771_21)
            {
                self.Gradient3engines = x
                if self.Gradient3engines < 0 || self.Gradient3engines > 20
				{
                    NSLog("7.71 LR Max go around Gradient on 3 engines column 3 out of range [0-20]")
				}
                NSLog("7.71 LR Max go around Gradient on 3 engines column 3= \(self.Gradient3engines)")
                logInstance.title += ["7.71/3 Gradient 3 engines"]
                logInstance.detail += ["\(self.Gradient3engines)"]
            }
            else
            {
                NSLog("7.71 LR column 3 error")
            }
        }
        else 
		{ 
			NSLog("7.71 LR: File 771031 not found") 
		}
        // ** К О Н Е Ц   Н О М О Г Р А М М Ы  7.71 Left to Right **
        
        // ** Н О М О Г Р А М М А  7.74  С К О Р О С Т Ь   В К Л  А В Т О Т О Р М О Ж Е Н И Я  **
        var top, bottom: Double
        var v774_1: Double = 0
        switch self.AltitudeForCalculation{
        case 0...1000:      
		top = 0.0030357 * pow (self.AOT,2) - 0.94 * self.AOT + 275.1714286            //0
        bottom = 0.0022455 * pow (self.AOT,2) - 0.9038036 * self.AOT + 246.2342857    //1000
        v774_1 = top + ((bottom - top) / 1000) * (self.AltitudeForCalculation - 0)
        NSLog("7.74 left chart speed = \(v774_1)")
        case 1001...2000:   
		top = 0.0022455 * pow (self.AOT,2) - 0.9038036 * self.AOT + 246.2342857       //1000
        bottom = 0.0027731 * pow (self.AOT,2) - 0.8256677 * self.AOT + 217.3615105    //2000
        v774_1 = top + ((bottom - top) / 1000) * (self.AltitudeForCalculation - 1000)
        NSLog("7.74 left chart speed = \(v774_1)")
        case 2001...3000:   
		top = 0.0027731 * pow (self.AOT,2) - 0.8256677 * self.AOT + 217.3615105       //2000
        bottom = 0.0026786 * pow (self.AOT,2) - 0.7778571 * self.AOT + 189.6285714    //3000
        v774_1 = top + ((bottom - top) / 1000) * (self.AltitudeForCalculation - 2000)
        NSLog("7.74 left chart speed = \(v774_1)")
        case 3001...4000:   
		top = 0.0026786 * pow (self.AOT,2) - 0.7778571 * self.AOT + 189.6285714       //3000
        bottom = 0.0022193 * pow (self.AOT,2) - 0.7538396 * self.AOT + 162.9595451    //4000
        v774_1 = top + ((bottom - top) / 1000) * (self.AltitudeForCalculation - 3000)
        NSLog("7.74 left chart speed = \(v774_1)")
        default: NSLog("7.74 AltitudeForCalculation out of range [0-4000]")
        }
		
		logInstance.title += ["7.74/1"]
        logInstance.detail += ["\(v774_1)"]
        
        
        if deltaISA > 30
        {
            logInstance.warningTitle += ["7.74/1 Elevation and Temperature, double check"]
            logInstance.warningDetail += [" "]
        }
      
        switch self.Headwind
		{
        case let x where x < 0 && x > -15:
            let k = 2.0456277e-8 * pow(v774_1,4) - 3.1680897e-5 * pow(v774_1,3) + 0.0175814 * v774_1 * v774_1 - 4.2166799 * v774_1 + 385.7507931
            self.AutobrakeLDW = k * self.Headwind + v774_1
            NSLog("7.74 Max Autobrake LDW = \(self.AutobrakeLDW)")
        case let x where x >= 0 && x < 30:
            let k = 6.66674e-6 * pow(v774_1,2) + 0.0056666 * v774_1 + 1.8333355
            self.AutobrakeLDW = k * self.Headwind + v774_1
            NSLog("7.74 Max Autobrake LDW = \(self.AutobrakeLDW)")
        default: NSLog("7.74 wind calculation error")
        }
       
        logInstance.title += ["7.74 Max Autobrake LDW"]
        logInstance.detail += ["\(self.AutobrakeLDW)"]

        // **  К О Н Е Ц   Н О М О Г Р А М М Ы  7.74  С К О Р О С Т Ь   В К Л  А В Т О Т О Р М О Ж Е Н И Я  **
        
        // **  Н О М О Г Р А М М А    7.68    L E F T   T O  R I G H T  **
        //График 1 Температура и превышение
        //Уже посчитано, переменная vLDA12
        
        logInstance.title += ["7.68/1"]
        logInstance.detail += ["\(vLDA12)"]
        
        // Столбец 2 Зависимость от массы
        var v768LR2 :Double = 0
        if let n768021 = NomogramRetriever.shared.getNom2D(768021)
		{
            NSLog("7.68 LR: 768021 loaded")
            if let x = n768021.getFByValues(self.AbsoluteLDW, x2: vLDA12)
            {
                v768LR2 = x
                if v768LR2 < 400 || v768LR2 > 3600 
				{
                    NSLog("7.68 LR: column 2  LD out of range [400-3600]")
				}
                NSLog("7.68 LR: column 2 LD=\(v768LR2)")
                logInstance.title += ["7.68/2"]
                logInstance.detail += ["\(v768LR2)"]
            }
            else
            {
                NSLog("7.68 LR: column 2 error")
            }
        }
        else 
		{ 
			NSLog("7.68 LR: File 768021 not found") 
		}
        
        // Столбец 3 Ветер
        var v768LR3 :Double = 0
        if let n768031 = NomogramRetriever.shared.getNom2D(768031)
		{
            NSLog("7.68 LR: 768031 loaded")
            if self.Headwind < -5 || self.Headwind > 30 
			{
                NSLog("WIND out of range [-5:30]") 
			}
            if let x = n768031.getFByValues(self.Headwind, x2: v768LR2)
            {
                v768LR3 = x
                if v768LR3 > 3600 || vLDA32 < 400 
				{
                    NSLog("7.68 LR: column 3 LD out of range [400-3600]" )
				}
                NSLog("7.68 LR: column 3 LD=\(v768LR3)")
                logInstance.title += ["7.68/3"]
                logInstance.detail += ["\(v768LR3)"]
            }
            else
            {
                NSLog("7.68 LR: column 3 error")
            }
        }
        else 
		{ 
			NSLog("7.68 LR: File 768031 not found") 
		}
        
        //График 4 Уклон по формуле
        //В форме ввода написать, что уклон вверх это плюс, вниз это минус
        //В данном графике 7.68 LR для + уменьшаем, для - увеличиваем [в обратном 7.68 наоборот], в каждом графике смотреть знак!
        //переменная посчитана в предыдущем 7.68 (RL), dSlope
       
        
        if self.Slope < 0
        {
            self.Dland = v768LR3 + dSlope
        }
        else if self.Slope >= 0
        {
            self.Dland = v768LR3 - dSlope
        }
        self.Dlandreq = self.Dland * self.AirportType //ПД для сухой основного ад
       
        logInstance.title += ["7.68/4 D land dry"]
        logInstance.detail += ["\(self.Dland)"]
        
        self.Dlandreqwet = self.Dlandreq * Kmu
        
        logInstance.title += ["7.68/4 D land wet"]
        logInstance.detail += ["\(self.Dlandreqwet)"]
        
		self.Dland = self.Dland * Kmu
        
        logInstance.title += ["7.68/4 D land"]
        logInstance.detail += ["\(self.Dland)"]
        
        // **  К О Н Е Ц   Н О М О Г Р А М М Ы    7.6.8    L E F T   T O  R I G H T  **
        
        // **  С К О Р О С Т И   Н А   П О С А Д К Е  **
        
        self.TouchdownSpeed = self.AbsoluteLDW * self.AbsoluteLDW
        self.TouchdownSpeed = -2.6194639e-4 * self.TouchdownSpeed
        self.TouchdownSpeed = self.TouchdownSpeed + 0.5932133 * self.AbsoluteLDW + 94.695338
        
        logInstance.title += ["TouchDown Speed"]
        logInstance.detail += ["\(self.TouchdownSpeed)"]
        
        self.ApproachSpeed = self.AbsoluteLDW * self.AbsoluteLDW
        self.ApproachSpeed = -2.3310023e-4 * self.ApproachSpeed
        self.ApproachSpeed = self.ApproachSpeed + 0.5784965 * self.AbsoluteLDW + 118.8170163
       
        logInstance.title += ["Approach Speed"]
        logInstance.detail += ["\(self.ApproachSpeed)"]
        // **  К О Н Е Ц   С К О Р О С Т Е Й   Н А   П О С А Д К Е  **
        
        // **  Н О М О Г Р А М М А  7.75  М А С С А  Д Л Я  У Х О Д А  Н А  2  Д В И Г А Т Е Л Я Х **
        // Левая часть
        var v775_1 :Double = 0
        
        if self.AOT > -29 && deltaISA <= 30 
		{   //otherwise out of nomogramm
            if let n77501 = NomogramRetriever.shared.getNom2D(77501)
			{
                NSLog("7.75: 77501 loaded")
                if let x = n77501.getFByValues(floor(deltaISA), x2: self.AOT)
                {
                    v775_1 = x
                    if v775_1 > 381 || v775_1 < 255 
					{
                        NSLog("7.75: column 1 G out of range [255-381]" )
					}
                    NSLog("7.75: column 1 G=\(v775_1)")
                    logInstance.title += ["7.75/1"]
                    logInstance.detail += ["\(v775_1)"]
                }
                else
                {
                    NSLog("7.75: column 1 error")
				}
            }
            //Правая часть
            var k775 : Double = 0
            
            switch self.Headwind
			{
            case let x where x > 0 && x < 30:
                k775 = 0.0011078 * v775_1 - 0.0608287
                self.LDWmaxMAP2 = v775_1 + k775 * self.Headwind
                NSLog("7.75: Gmax 2 engines = \(self.LDWmaxMAP2)")
                logInstance.title += ["7.75/2 LDW max 2 eng"]
                logInstance.detail += ["\(self.LDWmaxMAP2)"]
            case let x where x <= 0 && x > -15:
                k775 = -0.0033263 * v775_1 + 0.1690904
                self.LDWmaxMAP2 = v775_1 - k775 * self.Headwind
                NSLog("7.75: Gmax 2 engines = \(self.LDWmaxMAP2)")
                logInstance.title += ["7.75/2 LDW max 2 eng"]
                logInstance.detail += ["\(self.LDWmaxMAP2)"]
            default: NSLog("7.75: Wind out of range")
            	logInstance.title += ["7.75/2 Headwind out of range (-15:+30)"]
				logInstance.detail += [" "]
            }
        }
        else // if AOT < -29 or ISA Temp deviation >30
        {
            self.LDWmaxMAP2 = 0 // if pTemp out of envelope
            logInstance.title += ["7.75/1 Temp and Elevation out of graph, double check"]
            logInstance.detail += [" "]
            logInstance.warningTitle += ["7.75/1 Temp and Elevation out of graph, double check"]
            logInstance.warningDetail += [" "]
        }
        
        
        
        // **  К О Н Е Ц  Н О М О Г Р А М М Ы  7.75  М А С С А  Д Л Я  У Х О Д А  Н А  2  Д В И Г А Т Е Л Я Х **
        
        // **  Н О М О Г Р А М М А  7.76  В П Р  Д Л Я  У Х О Д А  Н А  2  Д В И Г А Т Е Л Я Х **
        // Левая часть
        var v776_1 :Double = 0
     
        if (self.AOT > -10 && self.AOT <= 45 && deltaISA >= 0 && deltaISA <= 30) 
		{ //otherwise out of 776
            if let n77601 = NomogramRetriever.shared.getNom2D(77601)
			{
                NSLog("7.76: 77601 loaded")
                if let x = n77601.getFByValues(self.AOT, x2: self.AltitudeForCalculation)
                {
                    v776_1 = x
                    NSLog("7.76: column 1 H=\(v776_1)")
					logInstance.title += ["7.76/1"]
                    logInstance.detail += ["\(v776_1)"]
                }
                else
                {
                    NSLog("7.76: column 1 error")
                }
            }
            
            // Правая часть
            if self.LDWmaxMAP2 < 360 && self.LDWmaxMAP2 > 240 
			{
                if let n77602 = NomogramRetriever.shared.getNom2D(77602)
				{
                    NSLog("7.76: 77602 loaded")
                    if let x = n77602.getFByValues(self.LDWmaxMAP2, x2: v776_1)
                    {
                        self.DescisionHeight2 = x
                        if self.DescisionHeight2 > 700 || self.DescisionHeight2 < 150 
						{
                            NSLog("7.76: column 2 H out of range [150-700]" )
						}
                        NSLog("7.76: column 2 DH 2 engines=\(self.DescisionHeight2)")
                        logInstance.title += ["7.76/2 Descision height 2 engines"]
                        logInstance.detail += ["\(self.DescisionHeight2)"]

                    }
                        
                    else
                    {
                        NSLog("7.76: column 2 error")
                    }
                }
            }
            else
            {
                self.DescisionHeight2 = 0
				logInstance.title += ["7.76/2, LDW 2 engines out of range (240:360)"]
                logInstance.detail += [" "]
                //LogItem.warningTitle += ["7.76/2, LDW 2 engines out of range (240:360)"]
                //LogItem.warningDetail += [" "]
            }
        }
        else
        {
            self.DescisionHeight2 = 0
            logInstance.title += ["7.76/1 Temp and Elevation out of range, check"]
            logInstance.detail += [" "]
            //LogItem.warningTitle += ["7.76/1 Temp and Elevation out of range, check"]
            //LogItem.warningDetail += [" "]
        } 	//if Temp in envelope
			// **  К О Н Е Ц   Н О М О Г Р А М М Ы  7.76  В П Р  Д Л Я  У Х О Д А  Н А  2  Д В И Г А Т Е Л Я Х **
        
    }
    //  К О Н Е Ц   Р А С Ч Е Т А   П О С А Д К И
    

}
