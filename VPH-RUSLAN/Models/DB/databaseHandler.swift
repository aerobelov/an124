//
//  dbData.swift
//  IL76
//
//  Created by Pavel on 17/09/2016.
//  Copyright © 2016 Pavel. All rights reserved.
//

import Foundation
import SystemConfiguration
import FMDB

class databaseHandler: ReloadDatabaseProtocol,
                       ExecuteQueryProtocol,
                       DatabaseDataStatusProtocol {
   
    
    static var shared = databaseHandler()
    var icaoCode:[String] = []
    var airportName:[String] = []
    var airac: Int
    var revision: Int
    var stringStart: String?
    var startDate: String
    var stringEnd: String?
    var endDate: String
    var databaseFileURL: URL!
    var db: FMDatabase?
    
    
    var end: Date {
        let doubleDate: Double = Double(stringEnd!)!
        let date: Date = Date(timeIntervalSince1970: doubleDate)
        return date
    }
    
    var controlNumber: Int? {
        guard let x = Int(String(airac) + String(revision)) else {
            return nil
        }
        return x
    }
    
    func convertToDate(_ stringDate: String) -> String {
        let doubleDate: Double = Double(stringDate)!
        let date: Date = Date(timeIntervalSince1970: doubleDate)
        let displayDate = (String(describing: date) as NSString).substring(to: 10)
        return displayDate
    }
    
    func executeQuery(query: String) -> FMResultSet {
        return (db?.executeQuery(query, withArgumentsIn: []))!
    }
    
    func makeWritableCopy (named destFileName: String, ofResourceFile originalFileName: String) throws -> URL {
        
        //Get documentsdirectory in app bundle
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
            else {
                fatalError("No documents directory found in application bundle")
        }
        //Get future URL for destination file (in documents directory)
        let writableFileURL = documentsDirectory.appendingPathComponent(destFileName)
        
        //If dest file does not exist yet
        if (try? writableFileURL.checkResourceIsReachable()) == nil {
            guard let originalFileURL = Bundle.main.url(forResource: originalFileName, withExtension: nil) else {
                fatalError("Cannot find original file \(originalFileName) in application bundle")
            }
            
            if ((try? FileManager.default.copyItem(at: originalFileURL, to: writableFileURL)) != nil) {
              print ("Made a writable copy of file \(originalFileName) in \(writableFileURL)")
            }
        }
        
        return writableFileURL
    
    }
    
    
    
    func  reload () {
        
        //Getting database URL
        do {
        databaseFileURL = try makeWritableCopy(named: "vph.sqlite", ofResourceFile: "vph.sqlite")
        } catch let error as NSError {
            print ("ERROR MAKEWRITABLECOPY \(error.localizedDescription)")
        }
        
        if FileManager.default.fileExists(atPath: databaseFileURL.path) {
            
            db = FMDatabase(path: databaseFileURL.path)
            print ("DATABASE EXISTS")
            
            icaoCode = []
            airportName = []
            
            
            if (db?.open() != nil) {
                
                NSLog("DATABASE OPENED")
                let reqADQuery = "SELECT DISTINCT apt_dir.icao, apt_dir.apt_name, rw_thres.ICAO FROM apt_dir INNER JOIN rw_thres ON apt_dir.icao = rw_thres.ICAO WHERE apt_dir.icao is not '' ORDER BY apt_dir.icao ASC"
                let requestDatabaseInfoQuery = "SELECT airac, start_date, end_date, revision FROM info"
                let rsAD: FMResultSet? = db!.executeQuery(reqADQuery, withArgumentsIn: [])
                let rsInfo: FMResultSet? = db!.executeQuery(requestDatabaseInfoQuery, withArgumentsIn: [])
                while (rsAD!.next() == true) {
                    icaoCode.append(rsAD!.string(forColumn: "icao")!)
                    airportName.append(rsAD!.string(forColumn: "apt_name")!)
                }
                while (rsInfo!.next() == true) {
//---------------------USE BINDING OPTIONALS
                    airac = Int(rsInfo!.string(forColumn: "airac")!)!
                    stringStart = rsInfo!.string(forColumn: "start_date")
                    stringEnd = rsInfo!.string(forColumn: "end_date")
                    revision = Int(rsInfo!.string(forColumn: "revision")!)!
                    if let x = stringStart {
                        startDate = convertToDate(x)
                    }
                    if let y = stringEnd {
                        endDate = convertToDate(y)
                    }
                    vComparer.shared.oldIndex = Int(String(airac) + String(revision))
                }
                print ("DATABASE RELOADED", self.airac, self.revision)
            }
            
        } else {
            print ("DATABASE NOT FOUND")
        }
    }
    
    private init () {
        icaoCode = []
        airportName = []
        airac = 0000
        revision = 0
        stringStart = nil
        stringEnd = nil
        startDate = ""
        endDate = ""
        reload()
        
    }
}
