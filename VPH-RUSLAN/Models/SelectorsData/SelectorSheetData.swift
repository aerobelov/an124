//
//  InValueButton.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 10.06.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation

struct SelectorSheetData: SelectSheetDataProtocol {
    
    var value: Double
    var title: String
    var formatterConfigurator: FormatterConfigurator
    var maximum: Double
    var minimum: Double
    var values: [String]
    var step: Double
    
    var indicatedValue: String {
        return self.formatterConfigurator.formatter.string(from: NSNumber(value: self.value)) ?? ""
    }
    
    init(value: Double, maximum: Double, minimum: Double, title: String, step: Double, formatterType: formatterType) {
        self.value = value
        self.title = title
        self.formatterConfigurator = FormatterConfigurator(type: formatterType)
        self.minimum = minimum
        self.maximum = maximum
        self.step = step
        self.values = []
    }
    
    func format(_ value: Double) -> String {
        return self.formatterConfigurator.formatter.string(from: NSNumber(value: value)) ?? "Err"
    }
    
}





