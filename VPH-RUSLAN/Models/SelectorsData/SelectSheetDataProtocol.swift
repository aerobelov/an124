//
//  SelectSheetDataProtocol.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 19.09.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation

protocol SelectSheetDataProtocol {
    var value: Double {get set}
    var title: String { get set }
    var indicatedValue: String {get}
    var formatterConfigurator: FormatterConfigurator {get set}
    var values: [String] { get set }
    var maximum: Double { get set }
    var minimum: Double { get set }
    var step: Double {get set}
    func format(_ value: Double) -> String
}

