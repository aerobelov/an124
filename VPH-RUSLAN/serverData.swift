//
//  serverData.swift
//  IL76
//
//  Created by Pavel Belov on 16.11.16.
//  Copyright © 2016 Pavel. All rights reserved.
//

import Foundation


class jsonChecker : NSObject, URLSessionDelegate {
    
    //Constants
    //let fileName = "versions.json"
    let url = URL(string: "http://cao.vda.ru/navdb/efbvph/versions.json") //"http://opd.vda.ru/vph/versions.json")//
    let pswString = "vdaefb:h98VE4nBQ3u"
    
    //Session variables
    let fileManager = FileManager.default
    var localJsonURL: URL!
    var jsonSession: URLSession!
    let sessionConfig = URLSessionConfiguration.default
    var jsonTask: URLSessionTask!
    let pswData: Data
    let base64encodedData: String
    var authString:String
    var gotResponse: Bool = false
    
    //Received data
    var newAirac: Int = 0
    var newRevision: Int = 0
    var newUrl: String = ""
    var remoteControlNumber: Int?
    
    //Delegate variables
    var proggressValue: Float = 0
    var delegate: DownloadCompleteProtocol!
    
    override init() {
        
        pswData = pswString.data(using: .utf8)!
        base64encodedData = pswData.base64EncodedString()
        authString = "Basic \(base64encodedData)"
        sessionConfig.httpAdditionalHeaders = ["Authorization": authString]
        sessionConfig.timeoutIntervalForResource = 60
        sessionConfig.urlCache = nil
    }
    
    
    func getJson () {
        
        jsonSession = Foundation.URLSession(configuration: sessionConfig, delegate: self,  delegateQueue: OperationQueue.main)
        jsonTask = jsonSession.dataTask(with: url!) {
            
            ( data, response, error) in
            if let httpResponse = response as? HTTPURLResponse {
                
                //PRINT SERVER RESPONSE FOR CHECK
                let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                //print("RESPONSE= \(dataString)")
                
                //PARCING DATA
                do {
                if let data = data {
                    if let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments) as? [String:Any] {
                        if let array = json["versions"] as? [[String:String]] {
                            for items in array {
                                if let jairac = items["airac"] {
                                    if let x = Int(jairac) {
                                        self.newAirac = x
                                        print("NEWAIRAC \(self.newAirac)")
                                    }
                                }
                                if let jrev = items["revision"] {
                                    if let x = Int(jrev) {
                                        self.newRevision = x
                                        print("REVISION \(self.newRevision)")
                                    }
                                }
                                if let addr = items["url"] {
                                    self.newUrl = addr
                                    print("NEWURL \(self.newUrl)")
                                }
                            }
                            self.remoteControlNumber = Int(String(self.newAirac) + String(self.newRevision))
                            print("CONTROL NUMBER just calc =\(self.remoteControlNumber ?? 0)")
                            self.gotResponse = true
                            self.sendNumber()
                        }
                    }
                }
                } catch let error as NSError {
                    print ("ERROR PARCING DATA \(error)")
                }

                
            } else {
                print ("NO RESPONSE RECEIVED")
            }
            
        }                                                                   //json tsk
        jsonTask.resume()
        
    }                                                                       //get json func
    
    //SEND CONTROL NUMBER TO UPDATE VIEW CONTROLLER
    func sendNumber () {
        if self.remoteControlNumber != nil {
            delegate.compareVersions(cNumber: self.remoteControlNumber!)
            jsonSession.invalidateAndCancel()
        }
    }
    
}


//    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask,  didWriteData bytesWritten: Int64, totalBytesWritten: Int64,
//                    totalBytesExpectedToWrite: Int64)
//    {
//        proggressValue = Float(totalBytesWritten)/Float(totalBytesExpectedToWrite)
//        delegate.settingProgress(value: proggressValue)
//    }
    
    
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if (error != nil) {
            print(error)
        } else {
          //  delegate.downloadCompletedFunction()
            print("The task finished transferring data successfully")
        }
    }
    
