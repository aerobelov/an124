//
//  formatterFabric.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 16.11.2021.
//  Copyright © 2021 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation

enum formatterType {
    case one, two, distance, weight, int, zero
}

class FormatterConfigurator {
    
    var formatter = NumberFormatter()
    
    init(type: formatterType) {
        switch type {
            case .zero:
                formatter.allowsFloats = false
                formatter.minimumIntegerDigits = 1
                formatter.numberStyle = .decimal
            case .one:
                formatter.allowsFloats = true
                formatter.maximumFractionDigits = 1
                formatter.minimumFractionDigits = 1
                formatter.minimumIntegerDigits = 1
                formatter.decimalSeparator = "."
                formatter.alwaysShowsDecimalSeparator = false
                formatter.numberStyle = .decimal
            case .two:
                formatter.allowsFloats = true
                formatter.roundingMode = .up
                formatter.maximumFractionDigits = 2
                formatter.minimumFractionDigits = 0
                formatter.minimumIntegerDigits = 1
                formatter.decimalSeparator = "."
                formatter.alwaysShowsDecimalSeparator = false
                formatter.numberStyle = .decimal
            case .weight:
                formatter.allowsFloats = true
                formatter.maximumFractionDigits = 1
                formatter.minimumFractionDigits = 1
                formatter.minimumIntegerDigits = 1
                formatter.roundingMode = .down
                formatter.decimalSeparator = "."
                formatter.alwaysShowsDecimalSeparator = false
            case .distance:
                formatter.allowsFloats = false
                formatter.maximumFractionDigits = 0
                formatter.roundingMode = .down
                formatter.usesGroupingSeparator = false
                formatter.groupingSize = 3
            case .int:
                formatter.allowsFloats = false
                formatter.maximumFractionDigits = 0
                formatter.roundingMode = .down
                formatter.usesGroupingSeparator = false
                formatter.groupingSize = 3
        }
    }
}
