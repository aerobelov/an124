//
//  ExpirationChecker.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 24.02.2020.
//  Copyright © 2020 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation

class BadgeController {
    
    //let mainDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    enum status {
        case update
        case expired
        case empty
    }
    static var badge: String?
    static var currentState: status?
    
    static func show(state: status) {
        self.currentState = state
        switch state {
            case .update: self.badge = "Update"
            case .expired: self.badge = "Expired"
            case .empty: self.badge = nil
        }
        let mainDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let tabBarController = mainDelegate.window?.rootViewController as! UITabBarController
        tabBarController.tabBar.items?[4].badgeValue = self.badge
    }
}

class ExpirationChecker: NSObject {
    static var shared = ExpirationChecker()
    var airac: String?
    var startDate: String?
    var endDate: String?
    var revision: String?
    var status: String?
    var badge: String?
    var delegate: CanDisplayDatabaseStatus?
    
    override private init() {
        super.init()
        self.checkDatabaseExpired()
    }
    
    //Compare current date with database loaded
    let now = Date()
    var dbObject = databaseHandler.shared
    
    func checkDatabaseExpired () {
        
        // Check if DB is not open
    if let object = dbObject.db {
        if  !object.open() {
           print("error opening db")
        }
           
        else { //If opened
               airac = String(dbObject.airac)
               startDate = dbObject.startDate
               endDate = dbObject.endDate
               revision = String(dbObject.revision)
               
               let order = (Calendar.current as NSCalendar).compare(now, to: databaseHandler.shared.end as Date,
                                                                    toUnitGranularity: .day)
               //Comparing calendar
               switch order {
                   case .orderedDescending, .orderedSame:
                        self.status = "Database is expired"
                        BadgeController.show(state: .expired)
                   case .orderedAscending:
                    if let upd = vComparer.shared.updates {
                        if !upd {
                            self.status = "Database is up to date"
                            if let state = BadgeController.currentState {
                                BadgeController.show(state: state)
                            }
                        }
                        
                    }
               }
            
                if let updateViewController = delegate {
                    if status != nil {
                         updateViewController.statusLabel.text = status!
                    }
                    if airac != nil {
                         updateViewController.airac.text = airac!
                    }
                    if startDate != nil {
                         updateViewController.startDate.text = startDate!
                    }
                    if endDate != nil {
                         updateViewController.endDate.text = endDate!
                    }
                    if revision != nil {
                         updateViewController.revision.text = revision!
                    }
                        
                }
               
           }
        }
        
    }
    
   
}
