//
//  UpdatesChecker.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 22.02.2020.
//  Copyright © 2020 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation

class UpdatesChecker: NSObject {
    static let shared = UpdatesChecker()
    var delegate: ReloadDatabaseProtocol = databaseHandler.shared
    var displayDelegate: CanDisplayUpdatesAvailable?
    var updater: Updater = JsonDowloader.sharedInstance
    var cn: Int?
    
    func initiateUpdatesCheck() {
        
        DispatchQueue.global(qos: .userInteractive).sync { [weak self] in
            guard let self = self else {
              return
            }
            self.updater.getJsonFromSao {
            if let cn = self.updater.remoteControlNumber {
                    if let old = databaseHandler.shared.controlNumber {
                        if vComparer.shared.compare(old: old, new: cn) {
                            DispatchQueue.main.async {
                                BadgeController.show(state: .update)
                            }
                        } else {
                            vComparer.shared.updates = false
                        }
                    }
                } else {
                }
            
            }
            self.delegate.reload()
        }
    }
    
    
}
