//
//  jsonDownloader.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 14.02.2020.
//  Copyright © 2020 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation

class JsonDowloader : NSObject, URLSessionDelegate, Updater {
    
    static let sharedInstance = JsonDowloader()
    let url = URL(string: "https://cao.vda.ru/navdb/efbvph/versions.json")
    let pswString = "vdaefb:h98VE4nBQ3u"
    let sessionConfig = URLSessionConfiguration.default
    let authString: String
    let base64encodedData: String
    let pswData: Data
    var jsonSession: URLSession!
    var jsonTask: URLSessionTask!
    var verifyDelegate: versionCompareProtocol = vComparer.shared
    
    //Received data
    var newAirac: Int = 0
    var newRevision: Int = 0
    var newUrl: URL?
    var remoteControlNumber: Int?
    
    //Delegate variables
    var proggressValue: Float = 0
    var delegate: DownloadCompleteProtocol!
    
    struct versionJson: Decodable {
        struct DatabaseInfo: Decodable {
           var airac: String?
           var revision: String?
           var url: String?
        }
        var versions: [DatabaseInfo]
    }
    
    override private init() {
        pswData = pswString.data(using: .utf8)!
        base64encodedData = pswData.base64EncodedString()
        authString = "Basic \(base64encodedData)"
        sessionConfig.httpAdditionalHeaders = ["Authorization": authString]
        sessionConfig.timeoutIntervalForResource = 60
        sessionConfig.urlCache = nil
    }
   
    
    func getJsonFromSao (completion: @escaping ()->Void )  {
        
        let jsonSession = URLSession(configuration: sessionConfig)
        if currentReachabilityStatus != .notReachable {
            jsonSession.dataTask(with: url!) { (data, response, error) in
                guard let data = data else { return }
                do {
                    let decoder = JSONDecoder()
                    let jsonData = try decoder.decode(versionJson.self, from: data)
                    print ("JSON \(jsonData)")
                    if let airac = jsonData.versions[0].airac {
                        if let revision = jsonData.versions[0].revision {
                            self.newRevision = Int(revision)!
                            self.remoteControlNumber = Int(airac+revision)
                            self.verifyDelegate.newIndex = self.remoteControlNumber
                            if let jurl = jsonData.versions[0].url {
                                self.newUrl = URL(string: jurl)
                            }
                            completion()
                        }
                    }
                }  catch  {}
            }.resume()
        }
    }
}
