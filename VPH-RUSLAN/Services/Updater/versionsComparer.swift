//
//  vComparer.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 16.02.2020.
//  Copyright © 2020 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation

class vComparer: NSObject, versionCompareProtocol {
    static let shared = vComparer()
    var oldIndex: Int?
    var newIndex: Int?
    var updates: Bool?
    
    private override init() {
       
    }
    
    func compare(old: Int, new: Int) -> Bool {
        self.oldIndex = old
        self.newIndex = new
        self.updates = new>old
        return self.updates!
    }
}
