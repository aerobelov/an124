//
//  NomogramRetriever.swift
//  VPH-RUSLAN
//
//  Created by Pavel Belov on 26.02.2020.
//  Copyright © 2020 Volga-Dnepr Airlines. All rights reserved.
//

import Foundation

class NomogramRetriever {
    
    static var shared = NomogramRetriever()
    var noms2D = [Int : VDANomogram2D]()
    
    func getNom2D(_ pNomId:Int) -> VDANomogram2D? {
           if let n = noms2D[pNomId]{
               return n
           } else {
               let n = VDANomogram2D(filename: "n" + pNomId.description)
               guard n.nomogram.isEmpty else {
                    noms2D.updateValue(n, forKey: pNomId)
                    return n
               }
               return nil
           }
           
    }
}
