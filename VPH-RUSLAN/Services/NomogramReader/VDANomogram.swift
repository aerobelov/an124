//
//  VDANomogram.swift
//  MyFileReader
//
//  Created by Admin on 24.07.14.
//  Copyright (c) 2014 Volga-Dnepr Airline. All rights reserved.
//

import Foundation

class VDANomogram2D: NSObject {
  
    
    var nomogram: Array<(x1:Double, x2:Double, f:Double)>
    var deltaX: (x1: Double, x2: Double)
    var minX: (x1:Double, x2:Double)
    var maxX: (x1:Double, x2:Double)
    let cnstZero = 10E-10
    let cnstHalfZero = 5E-10
    //let path: URL?
    
    //----------------------------------------------------------------------------------------------
    
    init (filename:String) {
       
      let bundle = Bundle.main
      var x11:Double? = nil
      var x21:Double? = nil
      var x12:Double? = nil
      var x22:Double? = nil
      var x13:Double? = nil
      var x23:Double? = nil
        
      //Initialization
      nomogram = []
      deltaX = (0,0)
      minX = (0,0)
      maxX = (0,100000) // second value is reserved for future
      
      if let path = bundle.url(forResource: filename, withExtension: "txt") {
          if let nomFile = try? String(contentsOf: path, encoding: .utf8) {
            let nomStrings = nomFile.components(separatedBy: "\n")
            for item in nomStrings {
                let nomItem = item.components(separatedBy: ";")
                if nomItem.count == 3 {
                   let x1 = (nomItem[0] as NSString).doubleValue
                   let x2 = (nomItem[1] as NSString).doubleValue
                   nomogram.append((x1:x1,x2:x2,f:(nomItem[2] as NSString).doubleValue))
                    if let x=x13 {
                        if x < x1 {
                            x13 = x1
                        }
                        
                    } else {
                        x13 = x1
                    }
                    if let x=x23 {
                        if x < x2 {
                            x23 = x2
                        }
                        
                    } else {
                        x23 = x2
                    }
                    if let x=x11 {
                        if x1 < x {
                            x12 = x11
                            x11 = x1
                        }
                        if x < x1 {
                            if let y=x12 {
                                if x1 < y {
                                  x12 = x1
                                }
                            }
                            else {
                              x12 = x1
                            }
                        }
                    } else {
                        x11 = x1
                    }
                    if let x=x21 {
                        if x2 < x {
                            x22 = x21
                            x21 = x2
                        }
                        if x < x2 {
                            if let y=x22 {
                                if x2 < y {
                                    x22 = x2
                                }
                            } else {
                                x22 = x2
                            }
                        }
                    } else {
                        x21 = x2
                    }
                
                }
                
                
            }
            
            deltaX = (x12!-x11!, x22!-x21!)
            //NSLog("Delta \(x12!-x11!) and \(x22!-x21!)")
            minX = (x11!, x21!)
            maxX = (x13!, x23!)
            
          } else {
            NSLog("File \(String(describing: path)) has not been found")
          }
        }
    }
    
    //----------------------------------------------------------------------------------------------
    
    func getFbyLineNum(_ lineNum: Int)->Double?{
        
      return nomogram[lineNum].2

    }
    
    //----------------------------------------------------------------------------------------------
    
    func getIndexByValues(_ x1:Double, x2:Double)->Int?{
      
        let t1  = floor(cnstHalfZero+(x1-minX.x1)/deltaX.x1)
        let t2  = floor(cnstHalfZero+(x2-minX.x2)/deltaX.x2)
        let d2   = floor(cnstHalfZero+(maxX.x2-minX.x2)/deltaX.x2+1)
        
        return Int(t1*d2 + t2)
                
    }
    
    //----------------------------------------------------------------------------------------------
    
    func getFByValues(_ x1:Double, x2:Double)->Double?{
        
        //return 100.0
        
        //NSLog("Log 7 26: \(x1) and \(minX.x1) and \(deltaX.x1) and \((x1-minX.x1)/deltaX.x1)")
        
        if abs(floor(cnstHalfZero+(x1-minX.x1)/deltaX.x1)-(x1-minX.x1)/deltaX.x1) < cnstZero {
            //NSLog("Log 7 26: \(x2) and \(minX.x2) and \(deltaX.x2) and \((x2-minX.x2)/deltaX.x2)")
            //let  r = abs(5E-10+floor((x2-minX.x2)/deltaX.x2)-(x2-minX.x2)/deltaX.x2)
            //NSLog("\(r)")
            if abs(floor(cnstHalfZero+(x2-minX.x2)/deltaX.x2)-(x2-minX.x2)/deltaX.x2) < cnstZero{
                
                let t1  = floor(cnstHalfZero+(x1-minX.x1)/deltaX.x1)
                let t2  = floor(cnstHalfZero+(x2-minX.x2)/deltaX.x2)
                let d2   = floor(cnstHalfZero+(maxX.x2-minX.x2)/deltaX.x2+1)
                
                //NSLog("Log 7 26: \(t1) and \(t2) and \(d2)")
                
                return nomogram[Int(t1*d2  + t2) ].2
                    
            }
            else{
                let t1  = floor(cnstHalfZero+(x1-minX.x1)/deltaX.x1)
                let t21 = floor(cnstHalfZero+(x2-minX.x2)/deltaX.x2)
                let t22 = ceil (-cnstHalfZero+(x2-minX.x2)/deltaX.x2)
                let d2   = floor(cnstHalfZero+(maxX.x2-minX.x2)/deltaX.x2+1)
                //let q2   = (x2-nomogram[Int(t1*d2+t21)].1)/(nomogram[Int(t1*d2+t22)].1-nomogram[Int(t1*d2+t21)].1)
                var q2   = (x2-nomogram[Int(t1*d2+t21)].1)//(nomogram[Int(t1*d2+t22)].1-nomogram[Int(t1*d2+t21)].1)
                q2 = q2 / (nomogram[Int(t1*d2+t22)].1 - nomogram[Int(t1*d2+t21)].1)
                
                return nomogram[Int(t1*d2  + t21) ].2 * (1-q2) +  nomogram[Int(t1*d2  + t22)].2  * q2
                
            }
            
        }
        else{
            if abs(floor(cnstHalfZero+(x2-minX.x2)/deltaX.x2)-(x2-minX.x2)/deltaX.x2) < cnstZero{
                
                let t11 = floor(cnstHalfZero+(x1-minX.x1)/deltaX.x1)
                let t12 = ceil (-cnstHalfZero+(x1-minX.x1)/deltaX.x1)
                let t2  = floor(cnstHalfZero+(x2-minX.x2)/deltaX.x2)
                let d2   = floor(cnstHalfZero+(maxX.x2-minX.x2)/deltaX.x2+1)
                var q1   = (x1 - nomogram[Int(t11*d2+t2)].0)//(nomogram[Int(t12*d2+t2)].1-nomogram[Int(t11*d2+t2)].1)
                q1 = q1 / (nomogram[Int(t12*d2+t2)].0 - nomogram[Int(t11*d2+t2)].0)
                
                return nomogram[Int(t11*d2  + t2) ].2 * (1-q1) +  nomogram[Int(t12*d2  + t2)].2  * q1
                
            }
            else{
                let t11 = floor(cnstHalfZero+(x1-minX.x1)/deltaX.x1)
                let t12 = ceil (-cnstHalfZero+(x1-minX.x1)/deltaX.x1)
                let t21 = floor(cnstHalfZero+(x2-minX.x2)/deltaX.x2)
                let t22 = ceil (-cnstHalfZero+(x2-minX.x2)/deltaX.x2)
                let d2  = floor(cnstHalfZero+(maxX.x2-minX.x2)/deltaX.x2+1)
                
                var q1  = (x1 - nomogram[Int(t11*d2+t21)].0)//(nomogram[Int(t12*d2+t2)].1-nomogram[Int(t11*d2+t2)].1)
                q1 = q1 / (nomogram[Int(t12*d2+t21)].0 - nomogram[Int(t11*d2+t21)].0)
                
                var q2  = (x2-nomogram[Int(t11*d2+t21)].1)//(nomogram[Int(t1*d2+t22)].1-nomogram[Int(t1*d2+t21)].1)
                q2 = q2 / (nomogram[Int(t11*d2+t22)].1 - nomogram[Int(t11*d2+t21)].1)
                
                
                
                //let q1   = (x1-nomogram[Int(t11*d2+t21)].1)/(nomogram[Int(t12*d2+t21)].1-nomogram[Int(t11*d2+t21)].1)
                //let q2   = (x2-nomogram[Int(t11*d2+t21)].1)/(nomogram[Int(t11*d2+t22)].1-nomogram[Int(t11*d2+t21)].1)
                
                var res = nomogram[Int(t11*d2  + t21) ].2 * (1-q1) * (1-q2)  +  nomogram[Int(t11*d2  + t22) ].2 * (1-q1) * q2
                res += nomogram[Int(t12*d2  + t21) ].2 * q1 * (1-q2)
                res += nomogram[Int(t12*d2  + t22) ].2 * q1 * q2
                
                return res
                
            }
        }
        
        
        
        //return Int(floor( ((x1-minX.x1)/deltaX.x1) * ((maxX.x2-minX.x2)/deltaX.x2+1) + (x2-minX.x2)/deltaX.x2))
    }
    
    //----------------------------------------------------------------------------------------------
    
/*
    
    NSLog("Start")
    let file = "infile.txt"
    
    let dirs : String[]? = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true) as? String[]
    
    if (dirs != nil) {
    let directories:String[] = dirs!;
    let dir = "/Users/admin/Documents/"//"directories[0]; //documents directory
    let path = dir.stringByAppendingPathComponent(file);
    //let text = "some text"
    
    //writing
    //text.writeToFile(path, atomically: false, encoding: NSUTF8StringEncoding, error: nil);
    NSLog(path)
    
    //reading
    if let text2 = String.stringWithContentsOfFile(path, encoding: NSUTF8StringEncoding, error: nil)
    {
    NSLog("Readed")
    let k = 100
    let ar = text2.componentsSeparatedByString("\n")
    let a = ar[k]
    
    
    NSLog(a)
    
    }
    else{
    NSLog("File has not been found")
    }
    }
    NSLog("Finish")
*/
    
    
    
    
    
}
